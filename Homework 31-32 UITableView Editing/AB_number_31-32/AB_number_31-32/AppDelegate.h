//
//  AppDelegate.h
//  AB_number_31-32
//
//  Created by Alexander Berezovskyy on 03.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

