//
//  ViewController.m
//  AB_number_31-32
//
//  Created by Alexander Berezovskyy on 03.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

#import "ABStudent.h"
#import "ABGroup.h"

@interface ViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *groupsArray;
@property (nonatomic, strong) UITableView *generalTableView;

@end

@implementation ViewController

- (void) loadView {
    [super loadView];
    
    // create table
    
    CGRect frame = self.view.bounds;
    frame.origin = CGPointZero;
    
    UITableView *generalTableView = [[UITableView alloc] initWithFrame:frame style:UITableViewStyleGrouped];
    
    generalTableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    generalTableView.delegate = self;
    generalTableView.dataSource = self;
    
    [self.view addSubview:generalTableView];
    
    self.generalTableView = generalTableView;
    self.generalTableView.allowsSelectionDuringEditing = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.groupsArray = [NSMutableArray array];
    
    for (int i = 0; i < 5; i++) {
        
        ABGroup *group = [ABGroup groupWithStudents:5];
        [self.groupsArray addObject:group];
    }
    
    // Nav bar settings
    
    self.navigationItem.title = @"Students";
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.translucent = NO;
    
    // Edit button
    
    UIBarButtonItem *editBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editAction:)];
    editBarButton.tintColor = [UIColor cyanColor];
    self.navigationItem.rightBarButtonItem = editBarButton;
    
    // Add group button
    
    UIBarButtonItem *addGroupBarButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addGroupAction:)];
    addGroupBarButton.tintColor = [UIColor cyanColor];
    self.navigationItem.leftBarButtonItem = addGroupBarButton;
}


#pragma mark - Actions

- (void) addGroupAction:(UIBarButtonItem *) sender {
 
    ABGroup *group = [ABGroup groupWithStudents:2];
    
    NSInteger addSectionAtIndex = 0;
    
    [self.groupsArray insertObject:group atIndex:addSectionAtIndex];
    
    UITableViewRowAnimation rowAnimation = self.groupsArray.count % 2 ? UITableViewRowAnimationRight : UITableViewRowAnimationLeft;
    
    [self.generalTableView beginUpdates];
    
    [self.generalTableView insertSections:[NSIndexSet indexSetWithIndex:addSectionAtIndex] withRowAnimation:rowAnimation];
    
    [self.generalTableView endUpdates];
}

- (void) editAction:(UIBarButtonItem *) sender {
    
    Boolean isEditing = self.generalTableView.editing;
    [self.generalTableView setEditing:!isEditing animated:YES];
    
    UIBarButtonSystemItem currentItem = UIBarButtonSystemItemEdit;
    
    if (self.generalTableView.editing) {
        currentItem = UIBarButtonSystemItemDone;
    }
    
    UIBarButtonItem *newButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:currentItem target:self action:@selector(editAction:)];
    newButton.tintColor = [UIColor cyanColor];
    self.navigationItem.rightBarButtonItem = newButton;
}


#pragma mark - UITableViewDataSource

// Rows & Sections

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.groupsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    ABGroup *group = [self.groupsArray objectAtIndex:section];
    
    return group.groupStudents.count + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *firstCellIdentifier = @"firstCellIdentifier";
    static NSString *studentCellIdentifier = @"studentCellIdentifier";
    
    if (indexPath.row == 0) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:firstCellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:firstCellIdentifier];
        }
        
        cell.textLabel.text = @"Tap to add new student";
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.backgroundColor = [UIColor lightGrayColor];
        cell.layer.borderColor = [UIColor whiteColor].CGColor;
        cell.layer.borderWidth = 0.5f;
        
        return cell;
        
    } else {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:studentCellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:studentCellIdentifier];
        }
        
        ABGroup *group = [self.groupsArray objectAtIndex:indexPath.section];
        ABStudent *student = [group.groupStudents objectAtIndex:indexPath.row - 1];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%1.2f", student.averageGrade];
        
        return cell;
    }
}

// Titles

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    ABGroup *group = [self.groupsArray objectAtIndex:section];
    
    return group.groupName;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section {
    ABGroup *group = [self.groupsArray objectAtIndex:section];
    NSString *studentsCount = [NSString stringWithFormat:@"Students in group:  %li", group.groupStudents.count];
    
    return studentsCount;
}

// Data manipulation - insert and delete support

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        ABGroup *group = [self.groupsArray objectAtIndex:indexPath.section];
        ABStudent *student = [group.groupStudents objectAtIndex:indexPath.row - 1];
        
        NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:group.groupStudents];
        [tmpArray removeObject:student];
        group.groupStudents = tmpArray;
        
        [self.generalTableView beginUpdates];
        
        [self.generalTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.generalTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationRight];
        });
        
        [self.generalTableView endUpdates];
    }
}

// Moving & reordering

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return indexPath.row > 0;
}

// Data manipulation - reorder / moving support

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    ABGroup *sourceGroup = [self.groupsArray objectAtIndex:sourceIndexPath.section];
    ABStudent *sourceStudent = [sourceGroup.groupStudents objectAtIndex:sourceIndexPath.row - 1];
    
    ABGroup *destinationGroup = [self.groupsArray objectAtIndex:destinationIndexPath.section];
    
    NSMutableArray *tmpStudentsGroupArray = [NSMutableArray arrayWithArray:sourceGroup.groupStudents];
    
    if (sourceIndexPath.section == destinationIndexPath.section) {
        
        [tmpStudentsGroupArray exchangeObjectAtIndex:sourceIndexPath.row - 1 withObjectAtIndex:destinationIndexPath.row - 1];
        
        sourceGroup.groupStudents = tmpStudentsGroupArray;
        
    } else {
        
        [tmpStudentsGroupArray removeObject:sourceStudent];
        sourceGroup.groupStudents = tmpStudentsGroupArray;

        tmpStudentsGroupArray = [NSMutableArray arrayWithArray:destinationGroup.groupStudents];
        [tmpStudentsGroupArray insertObject:sourceStudent atIndex:destinationIndexPath.row];
        destinationGroup.groupStudents = tmpStudentsGroupArray;
        
        [self.generalTableView reloadData];
    }
    
    // if elements count in group equal zero - delete group (uncomment)
   
    /*
    if (sourceGroup.groupStudents.count == 0) {
        
        [self.groupsArray removeObject:sourceGroup];
        
        [self.generalTableView beginUpdates];
        
        [self.generalTableView deleteSections:[NSIndexSet indexSetWithIndex:sourceIndexPath.section] withRowAnimation:UITableViewRowAnimationRight];
        
        [self.generalTableView endUpdates];
    }
     */
}


#pragma mark - UITableViewDelegate

// Moving/reordering

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    
    if (proposedDestinationIndexPath.row == 0) {
        return sourceIndexPath;
    } else {
        return proposedDestinationIndexPath;
    }
}

// Header & Footer

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = [UIColor whiteColor].CGColor;
    view.layer.backgroundColor = [UIColor brownColor].CGColor;
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    header.textLabel.textAlignment = NSTextAlignmentCenter;
    header.textLabel.textColor = [UIColor whiteColor];
    
}

- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section {
    
    view.layer.borderWidth = 0.5f;
    view.layer.borderColor = [UIColor yellowColor].CGColor;
    view.layer.backgroundColor = [UIColor lightGrayColor].CGColor;
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    header.textLabel.textAlignment = NSTextAlignmentRight;
    header.textLabel.textColor = [UIColor whiteColor];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 30.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 27.f;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(nonnull UITableViewCell *)cell forRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    // cell create with animation - uncommented rows
    
    /*
    cell.frame = CGRectMake(0 - CGRectGetWidth(cell.frame), CGRectGetMinY(cell.frame), CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame));
    
    [UIView animateWithDuration:0.3f
                          delay:0.1f
                        options:UIViewAnimationOptionCurveEaseOut | UIViewContentModeScaleAspectFill 
                     animations:^{
                         
                         cell.frame = CGRectMake(CGRectGetMinX(cell.frame) + CGRectGetWidth(cell.frame), CGRectGetMinY(cell.frame), CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame));

                     }
                     completion:^(BOOL finished) {

                     }];
    */
}

// Editing

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        return UITableViewCellEditingStyleInsert;
    }
    return UITableViewCellEditingStyleDelete;
}

// Selected

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        
        ABGroup *group = [self.groupsArray objectAtIndex:indexPath.section];
        
        NSInteger addRowIndex = 0;
        
        NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:group.groupStudents];
        [tmpArray insertObject:[ABStudent randomStudent] atIndex:addRowIndex];
        group.groupStudents = tmpArray;
        
        NSIndexPath *newIndexPath = [NSIndexPath indexPathForRow:addRowIndex + 1 inSection:indexPath.section];
        
        [self.generalTableView beginUpdates];
        
        [self.generalTableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationMiddle];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            
            [self.generalTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationNone];
        });
        
        [self.generalTableView endUpdates];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return indexPath.row == 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
