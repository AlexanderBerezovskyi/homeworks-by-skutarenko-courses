//
//  ABStudent.h
//  AB_number_31-32
//
//  Created by Alexander Berezovskyy on 03.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABStudent : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, assign) CGFloat averageGrade;

+ (ABStudent *) randomStudent;

@end
