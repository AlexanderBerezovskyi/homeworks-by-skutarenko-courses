//
//  ABStudent.m
//  AB_number_31-32
//
//  Created by Alexander Berezovskyy on 03.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

static NSString *firstNames[] = {@"Georgan", @"Oigenhein", @"Denya", @"Sergio", @"Arslanito", @"Igorio"};

static NSString *lastNames[] = {@"Petrov", @"Ivanov", @"Sydorov", @"Mamontov", @"Vetrov", @"Vorotov"};

static int namesCount = 6;

+ (ABStudent *) randomStudent {
    
    ABStudent *student = [[ABStudent alloc] init];
    
    student.firstName = firstNames [arc4random_uniform(namesCount)];
    student.lastName = lastNames [arc4random_uniform(namesCount)];
    student.averageGrade = arc4random_uniform(301)/100.f + 2.f;
    
    return student;
}



@end
