//
//  ABGroup.m
//  AB_number_31-32
//
//  Created by Alexander Berezovskyy on 03.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABGroup.h"

@implementation ABGroup

static NSInteger groupNumber = 1;

+ (ABGroup *) groupWithStudents:(NSInteger)countStudent {
    
    ABGroup *group = [[ABGroup alloc] init];
    
    NSInteger studentsCountInGroup = countStudent;
    
    NSMutableArray *studentsArrayInGroup = [NSMutableArray array];
    
    for (int i = 0; i < studentsCountInGroup; i++) {
        
        ABStudent *student = [ABStudent randomStudent];
        [studentsArrayInGroup addObject:student];
    }
    
    group.groupStudents = studentsArrayInGroup;
    group.groupName = [NSString stringWithFormat:@"Group № %li", groupNumber];
    
    groupNumber++;
    
    return group;
}

@end
