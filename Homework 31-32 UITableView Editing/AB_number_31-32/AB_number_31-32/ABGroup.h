//
//  ABGroup.h
//  AB_number_31-32
//
//  Created by Alexander Berezovskyy on 03.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABStudent.h"

@interface ABGroup : NSObject

@property (nonatomic, strong) NSString *groupName;
@property (nonatomic, strong) NSArray *groupStudents;

+ (ABGroup *) groupWithStudents:(NSInteger)countStudent;

@end
