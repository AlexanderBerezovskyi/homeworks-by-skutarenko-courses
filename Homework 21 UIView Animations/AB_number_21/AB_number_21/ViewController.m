//
//  ViewController.m
//  AB_number_21
//
//  Created by Alexander Berezovskyy on 02.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) UIView *view1;
@property (nonatomic, weak) UIView *view2;
@property (nonatomic, weak) UIView *view3;
@property (nonatomic, weak) UIView *view4;
@property (nonatomic, weak) UIView *view5;
@property (nonatomic, weak) UIView *view6;
@property (nonatomic, weak) UIView *view7;
@property (nonatomic, weak) UIView *view8;

@property (nonatomic, assign) CGPoint directionLeftToRight;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
#pragma mark - Level Pupil
    
    // Settings
    
    CGFloat sideRect = 50.f;
    CGRect rectSVB = self.view.bounds;
    
    
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 100, sideRect, sideRect)];
    UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 200, sideRect, sideRect)];
    UIView *view3 = [[UIView alloc] initWithFrame:CGRectMake(0, 300, sideRect, sideRect)];
    UIView *view4 = [[UIView alloc] initWithFrame:CGRectMake(0, 400, sideRect, sideRect)];
    
    view1.backgroundColor = [self randomColor];
    view2.backgroundColor = [self randomColor];
    view3.backgroundColor = [self randomColor];
    view4.backgroundColor = [self randomColor];
    
    [self.view addSubview:view1];
    [self.view addSubview:view2];
    [self.view addSubview:view3];
    [self.view addSubview:view4];
    
    self.view1 = view1;
    self.view2 = view2;
    self.view3 = view3;
    self.view4 = view4;
    
    
#pragma mark - Level Student
    
    UIView *view5 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(rectSVB),
                                                             CGRectGetMinY(rectSVB),
                                                             sideRect, sideRect)];
    
    UIView *view6 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(rectSVB),
                                                             CGRectGetMaxY(rectSVB) - sideRect,
                                                             sideRect, sideRect)];
    
    UIView *view7 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rectSVB) - sideRect,
                                                             CGRectGetMinY(rectSVB),
                                                             sideRect, sideRect)];
    
    UIView *view8 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rectSVB) - sideRect,
                                                             CGRectGetMaxY(rectSVB) - sideRect,
                                                             sideRect, sideRect)];
    view5.backgroundColor = [UIColor redColor];
    view6.backgroundColor = [UIColor yellowColor];
    view7.backgroundColor = [UIColor greenColor];
    view8.backgroundColor = [UIColor blueColor];
    
    [self.view addSubview:view5];
    [self.view addSubview:view6];
    [self.view addSubview:view7];
    [self.view addSubview:view8];
    
    self.view5 = view5;
    self.view6 = view6;
    self.view7 = view7;
    self.view8 = view8;
    
    CGPoint directionLeftToRight = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.view1.bounds), 0);
    
    self.directionLeftToRight = directionLeftToRight;
    
    
#pragma mark - Level Master
    
//    [self createDeers:5];

}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self moveViewByCurve:self.view1
    withAnimationsOptions:UIViewAnimationOptionCurveEaseInOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
       withDirectionPoint:self.directionLeftToRight];
    
    [self moveViewByCurve:self.view2
    withAnimationsOptions:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
       withDirectionPoint:self.directionLeftToRight];
    
    [self moveViewByCurve:self.view3
    withAnimationsOptions:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
       withDirectionPoint:self.directionLeftToRight];

    [self moveViewByCurve:self.view4
    withAnimationsOptions:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
       withDirectionPoint:self.directionLeftToRight];
    
    [self clockWiseMovement];
}

#pragma mark - Color & Movement

- (UIColor *) randomColor {
    
    CGFloat r = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat g = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat b = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat a = (CGFloat) arc4random_uniform(51)  / 100;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:(0.5f + a)];
}

- (void) moveViewByCurve:(UIView *) currentView withAnimationsOptions:(UIViewAnimationOptions) animationOption withDirectionPoint:(CGPoint) directionPoint {
    
    [UIView animateWithDuration:2.f
                          delay:0
                        options:animationOption
                     animations:^{
   
                     currentView.transform = CGAffineTransformMakeTranslation(directionPoint.x,
                                                                              directionPoint.y);
                     }
     
                     completion:^(BOOL finished) {
                         NSLog(@"End of animation");
                     }];
}

- (void) clockWiseMovement {
    
    NSInteger direction = arc4random_uniform(2);
        
        [UIView animateWithDuration:2.f delay:0.f options:UIViewAnimationOptionCurveLinear animations:^{
            
            CGPoint tmpPoint5 = self.view5.center;
            CGPoint tmpPoint6 = self.view6.center;
            CGPoint tmpPoint7 = self.view7.center;
            CGPoint tmpPoint8 = self.view8.center;
            
            UIColor *tmpColor5 = self.view5.backgroundColor;
            UIColor *tmpColor6 = self.view6.backgroundColor;
            UIColor *tmpColor7 = self.view7.backgroundColor;
            UIColor *tmpColor8 = self.view8.backgroundColor;
            
            if (direction == 1) {
 
                self.view5.center = tmpPoint7;
                self.view6.center = tmpPoint5;
                self.view7.center = tmpPoint8;
                self.view8.center = tmpPoint6;
                
                self.view5.backgroundColor = tmpColor7;
                self.view6.backgroundColor = tmpColor5;
                self.view7.backgroundColor = tmpColor8;
                self.view8.backgroundColor = tmpColor6;
            
            } else if (direction == 0) {
                
                self.view7.center = tmpPoint5;
                self.view5.center = tmpPoint6;
                self.view8.center = tmpPoint7;
                self.view6.center = tmpPoint8;
                
                self.view7.backgroundColor = tmpColor5;
                self.view5.backgroundColor = tmpColor6;
                self.view8.backgroundColor = tmpColor7;
                self.view6.backgroundColor = tmpColor8;
            }
   
        } completion:^(BOOL finished) {
            
            [self clockWiseMovement];
        }];
}

- (void) moveView:(UIView *)currentView {
    
    CGRect rect = self.view.bounds;
    
//    rect = CGRectInset(rect, CGRectGetWidth(currentView.frame), CGRectGetHeight(currentView.frame));
    
    CGFloat x = arc4random() % (NSInteger) CGRectGetWidth(rect) + CGRectGetMinX(rect);
    CGFloat y = arc4random() % (NSInteger) CGRectGetHeight(rect) + CGRectGetMinY(rect);
//    CGFloat r = (CGFloat) (arc4random() % (NSInteger) (M_PI * 2 * 10000)) / 10000 - M_PI;
    
    NSInteger time = arc4random_uniform(8) + 1;
    
    [UIView animateWithDuration:time
                          delay:0
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         currentView.center = CGPointMake(x, y);
                         
//                         CGAffineTransform rotation = CGAffineTransformMakeRotation(r);
//                         currentView.transform = rotation;
                         
                     }
                     completion:^(BOOL finished) {
                         
                         __weak UIView* v = currentView;
                         [self moveView:v];
                         
                     }];
}

- (void) createDeers:(NSInteger) deers {
    
    for (int i = 0; i < deers; i++) {
        
        UIImageView *deerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(100, 100, 189, 245)];
        [self.view addSubview:deerImageView];
        
        UIImage *deerImage1 = [UIImage imageNamed:@"1.png"];
        UIImage *deerImage2 = [UIImage imageNamed:@"2.png"];
        UIImage *deerImage3 = [UIImage imageNamed:@"3.png"];
        UIImage *deerImage4 = [UIImage imageNamed:@"4.png"];
        UIImage *deerImage5 = [UIImage imageNamed:@"5.png"];
        UIImage *deerImage6 = [UIImage imageNamed:@"6.png"];
        UIImage *deerImage7 = [UIImage imageNamed:@"7.png"];
        
        NSArray *deerImagesArray = [[NSArray alloc] initWithObjects:deerImage1, deerImage2, deerImage3, deerImage4, deerImage5, deerImage6, deerImage7, nil];
        deerImageView.animationImages = deerImagesArray;
        deerImageView.animationDuration = 1.5f;
        [deerImageView startAnimating];
        
        [self moveView:deerImageView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
