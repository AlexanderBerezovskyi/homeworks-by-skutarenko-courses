//
//  ViewController.m
//  AB_number_21
//
//  Created by Alexander Berezovskyy on 02.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, weak) UIView *view1;
@property (nonatomic, weak) UIView *view2;
@property (nonatomic, weak) UIView *view3;
@property (nonatomic, weak) UIView *view4;
@property (nonatomic, weak) UIView *view6;
@property (nonatomic, weak) UIView *view7;
@property (nonatomic, weak) UIView *view8;

@property (nonatomic, assign) CGPoint directionLeftToRight;

@property (nonatomic, weak) UIImageView *deerImageView;

@property (nonatomic, assign) CGFloat rotation;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
#pragma mark - My dear deer
    
    // Settings
    
    CGFloat sideRect = 150.f;
    CGRect rectSVB = self.view.bounds;
    
    UIImageView *deerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    [self.view addSubview:deerImageView];
    
    UIImage *deerImage1 = [UIImage imageNamed:@"1.png"];
    UIImage *deerImage2 = [UIImage imageNamed:@"2.png"];
    UIImage *deerImage3 = [UIImage imageNamed:@"3.png"];
    UIImage *deerImage4 = [UIImage imageNamed:@"4.png"];
    UIImage *deerImage5 = [UIImage imageNamed:@"5.png"];
    UIImage *deerImage6 = [UIImage imageNamed:@"6.png"];
    UIImage *deerImage7 = [UIImage imageNamed:@"7.png"];
    
    NSArray *deerImagesArray = [[NSArray alloc] initWithObjects:deerImage1, deerImage2, deerImage3, deerImage4, deerImage5, deerImage6, deerImage7, nil];
    deerImageView.animationImages = deerImagesArray;
    deerImageView.animationDuration = 0.5f;
    CGFloat r = M_PI_2;
    CGAffineTransform rotation = CGAffineTransformMakeRotation(r);
    deerImageView.transform = rotation;
    [deerImageView startAnimating];
    self.rotation = r;
    
    self.deerImageView = deerImageView;
    
    
    UIView *view6 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(rectSVB),
                                                             CGRectGetMaxY(rectSVB) - sideRect,
                                                             sideRect, sideRect)];
    
    UIView *view7 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rectSVB) - sideRect,
                                                             CGRectGetMinY(rectSVB),
                                                             sideRect, sideRect)];
    
    UIView *view8 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rectSVB) - sideRect,
                                                             CGRectGetMaxY(rectSVB) - sideRect,
                                                             sideRect, sideRect)];
    view6.backgroundColor = [UIColor clearColor];
    view7.backgroundColor = [UIColor clearColor];
    view8.backgroundColor = [UIColor clearColor];

    [self.view addSubview:view6];
    [self.view addSubview:view7];
    [self.view addSubview:view8];

    self.view6 = view6;
    self.view7 = view7;
    self.view8 = view8;
    
    CGPoint directionLeftToRight = CGPointMake(CGRectGetWidth(self.view.bounds) - CGRectGetWidth(self.view1.bounds), 0);
    
    self.directionLeftToRight = directionLeftToRight;
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self clockWiseMovement];
}

#pragma mark - Color & Movement

- (void) clockWiseMovement {
    
    self.rotation += M_PI_2;
    CGAffineTransform rotation = CGAffineTransformMakeRotation(self.rotation);
    self.deerImageView.transform = rotation;
    
        [UIView animateWithDuration:2.f
                              delay:0.f
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
            
            CGPoint tmpPoint5 = self.deerImageView.center;
            CGPoint tmpPoint6 = self.view6.center;
            CGPoint tmpPoint7 = self.view7.center;
            CGPoint tmpPoint8 = self.view8.center;

                self.deerImageView.center = tmpPoint7;
                self.view6.center = tmpPoint5;
                self.view7.center = tmpPoint8;
                self.view8.center = tmpPoint6;
   
        } completion:^(BOOL finished) {
            
            [self clockWiseMovement];
        }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
