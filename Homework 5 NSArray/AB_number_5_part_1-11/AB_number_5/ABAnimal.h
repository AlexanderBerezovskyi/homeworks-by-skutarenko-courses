//
//  ABAnimal.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABAnimal : NSObject

@property (nonatomic, strong) NSString *color;

- (void) movement;

@end
