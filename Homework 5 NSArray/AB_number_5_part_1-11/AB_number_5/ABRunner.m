//
//  ABRunner.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABRunner.h"

@implementation ABRunner

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"Petr";
        self.height = 1.85f;
        self.weight = 82;
        self.gender = @"Man";
    }
    return self;
}

- (void) movement {
    
    NSLog(@"Run");
}

@end
