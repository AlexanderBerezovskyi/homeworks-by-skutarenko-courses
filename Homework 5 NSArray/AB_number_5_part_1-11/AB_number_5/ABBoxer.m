//
//  ABBoxer.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABBoxer.h"

@implementation ABBoxer

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"Leila";
        self.height = 1.72f;
        self.weight = 64;
        self.gender = @"Woman";
        self.reaction = 0.2f;
        self.power = 12;
    }
    return self;
}

- (void) movement {
    
    NSLog(@"Boxing");
}

@end
