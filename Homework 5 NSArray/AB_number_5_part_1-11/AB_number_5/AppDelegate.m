//
//  AppDelegate.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"

#import "ABRunner.h"
#import "ABSwimmer.h"
#import "ABBicyclist.h"
#import "ABBoxer.h"

#import "ABCat.h"
#import "ABDog.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    ABHuman *human = [[ABHuman alloc] init];
    
    ABRunner *runer = [[ABRunner alloc] init];
    ABSwimmer *swimmer = [[ABSwimmer alloc] init];
    ABBicyclist *bicyclist = [[ABBicyclist alloc] init];
    ABBoxer *boxer = [[ABBoxer alloc] init];
    
    ABCat *cat = [[ABCat alloc] init];
    ABDog *dog = [[ABDog alloc] init];
    
    NSMutableArray *objects = [NSMutableArray arrayWithObjects: runer, swimmer, bicyclist, boxer, cat, dog, nil];
    
    for (NSInteger i = [objects count] - 1; i >= 0; i--) {
        
        id s = [objects objectAtIndex:i];
        
        if ([s isKindOfClass:[ABAnimal class]]) {
            
            ABAnimal *a = (ABAnimal *) s;
            
            NSLog(@"Class = %@. Color = %@", a.superclass, a.color);
            
        } else if ([s isKindOfClass:[ABHuman class]]) {
        
            ABHuman *h = (ABHuman *) s;
            
        if ([s isKindOfClass:[ABBoxer class]]) {
           
            ABBoxer *boxer = (ABBoxer *) s;
            NSLog(@"Class = %@. Name - %@, height = %.2f m, weight = %li kg, gender = %@, power = %li, reaction = %.1f s", boxer.superclass, boxer.name, boxer.height, boxer.weight, boxer.gender, boxer.power, boxer.reaction);
            [human movement];
            
        } else {
            
            NSLog(@"Class = %@. Name - %@, height = %.2f m, weight = %li kg, gender = %@", h.superclass, h.name, h.height, h.weight, h.gender);
        }
        }
            [s movement];
    }
    
      
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
