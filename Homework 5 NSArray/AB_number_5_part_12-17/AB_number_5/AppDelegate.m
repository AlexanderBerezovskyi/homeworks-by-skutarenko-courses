//
//  AppDelegate.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"

#import "ABRunner.h"
#import "ABSwimmer.h"
#import "ABBicyclist.h"
#import "ABBoxer.h"

#import "ABCat.h"
#import "ABDog.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    ABRunner *runer = [[ABRunner alloc] init];
    ABSwimmer *swimmer = [[ABSwimmer alloc] init];
    ABBicyclist *bicyclist = [[ABBicyclist alloc] init];
    ABBoxer *boxer = [[ABBoxer alloc] init];
    
    ABCat *cat = [[ABCat alloc] init];
    ABDog *dog = [[ABDog alloc] init];
    
    NSArray *sportsmen = [[NSArray alloc] initWithObjects:runer, swimmer, bicyclist, boxer, nil];
    NSArray *animals = [[NSArray alloc] initWithObjects:cat, dog, nil];
    
    NSInteger c = MAX([sportsmen count], [animals count]);
    
    for (NSInteger i = 0; i < c; i++) {
        
        if ((i >= 0) && (i < [sportsmen count])) {
            ABHuman *s = [sportsmen objectAtIndex:i];
            NSLog(@"Name - %@, height = %.2f", s.name, s.height);
        }
        
        if ((i >= 0) && (i < [animals count])) {
            ABAnimal *a = [animals objectAtIndex:i];
            NSLog(@"Name - %@, color = %@", a.name, a.color);
        }
    }
  
    NSMutableArray *objectsMutable = [NSMutableArray array];
    [objectsMutable addObjectsFromArray:sportsmen];
    [objectsMutable addObjectsFromArray:animals];
    
#pragma mark - NSSortDescriptor
    
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"type" ascending:NO];
    
    NSArray *descriptorsArray = [NSArray arrayWithObjects:sortDescriptor2, sortDescriptor1, nil];
    NSArray *sortedObjectMutable = [objectsMutable sortedArrayUsingDescriptors:descriptorsArray];
    
    for (id object in sortedObjectMutable) {
        
        if ([object isKindOfClass:[ABAnimal class]]) {
            ABAnimal *animal = (ABAnimal *) object;
            NSLog(@"Name = %@, superclass = %@", animal.name, animal.superclass);
        }
        
        if ([object isKindOfClass:[ABHuman class]]) {
            ABHuman *human = (ABHuman *) object;
            NSLog(@"Name = %@, superclass = %@", human.name, human.superclass);
        }
    }
    
      
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
