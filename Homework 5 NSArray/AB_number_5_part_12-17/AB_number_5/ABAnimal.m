//
//  ABAnimal.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABAnimal.h"

@implementation ABAnimal

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.color = @"None";
        self.name = @"No name";
        self.type = @"Animals";
    }
    return self;
}

- (void) movement {
    
    NSLog(@"run like animal");
}


@end
