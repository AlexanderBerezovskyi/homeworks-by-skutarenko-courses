//
//  ABBoxer.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABHuman.h"

@interface ABBoxer : ABHuman

@property (nonatomic, assign) NSInteger power;
@property (nonatomic, assign) CGFloat reaction;

@end
