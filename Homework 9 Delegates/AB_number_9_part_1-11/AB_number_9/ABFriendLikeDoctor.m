//
//  ABFriendLikeDoctor.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABFriendLikeDoctor.h"

@implementation ABFriendLikeDoctor

#pragma mark - ABPatientDelegate

- (void)patientGotWorse:(ABPatient *)patient {
    
    NSLog(@"Patient %@ feels bad", patient.name);
    NSLog(@"Doctor: Do you have a headache? - %@. Cough? - %@.", patient.headache == NO ? @"NO" : @"YES", patient.cough == NO ? @"NO" : @"YES");
    
    if (patient.temperature > 38.f) {
        [patient drinkDecoction];
    
         } else {
        NSLog(@"Patient %@ go walk", patient.name);
    }
    
}

- (void) patientHasPain:(ABPatient *) patient {
    
    NSLog(@"Friend: I don't know what we can do");
}


@end
