//
//  AppDelegate.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABPatient.h"
#import "ABDoctor.h"
#import "ABFriendLikeDoctor.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    ABPatient *patient1 = [ABPatient temperatureCoughHeadacheForName:@"Alex"];
    ABPatient *patient2 = [ABPatient temperatureCoughHeadacheForName:@"Olga"];
    ABPatient *patient3 = [ABPatient temperatureCoughHeadacheForName:@"Kirill"];
    
    ABDoctor *doctor1 = [[ABDoctor alloc] init];
    patient1.delegate = doctor1;
    patient2.delegate = doctor1;
    patient3.delegate = doctor1;
    
    ABPatient *patient4 = [ABPatient temperatureCoughHeadacheForName:@"Anna"];
    ABPatient *patient5 = [ABPatient temperatureCoughHeadacheForName:@"Sergey"];
    
    ABFriendLikeDoctor *friendLikeDoctor1 = [[ABFriendLikeDoctor alloc] init];
    patient4.delegate = friendLikeDoctor1;
    patient5.delegate = friendLikeDoctor1;
    
    ABPatient *patient6 = [ABPatient temperatureCoughHeadacheForName:@"Tamara"];
    ABPatient *patient7 = [ABPatient temperatureCoughHeadacheForName:@"Denis"];
    
    ABFriendLikeDoctor *friendLikeDoctor2 = [[ABFriendLikeDoctor alloc] init];
    patient6.delegate = friendLikeDoctor2;
    patient7.delegate = friendLikeDoctor2;

    NSArray *patientsArray = [[NSArray alloc] initWithObjects:patient1, patient2, patient3,
                                                              patient4, patient5, patient6,
                                                              patient7, nil];
    
    for (ABPatient *pat in patientsArray) {
        
        [pat howAreYou];
    }
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
