//
//  AppDelegate.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABPatient.h"
#import "ABDoctor.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    ABPatient *patient1 = [ABPatient temperatureCoughHeadacheForName:@"Patient_1"];
    ABPatient *patient2 = [ABPatient temperatureCoughHeadacheForName:@"Patient_2"];
    ABPatient *patient3 = [ABPatient temperatureCoughHeadacheForName:@"Patient_3"];
    
    ABDoctor *doctor1 = [[ABDoctor alloc] init];
    doctor1.name = @"Doctor_1";
    patient1.delegate = doctor1;
    patient2.delegate = doctor1;
    patient3.delegate = doctor1;
    
    ABPatient *patient4 = [ABPatient temperatureCoughHeadacheForName:@"Patient_4"];
    ABPatient *patient5 = [ABPatient temperatureCoughHeadacheForName:@"Patient_5"];
    ABPatient *patient6 = [ABPatient temperatureCoughHeadacheForName:@"Patient_6"];
    
    ABDoctor *doctor2 = [[ABDoctor alloc] init];
    doctor2.name = @"Doctor_2";
    patient4.delegate = doctor2;
    patient5.delegate = doctor2;
    patient6.delegate = doctor2;
    
    ABPatient *patient7 = [ABPatient temperatureCoughHeadacheForName:@"Patient_7"];
    ABPatient *patient8 = [ABPatient temperatureCoughHeadacheForName:@"Patient_8"];
    ABPatient *patient9 = [ABPatient temperatureCoughHeadacheForName:@"Patient_9"];
    ABPatient *patient10 = [ABPatient temperatureCoughHeadacheForName:@"Patient_10"];
    ABPatient *patient11 = [ABPatient temperatureCoughHeadacheForName:@"Patient_11"];
    ABPatient *patient12 = [ABPatient temperatureCoughHeadacheForName:@"Patient_12"];
    ABPatient *patient13 = [ABPatient temperatureCoughHeadacheForName:@"Patient_13"];
    ABPatient *patient14 = [ABPatient temperatureCoughHeadacheForName:@"Patient_14"];

    ABDoctor *doctor3 = [[ABDoctor alloc] init];
    doctor3.name = @"Doctor_3";
    patient7.delegate = doctor3;
    patient8.delegate = doctor3;
    patient9.delegate = doctor3;
    patient10.delegate = doctor3;
    patient11.delegate = doctor3;
    patient12.delegate = doctor3;
    patient13.delegate = doctor3;
    patient14.delegate = doctor3;
    
    NSArray *patientsArray = [[NSArray alloc] initWithObjects:patient1, patient2, patient3,
                                                              patient4, patient5, patient6,
                                                              patient7, patient8, patient9,
                                                              patient10, patient11, patient12,
                                                              patient13, patient14, nil];
    
    NSArray *doctorsArray = [[NSArray alloc] initWithObjects:doctor1, doctor2, doctor3, nil];
    
    for (ABPatient *patient in patientsArray) {
        
        [patient howAreYou];
        NSLog(@"mark = %lu", patient.doctorMark);
        
    }
    
    for (ABDoctor *doctor in doctorsArray) {

        NSLog(@"%@ report: leg hurts = %@, hand hurts = %@, stomach hurts = %@", doctor.name,
              [doctor.raport allKeysForObject:@"Leg hurts"],
              [doctor.raport allKeysForObject:@"Hand hurts"],
              [doctor.raport allKeysForObject:@"Stomach hurts"]);
    }

    // Some patients have changed doctors
    
    for (ABPatient *patient in patientsArray) {
        
        if (patient.doctorMark < 4) {
            
            if (patient.delegate == doctor1) {
                patient.delegate = doctor2;
            
            } else if (patient.delegate == doctor2) {
                patient.delegate = doctor3;
                
            } else if (patient.delegate == doctor3) {
                patient.delegate = doctor2;
                
            } else {
                NSLog(@"fake patient");
            }
        }
    }
    
    // After changing a doctor
    
    for (ABPatient *patient in patientsArray) {
        
        NSLog(@"The doctor of %@ is %@", patient.name, patient.delegate.name);
    }

    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
