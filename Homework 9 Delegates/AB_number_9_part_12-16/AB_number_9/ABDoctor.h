//
//  ABDoctor.h
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ABPatient.h"

@interface ABDoctor : NSObject <ABPatientDelegate>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableDictionary *raport;

@end
