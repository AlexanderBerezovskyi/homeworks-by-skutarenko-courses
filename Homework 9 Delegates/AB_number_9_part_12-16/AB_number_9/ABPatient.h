//
//  ABPatient.h
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    
    ABPartOfBodyLeg,
    ABPartOfBodyHand,
    ABPartOfBodyStomach
    
} ABPartOfBody;

@protocol ABPatientDelegate;

@interface ABPatient : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CGFloat temperature;
@property (nonatomic, assign) Boolean headache;
@property (nonatomic, assign) Boolean cough;
@property (nonatomic, assign) ABPartOfBody bodyPart;
@property (nonatomic, assign) NSUInteger doctorMark;

@property (nonatomic, weak) id <ABPatientDelegate> delegate;


#pragma mark - Create a new object ABPatient

+ (ABPatient *) temperatureCoughHeadacheForName:(NSString *) firstName;

- (Boolean) howAreYou;


#pragma mark - Methods for ABDoctor

- (void) takePill;
- (void) makeShot;

- (void) treatHand;
- (void) treatLeg;
- (void) treatStomach;

@end


#pragma mark - ABPatientDelegate

@protocol ABPatientDelegate <NSObject>

@property (nonatomic, strong) NSString *name;

- (void) patientGotWorse:(ABPatient *) patient;
- (void) patientHasPain:(ABPatient *) patient;

@end





