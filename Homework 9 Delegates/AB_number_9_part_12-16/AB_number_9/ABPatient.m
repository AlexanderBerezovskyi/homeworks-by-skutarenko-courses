//
//  ABPatient.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABPatient.h"

@implementation ABPatient

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"none";
        self.temperature = 36.6f;
        self.headache = NO;
        self.cough = NO;
        self.doctorMark = 5;
    }
    return self;
}

+ (ABPatient *) temperatureCoughHeadacheForName:(NSString *) firstName {
    
    ABPatient *patient = [[ABPatient alloc] init];
    patient.name = firstName;
    patient.temperature = (CGFloat)(arc4random_uniform(51) + 360) / 10;
    patient.cough = (Boolean) arc4random_uniform(201)/100;
    patient.headache = (Boolean) arc4random_uniform(201)/100;
    patient.bodyPart = arc4random_uniform(3);
    
    return patient;
}

- (Boolean) howAreYou {
    
    Boolean iFeelGood = (Boolean) arc4random_uniform(201)/100;
    
    if (!iFeelGood) {
        NSLog(@"**********************************");
        
        [self.delegate patientGotWorse:self];
    
    } else {
        
        NSLog(@"**********************************");
        NSLog(@"Patient %@ is OK", self.name);
    }
   
    if (self.bodyPart == ABPartOfBodyHand) {
        NSLog(@"I have some problem with my: *hand*");
    } else if (self.bodyPart == ABPartOfBodyLeg) {
        NSLog(@"I have some problem with my: *leg*");
    } else if (self.bodyPart == ABPartOfBodyStomach) {
        NSLog(@"I have some problem with my: *stomach*");
    } else {
        NSLog(@"something went wrong");
    }
    
    [self.delegate patientHasPain:self];
    
    return iFeelGood;
}

#pragma mark - Methods for ABDoctor

- (void) takePill {
    
    NSLog(@"%@ takes a pill", self.name);
}

- (void) makeShot {
    
    NSLog(@"%@ make a shot", self.name);
}

- (void) treatHand {
    
    NSLog(@"Treat the hand");
}

- (void) treatLeg {
    
    NSLog(@"Treat the leg");
}

- (void) treatStomach {
    
    NSLog(@"Treat the stomach");
}



@end

