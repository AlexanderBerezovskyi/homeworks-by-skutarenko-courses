//
//  ABDoctor.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDoctor.h"

@implementation ABDoctor

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.raport = [[NSMutableDictionary alloc] init];
        self.name = @"nope";
    }
    return self;
}

#pragma mark - ABPatientDelegate

- (void)patientGotWorse:(ABPatient *)patient {
    
    NSLog(@"Patient %@ feels bad", patient.name);
    NSLog(@"Doctor: Do you have a headache? - %@", patient.headache == NO ? @"NO" : @"YES");
    
    if (patient.temperature > 37.f && patient.temperature <= 39.f) {
        [patient takePill];
        
    } else if (patient.temperature > 39.f) {
        [patient makeShot];
        
    } else {
        NSLog(@"Patient %@ should rest", patient.name);
    }
    
    patient.doctorMark = (NSUInteger) (5 - arc4random_uniform(4));
}

- (void) patientHasPain:(ABPatient *) patient {
    
    switch (patient.bodyPart) {
            
        case ABPartOfBodyLeg:
            [self.raport setObject:@"Leg hurts" forKey:patient.name];
            [patient treatLeg];
            break;
            
        case ABPartOfBodyHand:
            [self.raport setObject:@"Hand hurts" forKey:patient.name];
            [patient treatHand];
            break;
            
        case ABPartOfBodyStomach:
            [self.raport setObject:@"Stomach hurts" forKey:patient.name];
            [patient treatStomach];
            break;
            
        default:
            NSLog(@"You don't have any promlems");
            break;
    }
}


@end
