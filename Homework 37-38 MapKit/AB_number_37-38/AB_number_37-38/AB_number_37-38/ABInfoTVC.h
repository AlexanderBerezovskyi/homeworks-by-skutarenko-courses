//
//  ABInfoTVC.h
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 24.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABInfoTVC : UITableViewController

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *bithDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *countryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageStudent;

// student info
@property (strong, nonatomic) NSString *studentName;
@property (strong, nonatomic) NSString *studentDateOfBirth;
@property (strong, nonatomic) NSString *studentCurrentAddress;
@property (strong, nonatomic) NSString *studentGender;
@property (strong, nonatomic) NSDictionary *infoAddressDictionary;

@end
