//
//  UIView+MKAnnotationView.m
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 24.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "UIView+MKAnnotationView.h"
#import <MapKit/MKAnnotationView.h>

@implementation UIView (MKAnnotationView)

- (MKAnnotationView *) superAnnotationView {
    
    if ([self isKindOfClass:[MKAnnotationView class]]) {
        return (MKAnnotationView *) self;
    }
    
    if (!self.superview) {
        return nil;
    }
    
    return [self.superview superAnnotationView];
}

@end
