//
//  UIView+MKAnnotationView.h
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 24.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MKAnnotationView;

@interface UIView (MKAnnotationView)

- (MKAnnotationView *) superAnnotationView;

@end
