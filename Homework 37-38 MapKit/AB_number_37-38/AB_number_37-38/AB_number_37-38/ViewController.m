//
//  ViewController.m
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 22.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ABStudent.h"
#import "ABMeetPlace.h"
#import "UIView+MKAnnotationView.h"

//@class MKMapView;
//@class CLLocationManager;

#import "ABInfoTVC.h"

@interface ViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) NSMutableArray *studentsArray;
@property (strong, nonatomic) CLGeocoder *geocoder;
@property (strong, nonatomic) MKDirections *directions;

// student info
@property (strong, nonatomic) NSString *studentName;
@property (strong, nonatomic) NSString *studentDateOfBirth;
@property (strong, nonatomic) NSString *studentCurrentAddress;
@property (strong, nonatomic) NSString *studentGender;
@property (strong, nonatomic) NSDictionary *infoAddressDictionary;

@property (strong, nonatomic) ABMeetPlace *meetPlace;

@property (assign, nonatomic) NSInteger countInCircle;

@property (weak, nonatomic) IBOutlet UIView *infoCountView;

@property (weak, nonatomic) IBOutlet UILabel *circle1Label;
@property (weak, nonatomic) IBOutlet UILabel *circle2Label;
@property (weak, nonatomic) IBOutlet UILabel *circle3Label;

@property (strong, nonatomic) MKCircle *circle3500m;

@end

@implementation ViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.infoCountView.alpha = 0.f;
    
    self.studentsArray = [NSMutableArray array];
    self.meetPlace = [[ABMeetPlace alloc] init];
    self.geocoder = [[CLGeocoder alloc] init];
    
    // create location manager (user location)
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.distanceFilter  = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager requestAlwaysAuthorization];
    [self.locationManager startUpdatingLocation];
        
    // create annotations (students)
    static int studentCount = 20;
    
    for (int i = 0; i < studentCount; i++) {
        
        ABStudent *student = [ABStudent randomStudent];
        
        student.coordinate = student.currentCoordinate;
        student.title = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
        student.subtitle = [student randomDateInStringAmericanFormatForDate:student.birthDate];
        
        [self.mapView addAnnotation:student];
        [self.studentsArray addObject:student];
    }
}

- (void) dealloc {
   
    if ([self.geocoder isGeocoding]) {
        [self.geocoder cancelGeocode];
    }
    
    if ([self.directions isCalculating]) {
        [self.directions cancel];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - MKMapViewDelegate

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        mapView.userLocation.title = @"I'm here!";
        return nil;
    }
    
    static NSString *identifier = @"Annotation";
    
    MKAnnotationView *currentAnnotation = [self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    
    if (!currentAnnotation) {
        
        currentAnnotation = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                         reuseIdentifier:identifier];
        currentAnnotation.canShowCallout = YES;
        
        // create 2 buttons for callout
        
        if ([annotation isKindOfClass:[ABStudent class]]) {
           
            UIButton *descriptionButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
            [descriptionButton addTarget:self
                                  action:@selector(descriptionAction:)
                        forControlEvents:UIControlEventTouchUpInside];
            currentAnnotation.rightCalloutAccessoryView = descriptionButton;
            
            UIButton *directionButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
            [directionButton addTarget:self
                                  action:@selector(directionAction:)
                        forControlEvents:UIControlEventTouchUpInside];
            currentAnnotation.leftCalloutAccessoryView = directionButton;
        }
    
    } else {
        currentAnnotation.annotation = annotation;
    }
    
    if ([annotation isKindOfClass:[ABStudent class]]) {
        
        for (ABStudent *student in self.studentsArray) {
            
            if (annotation == student) {
                
                if (student.gender == ABGenderMan) {
                    currentAnnotation.image = [UIImage imageNamed:@"man_2.jpg"];
                } else {
                    currentAnnotation.image = [UIImage imageNamed:@"woman_3.png"];
                }
            }
        }
        
    } else if ([currentAnnotation.annotation isKindOfClass:[ABMeetPlace class]]) {
        
        currentAnnotation.image = [UIImage imageNamed:@"meet_2.png"];
        currentAnnotation.draggable = YES;
    }
    
    return currentAnnotation;
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    
    [self.mapView removeOverlays:self.mapView.overlays];
    
    if (newState == MKAnnotationViewDragStateEnding) {
        [view setDragState:MKAnnotationViewDragStateNone animated:YES];
        
        [self createThreeCircles];
    }
    

}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay {
    
    if ([overlay isKindOfClass:[MKCircle class]]) {
        
        MKCircleRenderer *circleRenderer = [[MKCircleRenderer alloc] initWithOverlay:overlay];
        circleRenderer.strokeColor = [UIColor redColor];
        circleRenderer.lineWidth = 0.5f;
        circleRenderer.fillColor = [[UIColor greenColor] colorWithAlphaComponent:0.1f];
        return circleRenderer;
    
    } else if ([overlay isKindOfClass:[MKPolyline class]]) {
        
        MKPolylineRenderer *polylineRenderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
        polylineRenderer.strokeColor = [UIColor blueColor];
        polylineRenderer.lineWidth = 3.f;
        polylineRenderer.lineJoin = kCGLineJoinRound;
        polylineRenderer.lineDashPhase = 20.f;
        [polylineRenderer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:10], nil]];
        
        return polylineRenderer;
    }
    
    return nil;
}


#pragma mark - CLLocationManagerDelegate (movement of user on real devices)

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    NSLog(@"OldLocation %f %f", oldLocation.coordinate.latitude, oldLocation.coordinate.longitude);
    NSLog(@"NewLocation %f %f", newLocation.coordinate.latitude, newLocation.coordinate.longitude);
}

#pragma mark - Actions

- (IBAction)showAllAction:(UIBarButtonItem *)sender {
    
    MKMapRect mapRect = MKMapRectNull;

    for (id <MKAnnotation> annotation in self.mapView.annotations) {
        
        // coordinate of center annotation
        CLLocationCoordinate2D location = annotation.coordinate;
        MKMapPoint center = MKMapPointForCoordinate(location);
        
        static double delta = 2000;
        
        MKMapRect newRect = MKMapRectMake(center.x - delta, center.y - delta, delta * 2, delta * 2);
        
        mapRect = MKMapRectUnion(mapRect, newRect);
    }
    
    mapRect = [self.mapView mapRectThatFits:mapRect];
    
    [self.mapView setVisibleMapRect:mapRect
                        edgePadding:UIEdgeInsetsMake(20, 10, 10, 10)
                           animated:YES];
}

- (IBAction) descriptionAction:(UIButton *)sender {
    
    MKAnnotationView *annotationView = [sender superAnnotationView];
    
    if ([annotationView.annotation isKindOfClass:[ABStudent class]]) {
        
        for (ABStudent *student in self.studentsArray) {
            
            if (annotationView.annotation == student) {
                
                self.studentName = annotationView.annotation.title;
                self.studentDateOfBirth = annotationView.annotation.subtitle;
                
                (student.gender == ABGenderMan) ?
                (self.studentGender = @"Man") : (self.studentGender = @"Woman");
            }
        }
    }
    
    CLLocationCoordinate2D coordinate = annotationView.annotation.coordinate;
    
    CLLocation *location = [[CLLocation alloc] initWithLatitude:coordinate.latitude
                                                      longitude:coordinate.longitude];
    if ([self.geocoder isGeocoding]) {
        [self.geocoder cancelGeocode];
    }
    
    [self.geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
        
       NSString *message = nil;
       
       if (error) {
           message = [error localizedDescription];
       } else {
           
           if ([placemarks count] > 0) {
               
               MKPlacemark *placemark = (MKPlacemark*) [placemarks firstObject];
               
               message = [placemark.addressDictionary description];
               
               self.studentCurrentAddress = placemark.name;
               self.infoAddressDictionary = placemark.addressDictionary;
               
           } else {
               message = @"No placemarks";
           }
           
           if (self.studentCurrentAddress) {
               [self performSegueWithIdentifier:@"showInfoTVC" sender:sender];
           }
       }
    }];
}

- (IBAction) directionAction:(UIButton *)sender {
    
    MKAnnotationView *annotationView = [sender superAnnotationView];
    
    if (!annotationView) {
        return;
    }
    
    if ([self.directions isCalculating]) {
        [self.directions cancel];
    }
    
    CLLocationCoordinate2D coordinate = annotationView.annotation.coordinate;
    
    if ([self.mapView.annotations containsObject:self.meetPlace]) {
        
        MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
        
        // create 1th point of way
        MKPlacemark *meetPlacemark = [[MKPlacemark alloc] initWithCoordinate:self.meetPlace.coordinate];
        MKMapItem *meetMapItem = [[MKMapItem alloc] initWithPlacemark:meetPlacemark];
        request.source = meetMapItem;
        
        // create 2th point of way
        MKPlacemark *studentPlacemark = [[MKPlacemark alloc] initWithCoordinate:coordinate];
        MKMapItem *studentMapItem = [[MKMapItem alloc] initWithPlacemark:studentPlacemark];
        request.destination = studentMapItem;
        
        request.transportType = MKDirectionsTransportTypeAutomobile;
        request.requestsAlternateRoutes = YES;
        
        // create direction (way)
        self.directions = [[MKDirections alloc] initWithRequest:request];
        
        [self.directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse * _Nullable response, NSError * _Nullable error) {
            
            if (error) {
                [self showAlertWithTitle:@"Error" andMessage:[error localizedDescription]];
            } else if (response.routes == 0) {
                [self showAlertWithTitle:@"Error" andMessage:@"No routes found"];
            } else {
                
                [self.mapView removeOverlays:self.mapView.overlays];
                
                NSMutableArray *polylinesArray = [NSMutableArray array];
                
                for (MKRoute *route in response.routes) {
                    [polylinesArray addObject:route.polyline];
                }
                [self.mapView addOverlays:polylinesArray level:MKOverlayLevelAboveRoads];
                
                CGFloat minimalDistance = [response.routes firstObject].distance;
                CGFloat minimalTimeTravelCar = [response.routes firstObject].expectedTravelTime;
                
                for (MKRoute *route in response.routes) {
                    
                    CGFloat tmpDistance = MIN(minimalDistance, route.distance);
                    minimalDistance = tmpDistance;
                    
                    CGFloat tmpTimeTravelCar = MIN(minimalTimeTravelCar, route.expectedTravelTime);
                    minimalTimeTravelCar = tmpTimeTravelCar;
                }
                
                [self showAlertWithTitle:@"Distance"
                    andMessage:[NSString stringWithFormat:@"MIN distance = %.0f meter(s)\nExpected travel time by car = %.1f min", minimalDistance, minimalTimeTravelCar/60]];
            }
        }];
    }
}

- (IBAction)allGoMeetPlaceAction:(UIBarButtonItem *) sender {
    
    if ([self.directions isCalculating]) {
        [self.directions cancel];
    }
    
    [self.mapView removeOverlays:self.mapView.overlays];
    
    if ([self.mapView.annotations containsObject:self.meetPlace]) {
        
        MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
        
        // create 1th point of way
        MKPlacemark *meetPlacemark = [[MKPlacemark alloc] initWithCoordinate:self.meetPlace.coordinate];
        MKMapItem *meetMapItem = [[MKMapItem alloc] initWithPlacemark:meetPlacemark];
        request.source = meetMapItem;
        
        // create 2th point of way
        
        MKCircleRenderer *circleRender = [[MKCircleRenderer alloc] initWithCircle:self.circle3500m];
        
        for (ABStudent *student in self.studentsArray) {
            
            if ([self overlay:circleRender containsPoint:student.coordinate]) {
                
                MKPlacemark *studentPlacemark = [[MKPlacemark alloc] initWithCoordinate:student.coordinate];
                MKMapItem *studentMapItem = [[MKMapItem alloc] initWithPlacemark:studentPlacemark];
                request.destination = studentMapItem;
                
                request.transportType = MKDirectionsTransportTypeAutomobile;
                request.requestsAlternateRoutes = NO;
                
                // create direction (way)
                self.directions = [[MKDirections alloc] initWithRequest:request];
                
                [self.directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse * _Nullable response, NSError * _Nullable error) {
                    
                    if (error) {
                        [self showAlertWithTitle:@"Error" andMessage:[error localizedDescription]];
                    } else if (response.routes == 0) {
                        [self showAlertWithTitle:@"Error" andMessage:@"No routes found"];
                        
                    } else {
                        
                        [self.mapView addOverlay:[response.routes firstObject].polyline
                                           level:MKOverlayLevelAboveRoads];
                    }
                }];
            }
        }
    }
}


- (IBAction)addMeetPlaceAction:(UIBarButtonItem *)sender {
    
    [self.mapView removeOverlays:self.mapView.overlays];
    
    self.meetPlace.coordinate = self.mapView.region.center;
    self.meetPlace.title = @"Meet Place";
    
    [self.mapView addAnnotation:self.meetPlace];
    
    [self createThreeCircles];
    
    [UIView animateWithDuration:1.5f
                     animations:^{
             self.infoCountView.alpha = 1.f;
         }
         completion:^(BOOL finished) {
             [UIView animateWithDuration:1.f
                  animations:^{
                      self.infoCountView.alpha = 0.5f;
                  }];
         }];
}

- (void) showAlertWithTitle:(NSString *) title andMessage:(NSString *)message {
    
    UIAlertController *alert = [UIAlertController
                                alertControllerWithTitle:title
                                message:message
                                preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Segues

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"showInfoTVC"]){
        
        ABInfoTVC *infoTVC = (ABInfoTVC *)segue.destinationViewController;

        // transfer data
        infoTVC.studentCurrentAddress = self.studentCurrentAddress;
        infoTVC.studentName = self.studentName;
        infoTVC.studentDateOfBirth = self.studentDateOfBirth;
        infoTVC.studentGender = self.studentGender;
        infoTVC.infoAddressDictionary = self.infoAddressDictionary;
    }
}


#pragma mark - Reduce methods

- (MKCircle *) createCircleWithRadius:(CGFloat) radiusMeters andCoordinate:(CLLocationCoordinate2D)coordinate2D {
    
    self.countInCircle = 0;
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:coordinate2D radius:radiusMeters];
    
    [self.mapView addOverlay:circle level:MKOverlayLevelAboveRoads];

    MKCircleRenderer *circleRender = (MKCircleRenderer *) [self.mapView rendererForOverlay:circle];
    
    for (ABStudent *student in self.studentsArray) {
        
        [self overlay:circleRender containsPoint:student.coordinate];
        
        if ([self overlay:circleRender containsPoint:student.coordinate]) {
            self.countInCircle++;
        }
    }
    
    return circle;
}

- (BOOL) overlay:(MKOverlayPathRenderer *)overlay containsPoint:(CLLocationCoordinate2D)point {
    
    CGPoint overlayPoint = [overlay pointForMapPoint:MKMapPointForCoordinate(point)];
    
    BOOL overlayContainsPoint = CGPathContainsPoint(overlay.path, NULL, overlayPoint, NO);
    
    return overlayContainsPoint;
}

- (void) createThreeCircles {
    
    [self createCircleWithRadius:1500 andCoordinate:self.meetPlace.coordinate];
    self.circle1Label.text = [NSString stringWithFormat:@"Circle 1: %li persons", self.countInCircle];
    [self createCircleWithRadius:2500 andCoordinate:self.meetPlace.coordinate];
    self.circle2Label.text = [NSString stringWithFormat:@"Circle 2: %li persons", self.countInCircle];
    [self createCircleWithRadius:3500 andCoordinate:self.meetPlace.coordinate];
    self.circle3Label.text = [NSString stringWithFormat:@"Circle 3: %li persons", self.countInCircle];
    
    self.circle3500m = [MKCircle circleWithCenterCoordinate:
    [self createCircleWithRadius:3500 andCoordinate:self.meetPlace.coordinate].coordinate
    radius:[self createCircleWithRadius:3500 andCoordinate:self.meetPlace.coordinate].radius];
}






@end
