//
//  ABInfoTVC.m
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 24.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABInfoTVC.h"

@interface ABInfoTVC ()

@end

@implementation ABInfoTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameLabel.text = self.studentName;
    self.bithDateLabel.text = self.studentDateOfBirth;
    self.addressLabel.text = self.studentCurrentAddress;
    self.cityLabel.text = [self.infoAddressDictionary objectForKey:@"City"];
    self.countryLabel.text = [self.infoAddressDictionary objectForKey:@"Country"];
    
    [self.studentGender isEqualToString:@"Man"] ? (self.imageStudent.image = [UIImage imageNamed:@"superman.jpg"]) : (self.imageStudent.image = [UIImage imageNamed:@"wonder_woman.jpeg"]);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
