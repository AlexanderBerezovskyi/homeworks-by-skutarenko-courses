//
//  ABStudent.m
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 23.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

static NSString *firstNameMen[] = {@"Georgan", @"Oigenhein", @"Denya", @"Sergio", @"Arslanito", @"Igorio"};
static NSString *firstNameWomen[] = {@"Lina", @"Kira", @"Maria", @"Olga", @"Karyna", @"Irina", @"Albina", @"Georgina"};
static NSString *lastNames[] = {@"Petrov", @"Ivanov", @"Sydorov", @"Mamontov", @"Vetrov", @"Vorotov", @"Gringo", @"Logman", @"Sisilio"};


@interface ABStudent ()


@end


@implementation ABStudent

+ (ABStudent *) randomStudent {
    
    ABStudent *student = [[ABStudent alloc] init];
    
    student.gender = arc4random_uniform(100) > 50 ? ABGenderMan : ABGenderWoman;
    
    student.firstName = (student.gender == ABGenderMan) ?
    firstNameMen[arc4random_uniform(6)] : firstNameWomen[arc4random_uniform(8)];
    
    student.lastName  = lastNames[arc4random_uniform(9)];
    student.birthDate = [ABStudent randomDateOfBirthBetween:18 and:40];
    student.currentCoordinate =
    CLLocationCoordinate2DMake(48.41462 + (CGFloat) arc4random_uniform(1000)/10000, 34.993098 + (CGFloat) arc4random_uniform(1000)/10000);
    
    return student;
}

+ (NSDate *) randomDateOfBirthBetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld {
    
    NSDate      *currentDate = [NSDate date];
    NSCalendar  *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsFromCurrentDate = [currentCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:currentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] - secondYearOld];
    NSDate *dateSecondYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] + secondYearOld - firstYearOld];
    NSDate *dateFirstYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    NSDateComponents *componentsBetweenFirstAndSecontYearsOld = [currentCalendar components:NSCalendarUnitSecond fromDate:dateSecondYearsAgo toDate:dateFirstYearsAgo options:NO];
    
    NSInteger betweenFirstAndSecond = [componentsBetweenFirstAndSecontYearsOld second];
    
    
    NSDate *dateOfBirth = [NSDate dateWithTimeInterval:arc4random() % betweenFirstAndSecond
                                             sinceDate:dateSecondYearsAgo];
    
    return dateOfBirth;
}

- (NSString *) randomDateInStringAmericanFormatForDate:(NSDate *)currentDate {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM/dd/yyyy"];
    
    NSString *randomDateString = [dateFormat stringFromDate:currentDate];
    
    return randomDateString;
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"%@ %@ %@", self.lastName, self.firstName, [self randomDateInStringAmericanFormatForDate:self.birthDate]];
}




@end
