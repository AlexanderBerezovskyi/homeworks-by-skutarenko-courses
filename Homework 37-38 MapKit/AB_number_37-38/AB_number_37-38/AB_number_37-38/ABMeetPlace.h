//
//  ABMeetPlace.h
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 25.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

@interface ABMeetPlace : NSObject <MKAnnotation>

// MKAnnotation protocol
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end
