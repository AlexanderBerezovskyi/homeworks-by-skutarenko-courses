//
//  ABStudent.h
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 23.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MKAnnotation.h>

typedef enum {
    
    ABGenderMan,
    ABGenderWoman
    
}ABGender;

@interface ABStudent : NSObject <MKAnnotation>

// MKAnnotation protocol
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

// Student
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSDate *birthDate;
@property (assign, nonatomic) ABGender gender;

@property (assign, nonatomic) CLLocationCoordinate2D currentCoordinate;

+ (ABStudent *) randomStudent;
+ (NSDate *) randomDateOfBirthBetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld;
- (NSString *) randomDateInStringAmericanFormatForDate:(NSDate *)currentDate;

@end
