//
//  ViewController.m
//  AB_number_19
//
//  Created by Alexander Berezovskyy on 21.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray *darkSquaresArray;
@property (nonatomic, strong) NSMutableArray *redCheckersArray;
@property (nonatomic, strong) NSMutableArray *greenCheckersArray;
@property (nonatomic, strong) UIView *checkerBoardView;

@property (nonatomic, strong) NSMutableArray *allCheckersCentersArray;
@property (nonatomic, strong) NSMutableArray *allCheckersArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.checkerBoardView = [[UIView alloc] init];
    
    self.darkSquaresArray = [NSMutableArray array];
    self.redCheckersArray = [NSMutableArray array];
    self.greenCheckersArray = [NSMutableArray array];
    self.allCheckersCentersArray = [NSMutableArray array];
    self.allCheckersArray = [NSMutableArray new];
    
    self.view.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:1.f];
    
    CGFloat viewWidth = CGRectGetWidth(self.view.bounds);
    CGFloat viewHeight = CGRectGetHeight(self.view.bounds);
    
    // Checker board size
    
    CGFloat checkerBoardSize = MIN(viewWidth, viewHeight);
    
    CGRect checkerBoard = CGRectMake((CGRectGetWidth(self.view.bounds) - checkerBoardSize) / 2,
                                     (CGRectGetHeight(self.view.bounds) - checkerBoardSize) / 2,
                                     checkerBoardSize,
                                     checkerBoardSize);

    self.checkerBoardView = [[UIView alloc] initWithFrame:checkerBoard];
    
    self.checkerBoardView.backgroundColor = [[UIColor alloc] initWithRed:(float) 255/255 green: (float) 228/255 blue:(float) 225/255 alpha:1];
    self.checkerBoardView.layer.borderWidth = 3.f;
    self.checkerBoardView.layer.shadowOpacity = 10.f;
    self.checkerBoardView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.checkerBoardView.layer.shadowColor = [UIColor whiteColor].CGColor;

    self.checkerBoardView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    [self.view addSubview:self.checkerBoardView];
    
    CGFloat squareSize = checkerBoardSize / 8;
    CGFloat checkerSize = squareSize * 0.75;
    
    for (int i = 0; i < 8; i++) {
        
        for (int j = 0; j < 8; j++) {
            
            if (((i % 2 == 1) && (j % 2 == 1)) || ((i % 2 == 0) && (j % 2 == 0))) {
            
                CGRect rectTmp = CGRectMake(i * squareSize, j * squareSize, squareSize, squareSize);
                
                UIView *blackSquareView = [[UIView alloc] initWithFrame:rectTmp];
                
                blackSquareView.backgroundColor = [[UIColor alloc] initWithRed:(float) 198/256
                                                                         green:(float) 166/255
                                                                          blue:(float) 100/255
                                                                         alpha:1];
                
                [self.checkerBoardView addSubview:blackSquareView];
                [self.darkSquaresArray addObject:blackSquareView];
                                
                // Create checkers
                
                UIView *checkerView = [[UIView alloc] initWithFrame:
                                       CGRectMake((squareSize - checkerSize) / 2 + i * squareSize,
                                                  (squareSize - checkerSize) / 2 + j * squareSize,
                                                  checkerSize,
                                                  checkerSize)];
                
                checkerView.layer.cornerRadius = checkerSize/2;
                checkerView.layer.shadowColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f].CGColor;
                checkerView.layer.shadowOpacity = 7.f;

                // Paint checkers in Red
                
                if (j < 3) {
                    
                    checkerView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.9f];
                    [self.checkerBoardView addSubview:checkerView];
                    [self.redCheckersArray addObject:checkerView];
                    [self.allCheckersArray addObject:checkerView];
                    
                    NSString *centerOfRedCheckerView = NSStringFromCGPoint(checkerView.center);
                    [self.allCheckersCentersArray addObject:centerOfRedCheckerView];
                
                // Paint checkers in Green
                    
                } else if (j > 4) {
                    
                    checkerView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.9f];
                    [self.checkerBoardView addSubview:checkerView];
                    [self.greenCheckersArray addObject:checkerView];
                    [self.allCheckersArray addObject:checkerView];
                    
                    NSString *centerOfGreenCheckerView = NSStringFromCGPoint(checkerView.center);
                    [self.allCheckersCentersArray addObject:centerOfGreenCheckerView];
                }
            }
        }
    }
}


#pragma mark - Autoresizing & Orientation of Views

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskAll;
}

- (void) viewWillTransitionToSize:(CGSize)size
        withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    UIColor *colorChange = [[UIColor alloc] initWithRed:(float) arc4random_uniform(256)/255
                                                  green:(float) arc4random_uniform(256)/255
                                                   blue:(float) arc4random_uniform(256)/255
                                                  alpha:1];
    
    for (UIView *v in self.darkSquaresArray) {
        
        v.backgroundColor = colorChange;
    }
    
    [UIView animateWithDuration:2 animations:^{
        
        for (int i = 0; i < [self.allCheckersArray count]; i++) {
            
//            Move checker to random free place
            
            UIView *checker = [self.allCheckersArray objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:checker];
            
            NSString *checkerString = NSStringFromCGPoint(checker.center); // <<-- current place of checker
            
            UIView *checkerGoTo =
            [self.darkSquaresArray objectAtIndex:arc4random_uniform(32)]; // <<-- checker go to
            
            NSString *checkerGoToString = NSStringFromCGPoint(checkerGoTo.center);
            
            if (![self.allCheckersCentersArray containsObject:checkerGoToString]) {
                
                [self.allCheckersCentersArray removeObject:checkerString];
                checker.center = checkerGoTo.center;
                [self.allCheckersCentersArray addObject:checkerGoToString];
            }
            
//            Change red and green
            
            /*
            UIView *redChecker = [self.redCheckersArray objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:redChecker];
            
            UIView *greenChecker = [self.greenCheckersArray objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:greenChecker];
            
            CGPoint tmpPoint = redChecker.center;
            
            redChecker.center = greenChecker.center;
            
            greenChecker.center = tmpPoint;
            */
            
            
//            Random change red and green checkers
            /*
            UIView *redChecker = [self.redCheckersArray objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:redChecker];

            UIView *greenChecker = [self.greenCheckersArray objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:greenChecker];

            CGPoint tmpPoint = redChecker.center;
            
            UIView *randomGreenChecker = [self.greenCheckersArray objectAtIndex:arc4random_uniform(12)];
                
            redChecker.center = randomGreenChecker.center;
            
            randomGreenChecker.center = tmpPoint;
             
             */
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





@end
