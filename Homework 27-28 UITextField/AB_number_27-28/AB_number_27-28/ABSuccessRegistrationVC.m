//
//  SuccessRegistrationVC.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 11.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABSuccessRegistrationVC.h"

@interface ABSuccessRegistrationVC ()

@end

@implementation ABSuccessRegistrationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"background.jpg"]];
    
    self.successRegistrationLabel.text = self.firstAndLastNames;
    
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
