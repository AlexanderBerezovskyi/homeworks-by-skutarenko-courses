//
//  ABStudentOperation.h
//  AB_number_13
//
//  Created by Alexander Berezovskyy on 13.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^ResultBlock)(NSString *name, CGFloat finishTime);

@interface ABStudentOperation : NSObject

@property (nonatomic, strong) NSString *name;

#pragma mark - Initialization

- (id)initWithName:(NSString *) name;


#pragma mark - Find Number

- (void) guessNumber:(NSUInteger) number inSpanFromZeroTo:(NSUInteger) span withResultBlock:(ResultBlock) resultBlock;

@end
