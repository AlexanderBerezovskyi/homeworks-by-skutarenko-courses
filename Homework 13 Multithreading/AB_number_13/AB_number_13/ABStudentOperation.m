//
//  ABStudentOperation.m
//  AB_number_13
//
//  Created by Alexander Berezovskyy on 13.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudentOperation.h"

@implementation ABStudentOperation

#pragma mark - Initialization

- (id)initWithName:(NSString *) name
{
    self = [super init];
    if (self) {
        self.name = name;
    }
    return self;
}


#pragma mark - Find Number

- (void) guessNumber:(NSUInteger) number inSpanFromZeroTo:(NSUInteger) span withResultBlock:(ResultBlock) resultBlock {
    
    NSOperationQueue *queueStudents = [ABStudentOperation staticOperationQueue];
    
    [queueStudents addOperationWithBlock:^{
        NSUInteger studentNumber = 0;
        CGFloat startTime = CACurrentMediaTime();
        NSLog(@"%@ started", self.name);
        
        do {
            studentNumber = arc4random() % span;
        } while (studentNumber != number);
        
        CGFloat finishTime = CACurrentMediaTime() - startTime;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            resultBlock (self.name, finishTime);
        });
    }];
    
    NSLog(@"%li", [queueStudents operationCount]);
    
//    [queueStudents waitUntilAllOperationsAreFinished];
}


#pragma mark - Static Method

+ (NSOperationQueue *) staticOperationQueue {
    
    static NSOperationQueue *operationQueue = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        operationQueue = [[NSOperationQueue alloc] init];
    });
    
    return operationQueue;
}

@end
