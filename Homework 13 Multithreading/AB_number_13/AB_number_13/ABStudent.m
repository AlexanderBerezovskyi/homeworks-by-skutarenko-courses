//
//  ABStudent.m
//  AB_number_13
//
//  Created by Alexander Berezovskyy on 10.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

#pragma mark - Initialization

- (id)initWithName:(NSString *) name
{
    self = [super init];
    if (self) {
        self.name = name;
    }
    return self;
}


#pragma mark - Find Number

- (void) guessNumber:(NSUInteger) number inSpanFromZeroTo:(NSUInteger) span {
    
    dispatch_async ([ABStudent staticQueue], ^{
        
        NSUInteger studentNumber = 0;
        CGFloat startTime = CACurrentMediaTime();
        NSLog(@"%@ started", self.name);
        
        do {
            studentNumber = arc4random() % span;
        } while (studentNumber != number);
        
        CGFloat finishTime = CACurrentMediaTime() - startTime;
        
        NSLog(@"%@ won after %.5f sec.", self.name, finishTime);
    });
}

- (void) guessNumber:(NSUInteger) number inSpanFromZeroTo:(NSUInteger) span withResultBlock:(ResultBlock) resultBlock {
    
    dispatch_async ([ABStudent staticQueue], ^{
        
        NSUInteger studentNumber = 0;
        CGFloat startTime = CACurrentMediaTime();
        NSLog(@"%@ started", self.name);
        
        do {
            studentNumber = arc4random() % span;
        } while (studentNumber != number);
        
        CGFloat finishTime = CACurrentMediaTime() - startTime;
        
        dispatch_async(dispatch_get_main_queue(), ^{
        
            resultBlock (self.name, finishTime);
        });
    });
}


#pragma mark - Static Method

+ (dispatch_queue_t) staticQueue {
    
    static dispatch_queue_t queue;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        queue = dispatch_queue_create("aberezovskyy.com.staticQueue", DISPATCH_QUEUE_CONCURRENT);
    });
    
    return queue;
}




















@end
