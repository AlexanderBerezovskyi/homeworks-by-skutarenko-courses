//
//  AppDelegate.m
//  AB_number_13
//
//  Created by Alexander Berezovskyy on 10.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABStudent.h"
#import "ABStudentOperation.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    ABStudent *student1 = [[ABStudent alloc] initWithName:@"Student_1"];
//    ABStudent *student2 = [[ABStudent alloc] initWithName:@"Student_2"];
//    ABStudent *student3 = [[ABStudent alloc] initWithName:@"Student_3"];
//    ABStudent *student4 = [[ABStudent alloc] initWithName:@"Student_4"];
//    ABStudent *student5 = [[ABStudent alloc] initWithName:@"Student_5"];
    
//    NSArray *studentsArray = [[NSArray alloc] initWithObjects:student1, student2, student3, student4, student5, nil];
    
//    for (ABStudent *st in studentsArray) {
    
        // ***Level Pupil***
//        [st guessNumber:4 inSpanFromZeroTo:10000000];
        
        
        // ***Level Student***
//        [st guessNumber:7 inSpanFromZeroTo:100000000 withResultBlock:^(NSString *name, CGFloat finishTime) {
//            
//            NSLog(@"%@ won after %.5f sec.", st.name, finishTime);
//        }];
//    }
    
    ABStudentOperation *studentOp1 = [[ABStudentOperation alloc] initWithName:@"St1"];
    ABStudentOperation *studentOp2 = [[ABStudentOperation alloc] initWithName:@"St2"];
    ABStudentOperation *studentOp3 = [[ABStudentOperation alloc] initWithName:@"St3"];
    ABStudentOperation *studentOp4 = [[ABStudentOperation alloc] initWithName:@"St4"];
    ABStudentOperation *studentOp5 = [[ABStudentOperation alloc] initWithName:@"St5"];
    
    NSArray *studentsArrayOp = [[NSArray alloc] initWithObjects:studentOp1, studentOp2, studentOp3,
                                studentOp4, studentOp5, nil];
    
        // ***Level Superman***
    for (ABStudentOperation *st in studentsArrayOp) {
        
        [st guessNumber:9 inSpanFromZeroTo:100000000 withResultBlock:^(NSString *name, CGFloat finishTime) {
            
            NSLog(@"%@ won after %.5f sec.", st.name, finishTime);
        }];
    }
        
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
