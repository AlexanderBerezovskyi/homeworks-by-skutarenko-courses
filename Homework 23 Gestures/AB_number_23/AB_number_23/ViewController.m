//
//  ViewController.m
//  AB_number_23
//
//  Created by Alexander Berezovskyy on 09.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, weak) UIImageView *deerImageView;

@property (nonatomic, assign) CGFloat totalSwipeValue;
@property (nonatomic, assign) CGFloat totalScaleValue;
@property (nonatomic, assign) CGFloat totalRotationValue;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Create UIImageView
    
    UIImageView *deerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    [self.view addSubview:deerImageView];
    
    UIView *backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"10.jpg"]];
    
    [self.view addSubview:backgroundView];
    
    UIImage *deerImage1 = [UIImage imageNamed:@"1.png"];
    UIImage *deerImage2 = [UIImage imageNamed:@"2.png"];
    UIImage *deerImage3 = [UIImage imageNamed:@"3.png"];
    UIImage *deerImage4 = [UIImage imageNamed:@"4.png"];
    UIImage *deerImage5 = [UIImage imageNamed:@"5.png"];
    UIImage *deerImage6 = [UIImage imageNamed:@"6.png"];
    UIImage *deerImage7 = [UIImage imageNamed:@"7.png"];
    
    NSArray *deerImagesArray = [[NSArray alloc] initWithObjects:deerImage1, deerImage2, deerImage3, deerImage4, deerImage5, deerImage6, deerImage7, nil];
    deerImageView.animationImages = deerImagesArray;
    deerImageView.animationDuration = 1.f;
    
    [deerImageView startAnimating];
    
    self.deerImageView = deerImageView;
    [self.view bringSubviewToFront:self.deerImageView];
    
    // Tap
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(handleTap:)];
    
    [self.view addGestureRecognizer:tapGesture];
    
    // Right Swipe
    
    UISwipeGestureRecognizer *rightSwipeGesture = [[UISwipeGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(handleRightSwipe:)];
    
    rightSwipeGesture.direction = UISwipeGestureRecognizerDirectionRight;
    
    [self.view addGestureRecognizer:rightSwipeGesture];
    
    // Left Swipe
    
    UISwipeGestureRecognizer *leftSwipeGesture = [[UISwipeGestureRecognizer alloc]
                                                   initWithTarget:self
                                                   action:@selector(handleLeftSwipe:)];
    
    leftSwipeGesture.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self.view addGestureRecognizer:leftSwipeGesture];
    
    // Double Tap
    
    UITapGestureRecognizer *doubleTapGesture = [[UITapGestureRecognizer alloc]
                                          initWithTarget:self
                                          action:@selector(handleDoubleTap:)];
    
    doubleTapGesture.numberOfTapsRequired = 2;
    doubleTapGesture.numberOfTouchesRequired = 2;
    
    [self.view addGestureRecognizer:doubleTapGesture];
    
    // Pinch
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc]
                                              initWithTarget:self
                                              action:@selector(handlePinch:)];
    
    [self.view addGestureRecognizer:pinchGesture];
    
    pinchGesture.delegate = self;
    
    // Rotation
    
    UIRotationGestureRecognizer *rotationGesture = [[UIRotationGestureRecognizer alloc]
                                                    initWithTarget:self
                                                    action:@selector(handleRotation:)];
    
    [self.view addGestureRecognizer:rotationGesture];
    
    rotationGesture.delegate = self;
}


#pragma mark - UIGestureRecognizer

- (void) handleTap:(UITapGestureRecognizer *) tapGesture {
    
    [UIView animateWithDuration:3.f
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.deerImageView.center = [tapGesture locationInView:self.view];
                     }
                     completion:^(BOOL finished) {
                         
                     }];
}

- (void) handleRightSwipe:(UISwipeGestureRecognizer *) rightSwipeGesture {
    
    [self rotationView:self.deerImageView rotateClockwise:NO];
}

- (void) handleLeftSwipe:(UISwipeGestureRecognizer *) leftSwipeGesture {
    
    [self rotationView:self.deerImageView rotateClockwise:YES];
}

- (void) handleDoubleTap:(UITapGestureRecognizer *) tapGesture {
    
    if ([self.deerImageView isAnimating]) {
        
        [self.deerImageView stopAnimating];
        self.deerImageView.image = [UIImage imageNamed:@"1.png"];
        
    } else {
        [self.deerImageView startAnimating];
    }
}

- (void) handlePinch:(UIPinchGestureRecognizer *) pinchGesture {
    
    if (pinchGesture.state == UIGestureRecognizerStateBegan) {
        self.totalScaleValue = 1.f;
    }
    
    CGFloat newScale = 1.0 + pinchGesture.scale - self.totalScaleValue;
    
    CGAffineTransform currentTransform = self.deerImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformScale(currentTransform, newScale, newScale);
    
    self.deerImageView.transform = newTransform;
    
    self.totalScaleValue = pinchGesture.scale;
}

- (void) handleRotation:(UIRotationGestureRecognizer *) rotationGesture {
    
    if (rotationGesture.state == UIGestureRecognizerStateBegan) {
        self.totalRotationValue = 0;
    }
    
    CGFloat newRotation = rotationGesture.rotation - self.totalRotationValue;
    
    CGAffineTransform currentTransform = self.deerImageView.transform;
    CGAffineTransform newTransform = CGAffineTransformRotate(currentTransform, newRotation);
    
    self.deerImageView.transform = newTransform;
    self.totalRotationValue= rotationGesture.rotation;
}

#pragma  mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
    return YES;
}


#pragma mark - Reduce Methods

- (void) rotationView:(UIView *) currentView rotateClockwise:(Boolean) clockwise {
    
    CGFloat totalRotateValue = clockwise ? 3.14f : -3.14f;
    self.totalSwipeValue = totalRotateValue;
    
    [UIView animateWithDuration:1.f
                          delay:0
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [self rotateDeer];
                         
                     } completion:^(BOOL finished) {
                         
         [UIView animateWithDuration:1.f
                               delay:0
                             options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionBeginFromCurrentState
                          animations:^{
                              
                              [self rotateDeer];
                              
                          }
                          completion:^(BOOL finished) {
                              
                          }];
                     }];
}

- (void) rotateDeer {
    
    CGAffineTransform currentDeerState = self.deerImageView.transform;
    CGAffineTransform deerRotation = CGAffineTransformRotate(currentDeerState, self.totalSwipeValue);
    self.deerImageView.transform = deerRotation;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
