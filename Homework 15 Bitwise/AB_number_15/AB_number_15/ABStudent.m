//
//  ABStudent.m
//  AB_number_15
//
//  Created by Alexander Berezovskyy on 15.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.name = @"no name";
    }
    return self;
}

- (instancetype)initWithName:(NSString *)name
{
    self = [super init];
    if (self) {
        self.name = name;
    }
    return self;
}


#pragma mark - Description

- (NSString *) description {
    
    return [NSString stringWithFormat:@"%@ studies:\n"
            "math -         %@ \n"
            "engineering -  %@ \n"
            "programming -  %@ \n"
            "literature -   %@ \n"
            "biology -      %@ \n"
            "psycology -    %@ \n"
            "art -          %@ \n",
            self.name,  [self answerByTypeSubject:ABStudentSubjectTypeMath],
                        [self answerByTypeSubject:ABStudentSubjectTypeEngineering],
                        [self answerByTypeSubject:ABStudentSubjectTypeProgramming],
                        [self answerByTypeSubject:ABStudentSubjectTypeLiterature],
                        [self answerByTypeSubject:ABStudentSubjectTypeBiology],
                        [self answerByTypeSubject:ABStudentSubjectTypePsycology],
                        [self answerByTypeSubject:ABStudentSubjectTypeArt]];
}


#pragma mark - Reduce Code Methods

- (NSString *) answerByTypeSubject:(ABStudentSubjectType) subjectType {
    
    return [NSString stringWithFormat:self.subjectType & subjectType ? @"Yes" : @"No"];
}



















@end
