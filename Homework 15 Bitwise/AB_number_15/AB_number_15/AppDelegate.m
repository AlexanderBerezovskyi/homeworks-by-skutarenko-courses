//
//  AppDelegate.m
//  AB_number_15
//
//  Created by Alexander Berezovskyy on 15.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABStudent.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSMutableArray *studentsArray = [NSMutableArray new];
    
    NSInteger countOfSubject = 7;
    
    for (int i = 0; i < 10; i++) {
        
        ABStudent *student = [[ABStudent alloc] initWithName:[NSString stringWithFormat:@"Student №%d", i+1]];
        
        student.subjectType = 0;
        
        for (int j = 0; j < countOfSubject; j++) {
            
            student.subjectType = student.subjectType |  arc4random_uniform(2) << j;

        }
       
        NSLog(@"%@", [student description]);
        [studentsArray addObject:student];
    }
    
    NSLog(@"%li", [studentsArray count]);
    
    NSMutableArray *techStudents = [NSMutableArray new];
    NSMutableArray *humanitiesStudents = [NSMutableArray new];
    NSMutableArray *learnProgramming = [NSMutableArray new];
    
    for (ABStudent *student in studentsArray) {
        
        if (student.subjectType & ABStudentSubjectTypeMath ||
            student.subjectType & ABStudentSubjectTypeEngineering ||
            student.subjectType & ABStudentSubjectTypeProgramming) {
            
            [techStudents addObject:student];
        }
        
        if (student.subjectType & ABStudentSubjectTypeLiterature ||
            student.subjectType & ABStudentSubjectTypeBiology ||
            student.subjectType & ABStudentSubjectTypePsycology ||
            student.subjectType & ABStudentSubjectTypeArt) {
            
            [humanitiesStudents addObject:student];
        }
        
        if (student.subjectType & ABStudentSubjectTypeProgramming) {
            
            [learnProgramming addObject:student];
        }
    }
    
    for (ABStudent *student in humanitiesStudents) {
        
        if (student.subjectType & ABStudentSubjectTypeBiology) {
            
            student.subjectType = student.subjectType & (~ABStudentSubjectTypeBiology);
            NSLog(@"%@ cancelled Biology", student.name);
            NSLog(@"%@", [student description]);
        }
    }
    
    for (ABStudent *student in techStudents) {
        NSLog(@"name = %@", student.name);
    }
    for (ABStudent *student in humanitiesStudents) {
        NSLog(@"name = %@", student.name);
    }

    NSLog(@"Programming learning %li students", [learnProgramming count]);
    

#pragma mark - ***Level Superman***
    
    // max int = 4 294 967 295 = 2^32 (32 bits)
    
    NSInteger randomNumber = 10;
    NSMutableString *binaryCode = [[NSMutableString alloc] init];
    NSInteger bitMask = 1;
    
    for (int i = 0; i < 32; i++) {
        
        (randomNumber & bitMask) ? [binaryCode insertString:@"1" atIndex:0] : [binaryCode insertString:@"0" atIndex:0];
        
        bitMask = bitMask << 1;

    }
    NSLog(@"Binary code = %@", binaryCode);

    
    
    
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
