//
//  ABStudent.h
//  AB_number_15
//
//  Created by Alexander Berezovskyy on 15.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {

    ABStudentSubjectTypeMath         = 1 << 0,
    ABStudentSubjectTypeEngineering  = 1 << 1,
    ABStudentSubjectTypeProgramming  = 1 << 2,
    ABStudentSubjectTypeLiterature   = 1 << 3,
    ABStudentSubjectTypeBiology      = 1 << 4,
    ABStudentSubjectTypePsycology    = 1 << 5,
    ABStudentSubjectTypeArt          = 1 << 6
    
} ABStudentSubjectType;

@interface ABStudent : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) ABStudentSubjectType subjectType;


#pragma mark - Initialization

- (instancetype)initWithName:(NSString *)name;





















@end
