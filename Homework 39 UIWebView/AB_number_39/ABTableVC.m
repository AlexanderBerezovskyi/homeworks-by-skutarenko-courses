//
//  ABTableVC.m
//  AB_number_39
//
//  Created by Alexander Berezovskyy on 07.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABTableVC.h"
#import "ABWebVC.h"

@interface ABTableVC ()

@property (strong, nonatomic) NSString *referenceName;

@end

@implementation ABTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (IBAction)clickReferencesAction:(UIButton *)sender {
    
    self.referenceName = sender.titleLabel.text;
    [self performSegueWithIdentifier:@"showInfoInWebView" sender:sender];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if([segue.identifier isEqualToString:@"showInfoInWebView"]){
        
        ABWebVC *webVC = (ABWebVC *)segue.destinationViewController;
        webVC.referenceName = self.referenceName;
    }
}















@end
