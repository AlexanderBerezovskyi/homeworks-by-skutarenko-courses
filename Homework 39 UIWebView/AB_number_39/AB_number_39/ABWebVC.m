//
//  ABWebVC.m
//  AB_number_39
//
//  Created by Alexander Berezovskyy on 07.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABWebVC.h"

@interface ABWebVC () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *forwardButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadIndicator;

@end

@implementation ABWebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.webView.scalesPageToFit = YES;
        
    if ([self.referenceName hasPrefix:@"https://"]) {
        
        NSURL *selectedURL = [NSURL URLWithString:self.referenceName];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:selectedURL];
        
        [self.webView loadRequest:request];
    
    } else {
        
        self.webView.paginationMode = YES;
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:self.referenceName
                                                             ofType:nil];
        NSURL *currentURL = [NSURL fileURLWithPath:filePath];
        
        NSURLRequest *currentRequest = [NSURLRequest requestWithURL:currentURL];
        
        [self.webView loadRequest:currentRequest];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self.loadIndicator startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [self refreshToolbarButtons];
    [self.loadIndicator stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self refreshToolbarButtons];
}


#pragma mark - Actions

- (IBAction)goBackAction:(UIBarButtonItem *)sender {
    
    if ([self.webView canGoBack]) {
        [self.webView stopLoading];
        [self.webView goBack];
    }
}

- (IBAction)goForwardAction:(UIBarButtonItem *)sender {
    
    if ([self.webView canGoForward]) {
        [self.webView stopLoading];
        [self.webView goForward];
    }
}

- (IBAction)refreshWebViewAction:(UIBarButtonItem *)sender {
    [self.webView stopLoading];
    [self.webView reload];
}


#pragma mark - Reduce methods

- (void) refreshToolbarButtons {
    self.backButton.enabled = self.webView.canGoBack;
    self.forwardButton.enabled = self.webView.canGoForward;
}



@end
