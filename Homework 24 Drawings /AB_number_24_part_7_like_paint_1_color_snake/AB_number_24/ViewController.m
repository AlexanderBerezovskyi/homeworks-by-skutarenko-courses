//
//  ViewController.m
//  AB_number_24
//
//  Created by Alexander Berezovskyy on 11.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray *viewsArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.viewsArray = [NSMutableArray new];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITouch

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    CGPoint p = [touch locationInView:self.view];
    
    CGFloat rectSize = 80.f;
    
    UIView *newView = [[UIView alloc] initWithFrame:CGRectMake(p.x - rectSize / 2,
                                                               p.y - rectSize / 2,
                                                               rectSize, rectSize)];
    newView.backgroundColor = [self randomColor];
    newView.layer.cornerRadius = rectSize/2;
    
    [self.view addSubview:newView];
    [self.viewsArray addObject:newView];
}


#pragma mark - Color

- (UIColor *) randomColor {
    
    CGFloat r = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat g = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat b = (CGFloat) arc4random_uniform(256) / 255;
//    CGFloat a = (CGFloat) arc4random_uniform(51)  / 100;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:1.f];
}

@end
