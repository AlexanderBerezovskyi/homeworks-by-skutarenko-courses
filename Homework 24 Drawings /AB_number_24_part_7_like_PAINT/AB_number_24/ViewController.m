//
//  ViewController.m
//  AB_number_24
//
//  Created by Alexander Berezovskyy on 11.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@property (nonatomic, assign) CGPoint lastPoint;
@property (nonatomic,weak) IBOutlet UIImageView *drawImage;

@property (nonatomic, strong) UIColor *pencilColor;
@property (nonatomic, assign) CGFloat pencilSize;
@property (nonatomic, assign) CGFloat rectMinSide;

// Color buttons

- (IBAction)blueColorActionButton:(UIButton *)sender;
- (IBAction)redColorActionButton:(UIButton *)sender;
- (IBAction)greenColorActionButton:(UIButton *)sender;
- (IBAction)yellowColorActionButton:(UIButton *)sender;
- (IBAction)purpleColorActionButton:(UIButton *)sender;
- (IBAction)brownColorActionButton:(UIButton *)sender;
- (IBAction)magentaColorActionButton:(UIButton *)sender;
- (IBAction)cyanColorActionButton:(UIButton *)sender;
- (IBAction)blackColorActionButton:(UIButton *)sender;
- (IBAction)whiteColorActionButton:(UIButton *)sender;

// Line sizes buttons

- (IBAction)lineSize1ActionButton:(UIButton *)sender;
- (IBAction)lineSize2ActionButton:(UIButton *)sender;
- (IBAction)lineSize3ActionButton:(UIButton *)sender;
- (IBAction)lineSize4ActionButton:(UIButton *)sender;
- (IBAction)lineSize5ActionButton:(UIButton *)sender;

// Other buttons

- (IBAction)clearAllActionButton:(UIButton *)sender;
- (IBAction)saveImageActionButton:(UIButton *)sender;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat rectMinSide = MIN(CGRectGetWidth(self.drawImage.bounds),
                              CGRectGetHeight(self.drawImage.bounds));
    
    self.rectMinSide = rectMinSide;
    
    // Color
    
    self.pencilColor = [UIColor blackColor];
    
    // Font Size
    
    self.pencilSize = rectMinSide/100;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITouch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    
    self.lastPoint = [touch locationInView:self.view];
    
    [self drawInTouch:touch];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    [self drawInTouch:touch];
}

#pragma mark - Reduce Methods

- (void) drawInTouch:(UITouch *)currentTouch {
    
    CGPoint currentPoint = [currentTouch locationInView:self.view];
    
    // Create rect for painting
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    CGRect drawRect = CGRectMake(0, 0,
                                 self.view.frame.size.width,
                                 self.view.frame.size.height);
    
    [self.drawImage.image drawInRect:drawRect];
    
    // Settings for Pencil
    
    CGContextSetLineCap              (UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineWidth            (UIGraphicsGetCurrentContext(), self.pencilSize);
    CGContextSetStrokeColorWithColor (UIGraphicsGetCurrentContext(), self.pencilColor.CGColor);
    
    // Create lines between points
    
    CGContextBeginPath      (UIGraphicsGetCurrentContext());
    CGContextMoveToPoint    (UIGraphicsGetCurrentContext(), self.lastPoint.x, self.lastPoint.y);
    CGContextAddLineToPoint (UIGraphicsGetCurrentContext(), currentPoint.x, currentPoint.y);
    CGContextStrokePath     (UIGraphicsGetCurrentContext());
    
    self.drawImage.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.lastPoint = currentPoint;
}

#pragma mark - Actions

// Colors

- (IBAction)blueColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor blueColor];
}

- (IBAction)redColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor redColor];
}

- (IBAction)greenColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor greenColor];
}

- (IBAction)yellowColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor yellowColor];
}

- (IBAction)purpleColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor purpleColor];
}

- (IBAction)brownColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor brownColor];
}

- (IBAction)magentaColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor magentaColor];
}

- (IBAction)cyanColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor cyanColor];
}

- (IBAction)blackColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor blackColor];
}

- (IBAction)whiteColorActionButton:(UIButton *)sender {
    self.pencilColor = [UIColor whiteColor];
}

// Line sizes

- (IBAction)lineSize1ActionButton:(UIButton *)sender {
    self.pencilSize = self.rectMinSide/100;
}

- (IBAction)lineSize2ActionButton:(UIButton *)sender {
    self.pencilSize = self.rectMinSide/50;
}

- (IBAction)lineSize3ActionButton:(UIButton *)sender {
    self.pencilSize = self.rectMinSide/26;
}

- (IBAction)lineSize4ActionButton:(UIButton *)sender {
    self.pencilSize = self.rectMinSide/15;
}

- (IBAction)lineSize5ActionButton:(UIButton *)sender {
    self.pencilSize = self.rectMinSide/8;
}
- (IBAction)clearAllActionButton:(UIButton *)sender {
    self.drawImage.image = nil;
    self.drawImage.image = [UIImage imageNamed:@"papirus.jpg"];
}

- (IBAction)saveImageActionButton:(UIButton *)sender {

    UIGraphicsBeginImageContext(self.drawImage.bounds.size);
    [self.drawImage.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage* image1 = UIGraphicsGetImageFromCurrentImageContext();

    NSString *path = [NSString stringWithFormat:@"/Users/alexanderberezovskyy/Documents/Курсы_iOS/Скутаренко_ДЗ/Homework 24 Drawings /AB_number_24_part_7_like_PAINT/SaveImages/image%@.jpg", [NSDate date]];
    [UIImageJPEGRepresentation(image1, 1.0) writeToFile:path atomically:YES];
}

@end
