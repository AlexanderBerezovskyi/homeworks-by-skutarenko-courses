//
//  ABDrawingView.m
//  AB_number_24
//
//  Created by Alexander Berezovskyy on 13.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDrawingView.h"

@interface ABDrawingView ()

@property (nonatomic,strong) NSMutableArray *starPointsArray;

@end

@implementation ABDrawingView

- (void)drawRect:(CGRect)rect {
    
    for (int i = 0; i < 5; i++) {
        
        NSInteger starSize = 150;
        NSInteger rectWidth = CGRectGetWidth(rect);
        NSInteger rectHeight = CGRectGetHeight(rect);
        
        NSInteger pointX = starSize / 2 + arc4random()%(rectWidth - 2 * starSize);
        NSInteger pointY = starSize / 2 + arc4random()%(rectHeight - 2 * starSize);
        
        [self createStarInPoint:CGPointMake(pointX, pointY) withSize:starSize];
    }
}

#pragma mark - Create star with center in point

- (void) createStarInPoint:(CGPoint) starPoint withSize:(NSInteger)rectSize {
    
    self.starPointsArray = [NSMutableArray new];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Create big sircle
    
    CGContextSetStrokeColorWithColor(context, [self randomColor].CGColor);
    CGRect bigSquare = CGRectMake(starPoint.x, starPoint.y, rectSize, rectSize);
    CGContextAddEllipseInRect(context, bigSquare);
    CGContextStrokePath(context);
    
    // Sign points for star
    
    CGFloat circleWidth = 4.f;
    
    CGContextSetStrokeColorWithColor(context, [self randomColor].CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetLineWidth(context, circleWidth);
    
    CGContextSetFillColorWithColor(context, [self randomColor].CGColor);
    
    for (int i = 1; i <= 5; i++) {
        
        CGContextAddArc(context, CGRectGetMidX(bigSquare), CGRectGetMidY(bigSquare),
                        CGRectGetWidth(bigSquare) / 2,
                        3 * M_PI_2 - (2 * M_PI / 5) * i,
                        3 * M_PI_2 -  2 * M_PI / 5 + (2 * M_PI / 5) * i, NO);
        
        CGPoint circlePoint = CGContextGetPathCurrentPoint(context);
        
        CGContextStrokePath(context);
        
        [self.starPointsArray addObject:[NSValue valueWithCGPoint:circlePoint]];
    }
    
    // Fill circles in points of star
    
    CGFloat smallSquareSize = 30.f;
    
    for (int i = 0; i < 5; i++) {
        
        CGPoint circlePoint = [[self.starPointsArray objectAtIndex:i] CGPointValue];
        
        CGRect smallSquare = CGRectMake(circlePoint.x - smallSquareSize/2,
                                        circlePoint.y - smallSquareSize/2,
                                        smallSquareSize, smallSquareSize);
        CGContextAddEllipseInRect(context, smallSquare);
        
        CGContextFillPath(context);
    }
    
    // Create lines of star
    
    CGContextSetFillColorWithColor  (context, [self randomColor].CGColor);
    
    CGContextMoveToPoint(context, [[self.starPointsArray objectAtIndex:0] CGPointValue].x,
                         [[self.starPointsArray objectAtIndex:0] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:3] CGPointValue].x,
                            [[self.starPointsArray objectAtIndex:3] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:1] CGPointValue].x,
                            [[self.starPointsArray objectAtIndex:1] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:4] CGPointValue].x,
                            [[self.starPointsArray objectAtIndex:4] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:2] CGPointValue].x,
                            [[self.starPointsArray objectAtIndex:2] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:0] CGPointValue].x,
                            [[self.starPointsArray objectAtIndex:0] CGPointValue].y);
    
    CGContextFillPath(context);
    
    self.starPointsArray = nil;
}


#pragma mark - Random color

- (UIColor *) randomColor {
    
    CGFloat r = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat g = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat b = (CGFloat) arc4random_uniform(256) / 255;
    CGFloat a = (CGFloat) arc4random_uniform(51)  / 100;
    
    return [UIColor colorWithRed:r green:g blue:b alpha:(0.5f + a)];
}



@end
