//
//  ABDrawingView.m
//  AB_number_24
//
//  Created by Alexander Berezovskyy on 13.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDrawingView.h"

@interface ABDrawingView ()

@property (nonatomic,strong) NSMutableArray *starPointsArray;

@end

@implementation ABDrawingView


// For Ipad

- (void)drawRect:(CGRect)rect {
    
    NSInteger starSize = 500;
    NSInteger rectWidth = CGRectGetWidth(rect);
    NSInteger rectHeight = CGRectGetHeight(rect);
    
        NSInteger pointX = starSize / 2 + arc4random()%(rectWidth  - 2 * starSize);
        NSInteger pointY = starSize / 2 + arc4random()%(rectHeight - 2 * starSize);
        
        [self createStarInPoint:CGPointMake(pointX, pointY) withSize:starSize];
}

#pragma mark - Create star with center in point

- (void) createStarInPoint:(CGPoint) starPoint withSize:(NSInteger)rectSize {
    
    self.starPointsArray = [NSMutableArray new];
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // Create big sircle
    
    CGContextSetStrokeColorWithColor(context, [UIColor clearColor].CGColor);
    CGRect bigSquare = CGRectMake(starPoint.x, starPoint.y, rectSize, rectSize);
    CGContextAddEllipseInRect(context, bigSquare);
    CGFloat lineWidth = 6.f;
    CGContextSetLineWidth(context, lineWidth);
    CGContextStrokePath(context);
    
    // Sign points for star
    
    for (int i = 1; i <= 3; i++) {
        
        CGContextAddArc(context, CGRectGetMidX(bigSquare), CGRectGetMidY(bigSquare),
                        CGRectGetWidth(bigSquare) / 2,
                        3 * M_PI_2 - (2 * M_PI / 3) * i,
                        3 * M_PI_2 -  2 * M_PI / 3 + (2 * M_PI / 3) * i, NO);
        
        CGPoint circlePoint = CGContextGetPathCurrentPoint(context);
        
        CGContextStrokePath(context);
        
        [self.starPointsArray addObject:[NSValue valueWithCGPoint:circlePoint]];
    }
    
    // Create lines of star
    
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    CGContextSetLineCap(context, kCGLineCapRound);
    
    CGContextMoveToPoint(context, [[self.starPointsArray objectAtIndex:0] CGPointValue].x,
                                  [[self.starPointsArray objectAtIndex:0] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:1] CGPointValue].x,
                                     [[self.starPointsArray objectAtIndex:1] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:2] CGPointValue].x,
                                     [[self.starPointsArray objectAtIndex:2] CGPointValue].y);
    
    CGContextAddLineToPoint(context, [[self.starPointsArray objectAtIndex:0] CGPointValue].x,
                                     [[self.starPointsArray objectAtIndex:0] CGPointValue].y);
    
    CGContextStrokePath(context);
    
    // Create eye
    
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    CGRect littleSquare1 = CGRectMake(starPoint.x + 3 * rectSize / 8,
                                      starPoint.y + 3 * rectSize / 8,
                                      rectSize/4, rectSize/4);
    CGContextAddEllipseInRect(context, littleSquare1);
    CGContextStrokePath(context);
    
    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
    
    CGRect littleSquare2 = CGRectMake(starPoint.x + 5 * rectSize / 12,
                                      starPoint.y + 5 * rectSize / 12,
                                      rectSize/6, rectSize/6);
    CGContextAddEllipseInRect(context, littleSquare2);
    CGContextFillPath(context);
    
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    
    CGRect littleSquare3 = CGRectMake(starPoint.x + 7 * rectSize / 16,
                                      starPoint.y + 7 * rectSize / 16,
                                      rectSize/12, rectSize/12);
    CGContextAddEllipseInRect(context, littleSquare3);
    CGContextFillPath(context);
    
    
    CGContextSetStrokeColorWithColor(context, [UIColor blackColor].CGColor);
    
    CGContextAddArc(context, CGRectGetMidX(bigSquare), CGRectGetMinY(bigSquare) + CGRectGetHeight(bigSquare) / 4, CGRectGetHeight(bigSquare) * 3 / 8, 3 * M_PI_4 * 1.02f, M_PI_4 * 0.97f, YES);
    
    CGContextStrokePath(context);
    
    CGContextAddArc(context, CGRectGetMidX(bigSquare), CGRectGetMaxY(bigSquare) - CGRectGetHeight(bigSquare) / 4, CGRectGetHeight(bigSquare) * 3 / 8, 5 * M_PI_4 * 0.98f, 7 * M_PI_4 * 1.02f, NO);
    
    CGContextStrokePath(context);
    
    self.starPointsArray = nil;
}




@end
