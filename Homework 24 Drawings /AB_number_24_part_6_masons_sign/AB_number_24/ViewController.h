//
//  ViewController.h
//  AB_number_24
//
//  Created by Alexander Berezovskyy on 11.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ABDrawingView;

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet ABDrawingView *drawingView;

@end

