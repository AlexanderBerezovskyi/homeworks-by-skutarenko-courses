//
//  ViewController.h
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 03.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ABSuccessRegistrationVC.h"

@interface ViewController : UIViewController

// Text fields

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

// other buttons

@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *clearAllFieldsButton;

@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *informationTextFieldCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *littleLabelCollection;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *bigLabelCollection;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *otherButtonCollection;

// Actions

- (IBAction)actionTextFieldChanged:(UITextField *)sender;
- (IBAction)actionSubmitButton:(UIButton *)sender;
- (IBAction)actionClearAllButton:(UIButton *)sender;

// UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (void)textFieldDidEndEditing:(UITextField *)textField;
- (BOOL)textFieldShouldReturn:(UITextField *)textField;

// UITextField settings

- (NSString *) textField:(UITextField *)currentTextField setLength:(NSInteger)textLength;
- (NSString *) textField:(UITextField *)currentTextField validSet:(NSCharacterSet *)currentSet;

// Reduce Methods

- (NSCharacterSet *) unionCharacterSet:(NSCharacterSet *)characterSet withCharacters:(NSString *)characters;
- (void) littleLable:(UILabel *)littleLabel setText:(NSString *)labelText withColor:(UIColor *)labelTextColor;
- (void) changeTextField:(UITextField *)textField withMaxLength:(NSInteger)maxLength andChangeLabelText:(UILabel *)label;
- (NSInteger) countLittleCyanLabel;
- (void) nextFieldForChangeFromIndex:(NSInteger)textFieldIndex;
- (void) setPhoneNumberFormatForTextField:(UITextField *)currentTextField;
- (void) saveRegistrationData;

// Segues

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender;

// Borders & Colors

- (void) view:(UIView *)currentView setCornerRadius:(CGFloat)cornerRadius borderWidth:(CGFloat)borderWidth borderColor:(UIColor *)borderColor shadowOpacity:(CGFloat)shadowOpacity shadowColor:(UIColor *)shadowColor tintColor:(UIColor *)tintColor;









@end

