//
//  ABVideoVC.m
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 18.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABVideoVC.h"
#import <AVFoundation/AVFoundation.h>
#import <AVKit/AVKit.h>

@interface ABVideoVC () <UIPickerViewDelegate, UIPickerViewDataSource>

// video

@property (nonatomic, strong) AVPlayer *playerVideo;
@property (nonatomic, strong) NSMutableArray *videoFilesArray;
@property (nonatomic, strong) NSArray *videoFormatsArray;
@property (nonatomic, strong) NSString *selectedRowVideoPV;

@property (weak, nonatomic) IBOutlet UIPickerView *selectVideoPickerView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@end

@implementation ABVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // settings play button and picker view
    
    self.playButton.layer.borderWidth = self.selectVideoPickerView.layer.borderWidth = 2.f;
    self.playButton.layer.borderColor = self.selectVideoPickerView.layer.borderColor = [UIColor whiteColor].CGColor;

    // set video formats
    
    self.videoFormatsArray = [[NSArray alloc] initWithObjects:@"mp4", @"mov", @"m4v", @"3gp", nil];
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"cell_4.jpg"]];

    // Folder with videos
    
    NSString *videosFolderPath = @"/Users/alexanderberezovskyy/Documents/Курсы_iOS/Скутаренко_ДЗ/Homework 29 UITableView Static Cells/AB_number_27-28/Videos";
    
    NSArray* directory = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:videosFolderPath
                                                                             error:NULL];

    self.videoFilesArray = [[NSMutableArray alloc] init];
    
    [directory enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *filename = (NSString *)obj;

        NSString *extension = [[filename pathExtension] lowercaseString];
        
        if ([self.videoFormatsArray containsObject:extension]) {
            
            [self.videoFilesArray addObject:filename];
        }
    }];
    
    if (self.videoFilesArray.count != 0) {
        self.selectedRowVideoPV = [self.videoFilesArray objectAtIndex:0];
    }
}

- (void) viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    self.selectVideoPickerView = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (IBAction)videoAction:(UIButton *)sender {
    
    self.playerVideo = [AVPlayer playerWithURL:[NSURL fileURLWithPath:[NSString stringWithFormat: @"/Users/alexanderberezovskyy/Documents/Курсы_iOS/Скутаренко_ДЗ/Homework 29 UITableView Static Cells/AB_number_27-28/Videos/%@", self.selectedRowVideoPV]]];
    
    // create a player view controller
    
    AVPlayerViewController *controller = [[AVPlayerViewController alloc] init];
    [self presentViewController:controller animated:YES completion:nil];
    controller.player = self.playerVideo;
    [self.playerVideo play];
}


#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.videoFilesArray.count;
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectedRowVideoPV = [NSString stringWithFormat:@"%@", [self.videoFilesArray objectAtIndex:row]];
}


- (nullable NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *tmpString = [NSString stringWithFormat:@"%@", [self.videoFilesArray objectAtIndex:row]];
    
    NSAttributedString *tmpAttributedString = [[NSAttributedString alloc] initWithString:tmpString attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor],               NSFontAttributeName:[UIFont fontWithName:@"Papyrus-Condensed" size:10.f]}];
    
    return tmpAttributedString;
}


@end
