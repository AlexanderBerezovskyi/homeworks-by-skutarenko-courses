//
//  SuccessRegistrationVC.h
//  AB_number_27-28
//
//  Created by Alexander Berezovskyy on 11.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ABSuccessRegistrationVC : UIViewController

@property (strong, nonatomic) NSString *firstAndLastNames;

@end
