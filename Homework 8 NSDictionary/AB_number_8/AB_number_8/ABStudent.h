//
//  ABStudent.h
//  AB_number_8
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABStudent : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *greeting;

+ (ABStudent *) newStudent;

@end
