//
//  ABStudent.m
//  AB_number_8
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

static NSString *firstNames[] = {
    @"Ivan", @"Petr", @"Alex", @"Maxim", @"Denis", @"Kirill", @"Alexandr", @"Segey", @"Valentin", @"Mikhail"
};

static NSString *lastNames[] = {
    @"Ivanov", @"Petrov", @"Alexov", @"Maximov", @"Denisov", @"Kirillov", @"Alexandrov", @"Segeyev", @"Valentinov", @"Mikhailov"
};

static NSString *greetings[] = {
    @"hi", @"hello", @"bye", @"look", @"sit", @"go", @"read", @"teach", @"stop", @"have"
};

static int studentsCount = 10;

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.firstName = @"None";
        self.lastName = @"nOne";
        self.greeting = @"noNe";
    }
    return self;
}

+ (ABStudent *) newStudent {
    
    ABStudent *student = [[ABStudent alloc] init];
    
    student.firstName = firstNames [arc4random_uniform(studentsCount)];
    student.lastName = lastNames [arc4random_uniform(studentsCount)];
    student.greeting = greetings [arc4random_uniform(studentsCount)];
    
    return student;
}



















@end
