//
//  ABTableVC.m
//  AB_number_40
//
//  Created by Alexander Berezovskyy on 09.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABTableVC.h"
#import "ABStudent.h"
#import "ABGroup.h"

@interface ABTableVC () <UITextFieldDelegate, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *firstNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *dateOfBirthTextField;
@property (weak, nonatomic) IBOutlet UITextField *gradeTextField;
@property (weak, nonatomic) IBOutlet UISegmentedControl *genderSegmentedControl;

@property (strong, nonatomic) ABStudent *student;

- (IBAction)addNewParamAction:(UIButton *)sender;
- (IBAction)genderSelectedAction:(UISegmentedControl *)sender;
- (IBAction)clearAllPropertiesAction:(UIButton *)sender;


@end

@implementation ABTableVC

- (void) loadView {
    [super loadView];
    
    self.student = [ABStudent randomStudent];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.firstNameTextField.text = self.student.firstName;
    self.lastNameTextField.text = self.student.lastName;
    self.dateOfBirthTextField.text = [self.student randomDateInStringAmericanFormatForDate:self.student.birthDate];
    self.gradeTextField.text = [NSString stringWithFormat:@"%.2f", self.student.grade];
    
    [self.student addObserver:self
                   forKeyPath:@"firstName"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:nil];
    
    [self.student addObserver:self
                   forKeyPath:@"lastName"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:nil];
    
    [self.student addObserver:self
                   forKeyPath:@"birthDate"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:nil];
    
    [self.student addObserver:self
                   forKeyPath:@"grade"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:nil];
    
    [self.student addObserver:self
                   forKeyPath:@"gender"
                      options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld
                      context:nil];
    
    
    // ******* Level Master *******
    
    ABStudent *student2 = [ABStudent randomStudent];
    ABStudent *student3 = [ABStudent randomStudent];
    ABStudent *student4 = [ABStudent randomStudent];
    
    self.student.bestFriend = student2;
    student2.bestFriend = student3;
    student3.bestFriend = student4;
    student4.bestFriend = self.student;
    
    ABGroup *group1 = [[ABGroup alloc] init];
    
    group1.students = [NSMutableArray arrayWithObjects:self.student, student2, student3, student4, nil];
    
    NSInteger i = 1;
    
    for (ABStudent *st in group1.students) {
        NSLog(@"student #%li = %@", i, st);
        i++;
    }
    
    [student2 setValue:@"Ninja111" forKeyPath:@"bestFriend.firstName"];
    [student2 setValue:@"Ninja222" forKeyPath:@"bestFriend.bestFriend.firstName"];
    [student2 setValue:@"Ninja333" forKeyPath:@"bestFriend.bestFriend.bestFriend.firstName"];
    [student2 setValue:@"Ninja4444" forKeyPath:@"bestFriend.bestFriend.bestFriend.bestFriend.firstName"];
    
    i = 1;
    NSLog(@"***************************************");
    
    for (ABStudent *st in group1.students) {
        NSLog(@"student #%li = %@", i, st);
        i++;
    }
    
    // ******* Level Superman *******
    
    ABStudent *student5 = [ABStudent randomStudent];
    ABStudent *student6 = [ABStudent randomStudent];
    ABStudent *student7 = [ABStudent randomStudent];
    ABStudent *student8 = [ABStudent randomStudent];
    
    ABGroup *group2 = [[ABGroup alloc] init];
    
    group2.students = [[NSArray alloc] initWithObjects:student5, student6, student7, student8, nil];
    
    NSArray *groups = [[NSArray alloc] initWithObjects:group1, group2, nil];
    NSArray *students = [groups valueForKeyPath:@"@distinctUnionOfArrays.students"];

    NSArray *allNamesOfStudents = [students valueForKeyPath:@"@distinctUnionOfObjects.firstName"];
    NSLog(@"all names of students = %@", allNamesOfStudents);
    
    NSDate *minDate = [students valueForKeyPath:@"@min.birthDate"];
    NSDate *maxDate = [students valueForKeyPath:@"@max.birthDate"];
    NSLog(@"min birth date = %@\nmax birth date = %@", minDate, maxDate);
    
    NSNumber *sumGrade = [students valueForKeyPath:@"@sum.grade"];
    NSNumber *avgGrade = [students valueForKeyPath:@"@avg.grade"];
    NSLog(@"sum grade = %@\navg grade = %@", sumGrade, avgGrade);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc {
    
    [self.student removeObserver:self
                      forKeyPath:@"firstName"];
    
    [self.student removeObserver:self
                      forKeyPath:@"lastName"];
    
    [self.student removeObserver:self
                      forKeyPath:@"birthDate"];
    
    [self.student removeObserver:self
                      forKeyPath:@"grade"];
    
    [self.student removeObserver:self
                      forKeyPath:@"gender"];
}


#pragma mark - Actions

- (IBAction)addNewParamAction:(UIButton *)sender {
    
    if (sender.tag == 1) {
        
        self.student.birthDate = [ABStudent randomDateOfBirthBetween:18 and:40];
        self.dateOfBirthTextField.text = [self.student randomDateInStringAmericanFormatForDate:self.student.birthDate];
    
    } else if (sender.tag == 2) {
        
        self.student.grade = (arc4random_uniform(301) + 200) / 100.f;
        self.gradeTextField.text = [NSString stringWithFormat:@"%.2f", self.student.grade];
        
    }
}

- (IBAction)genderSelectedAction:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == 0) {
        self.student.gender = ABGenderMan;
    } else if (sender.selectedSegmentIndex == 1) {
        self.student.gender = ABGenderWoman;
    }
}

- (IBAction)clearAllPropertiesAction:(UIButton *)sender {
    
    [self.student clearAllProperties];
    self.firstNameTextField.text = @"";
    self.lastNameTextField.text = @"";
    self.dateOfBirthTextField.text = @"";
    self.gradeTextField.text = @"";
    self.genderSegmentedControl.selectedSegmentIndex = 0;
}


#pragma mark - UITextFieldDelegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
    if (textField.tag == 3) {

        self.student.firstName = textField.text;

        self.firstNameTextField.text = self.student.firstName;
    
    } else if (textField.tag == 4) {
        
        self.student.lastName = textField.text;
        
        self.lastNameTextField.text = self.student.lastName;
    }
}


#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma mark - Observing

- (void) observeValueForKeyPath:(NSString *)keyPath
                       ofObject:(id)object
                         change:(NSDictionary<NSKeyValueChangeKey,id> *)change
                        context:(void *)context {
    
    NSLog(@"\nkeyPath = %@, \nobject = %@, \nchange = %@", keyPath, object, change);
}





@end
