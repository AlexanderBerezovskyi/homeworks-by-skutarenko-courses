//
//  ABStudent.h
//  AB_number_37-38
//
//  Created by Alexander Berezovskyy on 23.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum {
    
    ABGenderMan,
    ABGenderWoman
    
}ABGender;

@interface ABStudent : NSObject

// Student
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSDate *birthDate;
@property (assign, nonatomic) CGFloat grade;
@property (assign, nonatomic) ABGender gender;

@property (weak, nonatomic) ABStudent *bestFriend;


+ (ABStudent *) randomStudent;
+ (NSDate *) randomDateOfBirthBetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld;
- (NSString *) randomDateInStringAmericanFormatForDate:(NSDate *)currentDate;
- (void) clearAllProperties;

@end
