//
//  ABGroup.h
//  AB_number_40
//
//  Created by Alexander Berezovskyy on 12.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABGroup : NSObject

@property (nonatomic, strong) NSArray *students;

@end
