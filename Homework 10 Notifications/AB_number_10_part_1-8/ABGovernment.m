//
//  ABGovernment.m
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABGovernment.h"


#pragma mark - Notifications

NSString * const ABGovernmentSalaryDidChangeNotification = @"ABGovernmentSalaryDidChangeNotification";
NSString * const ABGovernmentTaxLevelDidChangeNotification = @"ABGovernmentTaxLevelDidChangeNotification";
NSString * const ABGovernmentPensionDidChangeNotification = @"ABGovernmentPensionDidChangeNotification";
NSString * const ABGovernmentAveragePriceDidChangeNotification = @"ABGovernmentAveragePriceDidChangeNotification";


#pragma mark - User Info Keys

NSString * const ABGovernmentSalaryUserInfoKey = @"ABGovernmentSalaryUserInfoKey";
NSString * const ABGovernmentTaxLevelUserInfoKey = @"ABGovernmentTaxLevelUserInfoKey";
NSString * const ABGovernmentPensionUserInfoKey = @"ABGovernmentPensionUserInfoKey";
NSString * const ABGovernmentAveragePriceUserInfoKey = @"ABGovernmentAveragePriceUserInfoKey";


@implementation ABGovernment


#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        _salary = 3200.f;
        _taxLevel = 27.f;
        _pension = 1200.f;
        _averagePrice = 50.f;
        
    }
    return self;
}

- (void) setSalary:(CGFloat)salary {
    
    _salary = salary;
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:salary]
                                                           forKey:ABGovernmentSalaryUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ABGovernmentSalaryDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setTaxLevel:(CGFloat)taxLevel {
    
    _taxLevel = taxLevel;
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:taxLevel]
                                                           forKey:ABGovernmentTaxLevelUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ABGovernmentTaxLevelDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setPension:(CGFloat)pension {
    
    _pension = pension;
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:pension]
                                                           forKey:ABGovernmentPensionUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ABGovernmentPensionDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}

- (void) setAveragePrice:(CGFloat)averagePrice {
    
    _averagePrice = averagePrice;
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:averagePrice]
                                                           forKey:ABGovernmentAveragePriceUserInfoKey];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ABGovernmentAveragePriceDidChangeNotification
                                                        object:nil
                                                      userInfo:dictionary];
}


- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


















@end
