//
//  AppDelegate.m
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABGovernment.h"
#import "ABDoctor.h"
#import "ABBusinessman.h"
#import "ABPensioner.h"

@interface AppDelegate ()

@property (nonatomic, strong) ABGovernment *government;
@property (nonatomic, strong) ABDoctor *doctor;
@property (nonatomic, strong) ABBusinessman *businessman;
@property (nonatomic, strong) ABPensioner *pensioner;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    self.doctor = [[ABDoctor alloc] init];
    self.businessman = [[ABBusinessman alloc] init];
    self.pensioner = [[ABPensioner alloc] init];
    
    self.government = [[ABGovernment alloc] init];
    
    ABDoctor *doctor1 = [[ABDoctor alloc] initWithName:@"Doctor_1" andSalary:2500.f];
    ABDoctor *doctor2 = [[ABDoctor alloc] initWithName:@"Doctor_2" andSalary:3500.f];
    doctor1.averagePrice = doctor2.averagePrice = self.government.averagePrice;
    
    ABBusinessman *businessman1 = [[ABBusinessman alloc] initWithName:@"Businessman_1" andTaxLevel:25.f];
    ABBusinessman *businessman2 = [[ABBusinessman alloc] initWithName:@"Businessman_2" andTaxLevel:29.f];
    businessman1.averagePrice = businessman2.averagePrice = self.government.averagePrice;
    
    ABPensioner *pensioner1 = [[ABPensioner alloc] initWithName:@"Pensioner_1" andPension:1000.f];
    ABPensioner *pensioner2 = [[ABPensioner alloc] initWithName:@"Pensioner_2" andPension:1300.f];
    pensioner1.averagePrice = pensioner2.averagePrice = self.government.averagePrice;
   
    
    NSArray *people = [[NSArray alloc] initWithObjects:doctor1, doctor2, businessman1,
                                                       businessman2, pensioner1, pensioner2, nil];

    self.government.salary = 3100.f;
    self.government.salary = 3300.f;
    self.government.taxLevel = 28.f;
    self.government.taxLevel = 35.f;
    self.government.pension = 1100.f;
    self.government.pension = 1500.f;
    
    self.government.averagePrice = 62.f;
    
    for (id p in people) {
        
        if ([p isKindOfClass:[ABDoctor class]]) {
            ABDoctor *doc = (ABDoctor *) p;
            NSLog(@"%@: I can buy %.2f pieces of goods", doc.name, (CGFloat) doc.salary/doc.averagePrice);
        }
        
        if ([p isKindOfClass:[ABPensioner class]]) {
            ABPensioner *pens = (ABPensioner *) p;
            NSLog(@"%@: I can buy %.2f pieces of goods", pens.name, (CGFloat) pens.pension/pens.averagePrice);
        }
        
        if ([p isKindOfClass:[ABBusinessman class]]) {
            ABBusinessman *bus = (ABBusinessman *) p;
            NSLog(@"%@: Now in goods cost about of %.2f $ taxes", bus.name, ((bus.averagePrice/100) * bus.taxLevel));
        }
    }
      
    return YES;
}


- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}





- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
