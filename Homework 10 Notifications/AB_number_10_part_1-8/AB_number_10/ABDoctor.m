//
//  ABDoctor.m
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDoctor.h"
#import "ABGovernment.h"

@implementation ABDoctor

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self salaryAndAveragePrice];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doctorDidEnterBackgroundNotification:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doctorWillEnterForegroundNotification:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
    }
    return self;
}

- (id) initWithName:(NSString *)name andSalary:(CGFloat) salary {
    
    self = [super init];
    if (self) {
        
        [self salaryAndAveragePrice];
        
        self.name = name;
        self.salary = salary;
    }
    return self;
}

#pragma mark - Notifications

- (void) doctorSalaryChangeNotification:(NSNotification *) notification {
        
    NSNumber *value = [notification.userInfo objectForKey:ABGovernmentSalaryUserInfoKey];
    
    CGFloat salary = [value floatValue];
    
    if (self.salary > salary) {
        
        NSLog(@"The %@ is NOT happy", self.name);
    
    } else {
        
        NSLog(@"The %@ is happy", self.name);
    }
    
    NSLog(@"Old salary = %.2f, new = %.2f", self.salary, salary);
    
    self.salary = salary;
}

- (void) averagePriceChangeNotification:(NSNotification *) notification {
    
    NSNumber *value = [notification.userInfo objectForKey:ABGovernmentAveragePriceUserInfoKey];
    
    CGFloat averagePrice = [value floatValue];
    
    if (self.averagePrice > averagePrice) {
        
        NSLog(@"It's Good for %@", self.name);
        
    } else {
        
        NSLog(@"It's BAD for %@", self.name);
    }
    
    NSLog(@"Old average price = %.2f, new = %.2f", self.averagePrice, averagePrice);
    
    self.averagePrice = averagePrice;
}


- (void) doctorDidEnterBackgroundNotification:(NSNotification *) notification {
    
    NSLog(@"The doctor did go to the Background");
}

- (void) doctorWillEnterForegroundNotification:(NSNotification *) notification {
    
    NSLog(@"The doctor will go to the Foreground");
}


#pragma mark - Reduce Code Methods

- (void) salaryAndAveragePrice {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(doctorSalaryChangeNotification:)
                                                 name:ABGovernmentSalaryDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(averagePriceChangeNotification:)
                                                 name:ABGovernmentAveragePriceDidChangeNotification
                                               object:nil];
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end

