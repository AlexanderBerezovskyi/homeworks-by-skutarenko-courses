//
//  ABDoctor.h
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABDoctor : NSObject

@property (nonatomic, assign) CGFloat salary;
@property (nonatomic, assign) CGFloat averagePrice;
@property (nonatomic, strong) NSString *name;

- (id) initWithName:(NSString *)name andSalary:(CGFloat) salary;

@end
