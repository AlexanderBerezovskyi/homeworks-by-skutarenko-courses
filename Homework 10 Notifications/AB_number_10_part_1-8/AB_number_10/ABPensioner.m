//
//  ABPensioner.m
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABPensioner.h"
#import "ABGovernment.h"

@implementation ABPensioner

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self pensionAndAveragePrice];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(pensionerDidEnterBackgroundNotification:)
                                                     name:UIApplicationDidEnterBackgroundNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(pensionerWillEnterForegroundNotification:)
                                                     name:UIApplicationWillEnterForegroundNotification
                                                   object:nil];
    }
    return self;
}

- (id) initWithName:(NSString *)name andPension:(CGFloat) pension {
    
    self = [super init];
    if (self) {
        
        [self pensionAndAveragePrice];
        
        self.name = name;
        self.pension = pension;
    }
    return self;
}


#pragma mark - Notifications

- (void) pensionerPensionChangeNotification:(NSNotification *) notification {
    
    NSNumber *value = [notification.userInfo objectForKey:ABGovernmentPensionUserInfoKey];
    
    CGFloat pension = [value floatValue];
    
    if (self.pension > pension) {
        
        NSLog(@"The %@ is NOT happy", self.name);
        
    } else {
        
        NSLog(@"The %@ is happy", self.name);
    }
    
    NSLog(@"Old pension = %.2f, new = %.2f", self.pension, pension);
    
    self.pension = pension;
}


- (void) averagePriceChangeNotification:(NSNotification *) notification {
    
    NSNumber *value = [notification.userInfo objectForKey:ABGovernmentAveragePriceUserInfoKey];
    
    CGFloat averagePrice = [value floatValue];
    
    if (self.averagePrice > averagePrice) {
        
        NSLog(@"It's Good for %@", self.name);
        
    } else {
        
        NSLog(@"It's BAD for %@", self.name);
    }
    
    NSLog(@"Old average price = %.2f, new = %.2f", self.averagePrice, averagePrice);
    
    self.averagePrice = averagePrice;
}

- (void) pensionerDidEnterBackgroundNotification:(NSNotification *) notification {
    
    NSLog(@"The pensioner did go to the Background");
}

- (void) pensionerWillEnterForegroundNotification:(NSNotification *) notification {
    
    NSLog(@"The pensioner will go to the Foreground");
}


#pragma mark - Reduce Code Methods

- (void) pensionAndAveragePrice {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(pensionerPensionChangeNotification:)
                                                 name:ABGovernmentPensionDidChangeNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(averagePriceChangeNotification:)
                                                 name:ABGovernmentAveragePriceDidChangeNotification
                                               object:nil];
}


- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
