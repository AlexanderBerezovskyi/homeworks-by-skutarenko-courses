//
//  ABPensioner.h
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABPensioner : NSObject

@property (nonatomic, assign) CGFloat pension;
@property (nonatomic, assign) CGFloat averagePrice;
@property (nonatomic, strong) NSString *name;

- (id) initWithName:(NSString *)name andPension:(CGFloat) pension;

@end
