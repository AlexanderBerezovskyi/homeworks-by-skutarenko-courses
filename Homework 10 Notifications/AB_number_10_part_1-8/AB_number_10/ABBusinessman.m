//
//  ABBusinessman.m
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABBusinessman.h"
#import "ABGovernment.h"

@implementation ABBusinessman

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        [self taxLevelAndAveragePrice];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(businessmanRotateNotification:)
                                                     name:UIDeviceOrientationDidChangeNotification
                                                   object:nil];
        
    }
    return self;
}

- (id) initWithName:(NSString *)name andTaxLevel:(CGFloat) taxLevel {
    
    self = [super init];
    if (self) {
        
        [self taxLevelAndAveragePrice];
        
        self.name = name;
        self.taxLevel = taxLevel;
    }
    return self;
}


#pragma mark - Notifications

- (void) businessmanTaxLevelChangeNotification:(NSNotification *) notification {
    
    NSLog(@"businessmanTaxLevelChangeNotification = %@", notification.userInfo);
    
    NSNumber *value = [notification.userInfo objectForKey:ABGovernmentTaxLevelUserInfoKey];
    
    CGFloat taxLevel = [value floatValue];
    
    if (self.taxLevel < taxLevel) {
        
        NSLog(@"The %@ is NOT happy", self.name);
        
    } else {
        
        NSLog(@"The %@ is happy", self.name);
    }
    
    NSLog(@"Old tax level = %.2f, new = %.2f", self.taxLevel, taxLevel);
    
    self.taxLevel = taxLevel;
}

- (void) averagePriceChangeNotification:(NSNotification *) notification {
    
    NSNumber *value = [notification.userInfo objectForKey:ABGovernmentAveragePriceUserInfoKey];
    
    CGFloat averagePrice = [value floatValue];
    
    if (self.averagePrice > averagePrice) {
        
        NSLog(@"It's Normal for %@", self.name);
        
    } else {
        
        NSLog(@"It's Normal for %@", self.name);
    }
    
    NSLog(@"Old average price = %.2f, new = %.2f", self.averagePrice, averagePrice);
    
    self.averagePrice = averagePrice;
}

- (void) businessmanRotateNotification:(NSNotification *)notification {
    
    NSLog(@"Businessman rotate");
}

#pragma mark - Reduce Code Methods

- (void) taxLevelAndAveragePrice {
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(businessmanTaxLevelChangeNotification:)
                                                 name:ABGovernmentTaxLevelDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(averagePriceChangeNotification:)
                                                 name:ABGovernmentAveragePriceDidChangeNotification
                                               object:nil];
}


- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
