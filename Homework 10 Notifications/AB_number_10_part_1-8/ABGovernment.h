//
//  ABGovernment.h
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 08.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


#pragma mark - Notifications

extern NSString * const ABGovernmentSalaryDidChangeNotification;
extern NSString * const ABGovernmentTaxLevelDidChangeNotification;
extern NSString * const ABGovernmentPensionDidChangeNotification;
extern NSString * const ABGovernmentAveragePriceDidChangeNotification;


#pragma mark - User Info Keys

extern NSString * const ABGovernmentSalaryUserInfoKey;
extern NSString * const ABGovernmentTaxLevelUserInfoKey;
extern NSString * const ABGovernmentPensionUserInfoKey;
extern NSString * const ABGovernmentAveragePriceUserInfoKey;


@interface ABGovernment : NSObject

@property (nonatomic, assign) CGFloat salary;
@property (nonatomic, assign) CGFloat taxLevel;
@property (nonatomic, assign) CGFloat pension;
@property (nonatomic, assign) CGFloat averagePrice;

@end








