//
//  ABAppDelegateAnalog.m
//  AB_number_10
//
//  Created by Alexander Berezovskyy on 09.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABAppDelegateAnalog.h"

@implementation ABAppDelegateAnalog

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        
        [nc addObserver:self
               selector:@selector(didFinishLaunchingWithOptionsAnalog:)
                   name:UIApplicationDidFinishLaunchingNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(willResignActiveAnalog:)
                   name:UIApplicationWillResignActiveNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(didEnterBackgroundAnalog:)
                   name:UIApplicationDidEnterBackgroundNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(willEnterForegroundAnalog:)
                   name:UIApplicationWillEnterForegroundNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(didBecomeActiveAnalog:)
                   name:UIApplicationDidBecomeActiveNotification
                 object:nil];
        
        [nc addObserver:self
               selector:@selector(willTerminateAnalog:)
                   name:UIApplicationWillTerminateNotification
                 object:nil];
        
    }
    return self;
}

#pragma mark - Notifications

- (void) didFinishLaunchingWithOptionsAnalog:(NSNotification *) notification {
    
    NSLog(@"didFinishLaunchingWithOptions ANALOG");
}

- (void) willResignActiveAnalog:(NSNotification *) notification {
    
    NSLog(@"willResignActive ANALOG");
}

- (void) didEnterBackgroundAnalog:(NSNotification *) notification {
    
    NSLog(@"didEnterBackground ANALOG");
}

- (void) willEnterForegroundAnalog:(NSNotification *) notification {
    
    NSLog(@"willEnterForeground ANALOG");
}

- (void) didBecomeActiveAnalog:(NSNotification *) notification {
    
    NSLog(@"didBecomeActive ANALOG");
}

- (void) willTerminateAnalog:(NSNotification *) notification {
    
    NSLog(@"willTerminate ANALOG");
}

- (void) dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



@end
