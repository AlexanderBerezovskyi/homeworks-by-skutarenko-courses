//
//  ViewController.m
//  AB_number_19
//
//  Created by Alexander Berezovskyy on 21.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *darkSquaresOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *redGreenCheckersOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *greenBlueOutletCollection;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *allCheckersCollectionView;
@property (weak, nonatomic) IBOutlet UIView *checkerBoardView;

@property (nonatomic, strong) NSMutableArray *allCheckersCentersArray;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.allCheckersCentersArray = [NSMutableArray array];

    for (UIView *checkerView in self.allCheckersCollectionView) {
        
        NSString *centerCheckerView = NSStringFromCGPoint(checkerView.center);
        [self.allCheckersCentersArray addObject:centerCheckerView];
    }
}


#pragma mark - Autoresizing & Orientation of Views

- (UIInterfaceOrientationMask) supportedInterfaceOrientations {
    
    return UIInterfaceOrientationMaskAll;
}

- (void) viewWillTransitionToSize:(CGSize)size
        withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {

    UIColor *colorChange = [[UIColor alloc] initWithRed:(float) arc4random_uniform(256)/255
                                              green:(float) arc4random_uniform(256)/255
                                               blue:(float) arc4random_uniform(256)/255
                                              alpha:1];
    
    for (UIView *darkSquare in self.darkSquaresOutletCollection) {
        
        darkSquare.backgroundColor = colorChange;
    }
    
    [UIView animateWithDuration:2 animations:^{

        for (int i = 0; i < [self.redGreenCheckersOutletCollection count]; i++) {
            
            UIView *checker = [self.allCheckersCollectionView objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:checker];

            
//            Move checker to random free place
            /*
            NSString *checkerString = NSStringFromCGPoint(checker.center); // <<-- current place of checker
            
            UIView *checkerGoTo = [self.darkSquaresOutletCollection objectAtIndex:arc4random_uniform(32)]; // <<-- checker go to
            NSString *checkerGoToString = NSStringFromCGPoint(checkerGoTo.center);
            
            if (![self.allCheckersCentersArray containsObject:checkerGoToString]) {
                
                [self.allCheckersCentersArray removeObject:checkerString];
                checker.center = checkerGoTo.center;
                [self.allCheckersCentersArray addObject:checkerGoToString];

            }
             */

//            Change red and green
            /*
            UIView *redChecker = [self.redGreenCheckersOutletCollection objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:redChecker];
            
            UIView *greenChecker = [self.greenBlueOutletCollection objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:greenChecker];
            
            CGPoint tmpPoint = redChecker.center;
            
            redChecker.center = greenChecker.center;
            
            greenChecker.center = tmpPoint;
            */
            
//            Random change red and green checkers
            
            UIView *redChecker = [self.redGreenCheckersOutletCollection objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:redChecker];

            UIView *greenChecker = [self.greenBlueOutletCollection objectAtIndex:i];
            [self.checkerBoardView bringSubviewToFront:greenChecker];

            CGPoint tmpPoint = redChecker.center;
            
            UIView *randomGreenChecker = [self.greenBlueOutletCollection objectAtIndex:arc4random_uniform(12)];
                
            redChecker.center = randomGreenChecker.center;
            
            randomGreenChecker.center = tmpPoint;
        }
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
