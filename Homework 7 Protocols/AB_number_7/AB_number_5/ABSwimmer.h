//
//  ABSwimmer.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABHuman.h"
#import "ABSwimmers.h"

@interface ABSwimmer : ABHuman <ABSwimmers>

#pragma mark - ABSwimmers

@property (nonatomic, strong) NSString *styleOfSwim;
@property (nonatomic, assign) NSInteger age;

@end
