//
//  ABDog.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABAnimal.h"
#import "ABRunners.h"

@interface ABDog : ABAnimal <ABRunners>

#pragma mark - ABRunners

@property (nonatomic, assign) NSInteger speedOfRun;

@end
