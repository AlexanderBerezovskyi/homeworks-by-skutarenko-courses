//
//  ABJumpers.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 03.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABJumpers <NSObject>

@required

@property (nonatomic, assign) NSInteger heightOfJump;

- (void) jump;

@optional

- (void) eat;


@end
