//
//  ABCat.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABAnimal.h"
#import "ABJumpers.h"

@interface ABCat : ABAnimal <ABJumpers>

#pragma mark - ABJumpers

@property (nonatomic, assign) NSInteger heightOfJump;

@end
