//
//  ABRunner.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABHuman.h"
#import "ABRunners.h"

@interface ABRunner : ABHuman <ABRunners>

#pragma mark - ABRunners

@property (nonatomic, assign) NSInteger speedOfRun;

@end
