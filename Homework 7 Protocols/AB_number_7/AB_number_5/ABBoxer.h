//
//  ABBoxer.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABHuman.h"
#import "ABJumpers.h"
#import "ABRunners.h"
#import "ABSwimmers.h"

@interface ABBoxer : ABHuman <ABJumpers, ABRunners, ABSwimmers>

@property (nonatomic, assign) NSInteger power;
@property (nonatomic, assign) CGFloat reaction;

#pragma mark - ABJumpers

@property (nonatomic, assign) NSInteger heightOfJump;

#pragma mark - ABRunners

@property (nonatomic, assign) NSInteger speedOfRun;

#pragma mark - ABSwimmers

@property (nonatomic, strong) NSString *styleOfSwim;
@property (nonatomic, assign) NSInteger age;

@end
