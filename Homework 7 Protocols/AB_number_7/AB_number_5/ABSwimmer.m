//
//  ABSwimmer.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABSwimmer.h"

@implementation ABSwimmer

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"Denis";
        self.height = 1.88f;
        self.weight = 88;
        self.gender = @"Man";
        self.styleOfSwim = @"Breaststroke";
        self.age = 29;
    }
    return self;
}

- (void) movement {
    
    NSLog(@"Swim");
}

#pragma mark - ABSwimmers

- (void) swim {
    
    NSLog(@"Swim swimmer protocol");
}













@end
