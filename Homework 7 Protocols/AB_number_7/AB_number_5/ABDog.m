//
//  ABDog.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDog.h"

@implementation ABDog

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.color = @"White";
        self.name = @"Persik";
        self.speedOfRun = 75;
    }
    return self;
}

- (void) movement {
    
    NSLog(@"run like dog");
}

#pragma mark - ABRunners

- (void) run {
    
    NSLog(@"Run dog protocol");
}

- (void) eat {
    
    NSLog(@"Eat dog protocol");
}













@end
