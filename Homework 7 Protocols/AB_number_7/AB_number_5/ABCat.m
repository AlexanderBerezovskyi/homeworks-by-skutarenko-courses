//
//  ABCat.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABCat.h"

@implementation ABCat

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.color = @"Gray";
        self.name = @"Kuzya";
        self.heightOfJump = 3;
    }
    return self;
}

- (void) movement {
    
    NSLog(@"run like cat");
}

#pragma mark - ABJumpers

- (void) jump {
    
    NSLog(@"Jump cat protocol");
}

- (void) eat {
    
   NSLog(@"Eat cat protocol"); 
}





@end
