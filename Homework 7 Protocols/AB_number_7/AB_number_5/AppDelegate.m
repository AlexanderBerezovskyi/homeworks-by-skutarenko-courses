//
//  AppDelegate.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"

#import "ABRunner.h"
#import "ABSwimmer.h"
#import "ABBicyclist.h"
#import "ABBoxer.h"

#import "ABCat.h"
#import "ABDog.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    ABRunner *runer = [[ABRunner alloc] init];
    ABSwimmer *swimmer = [[ABSwimmer alloc] init];
    ABBicyclist *bicyclist = [[ABBicyclist alloc] init];
    ABBoxer *boxer = [[ABBoxer alloc] init];
    
    ABCat *cat = [[ABCat alloc] init];
    ABDog *dog = [[ABDog alloc] init];
    

    NSArray *allObjects = [[NSArray alloc] initWithObjects:runer, swimmer, bicyclist, boxer, cat, dog, nil];

    for (id <ABRunners, ABJumpers, ABSwimmers> object in allObjects) {
        NSLog(@"Class name = %@", [object class]);
        
        if ([object conformsToProtocol:@protocol(ABJumpers)]) {
            [object jump];
            
            if (object.heightOfJump) {
                NSLog(@"height of jump = %li", object.heightOfJump);
            }
            
            if ([object respondsToSelector:@selector(eat)]) {
                [object eat];
            }
        }
        
        if ([object conformsToProtocol:@protocol(ABRunners)]) {
            [object run];
            
            if (object.speedOfRun) {
                NSLog(@"speed of run = %li", object.speedOfRun);
            }
            
            if ([object respondsToSelector:@selector(eat)]) {
                [object eat];
            }
        }
        
        if ([object conformsToProtocol:@protocol(ABSwimmers)]) {
            [object swim];
            
            if (object.styleOfSwim) {
                NSLog(@"Style of swim = %@", object.styleOfSwim);
            }
            
            if (object.age) {
                NSLog(@"age = %li", object.age);
            }
        }
        
        if (![object conformsToProtocol:@protocol(ABJumpers)] && ![object conformsToProtocol:@protocol(ABRunners)] && ![object conformsToProtocol:@protocol(ABSwimmers)]) {
            NSLog(@"LOUNGER");
        }
    }

    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
