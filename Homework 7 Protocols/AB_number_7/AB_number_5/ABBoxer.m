//
//  ABBoxer.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABBoxer.h"

@implementation ABBoxer

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"Leila";
        self.height = 1.72f;
        self.weight = 64;
        self.gender = @"Woman";
        self.reaction = 0.2f;
        self.power = 12;
        self.heightOfJump = 2;
        self.speedOfRun = 42;
        self.styleOfSwim = @"Simple";
        self.age = 34;
    }
    return self;
}

- (void) movement {
    
    NSLog(@"Boxing");
}

#pragma mark - ABJumpers

- (void) jump {
    
    NSLog(@"Jump boxer protocol");
}

- (void) eat {
    
    NSLog(@"Eat boxer protocol");
}

#pragma mark - ABRunners

- (void) run {
    
    NSLog(@"Run boxer protocol");
}

#pragma mark - ABSwimmers

- (void) swim {
    
    NSLog(@"Swim boxer protocol");
}












@end
