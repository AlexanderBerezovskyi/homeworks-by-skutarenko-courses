//
//  ABBicyclist.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABBicyclist.h"

@implementation ABBicyclist

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"Eva";
        self.height = 1.74f;
        self.weight = 65;
        self.gender = @"Woman";
    }
    return self;
}

- (void) movement {
    
    NSLog(@"Drive");
}

@end
