//
//  ABHuman.m
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABHuman.h"

@implementation ABHuman

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"Unknown";
        self.height = 1.7f;
        self.weight = 70;
        self.gender = @"Man";
        self.type = @"People";
    }
    return self;
}

- (void) movement {
    
    NSLog(@"Move");
}

@end
