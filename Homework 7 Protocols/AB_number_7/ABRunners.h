//
//  ABRunners.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 03.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABRunners <NSObject>

@required

@property (nonatomic, assign) NSInteger speedOfRun;

- (void) run;

@optional

- (void) eat;


@end
