//
//  ABHuman.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 28.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABHuman : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, assign) CGFloat height;
@property (nonatomic, assign) NSInteger weight;
@property (nonatomic, strong) NSString *type;

- (void) movement;

@end
