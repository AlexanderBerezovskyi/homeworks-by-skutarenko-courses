//
//  ABSwimmers.h
//  AB_number_5
//
//  Created by Alexander Berezovskyy on 03.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ABSwimmers <NSObject>

@required

@property (nonatomic, strong) NSString *styleOfSwim;

- (void) swim;

@optional

@property (nonatomic, assign) NSInteger age;


@end
