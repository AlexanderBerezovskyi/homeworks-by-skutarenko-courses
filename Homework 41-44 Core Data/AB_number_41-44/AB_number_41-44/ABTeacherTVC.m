//
//  ABTeacherTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 29.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABTeacherTVC.h"
#import "ABCourse+CoreDataProperties.h"
#import "ABDataManager.h"
#import "ABUser+CoreDataProperties.h"
#import "ABSaveUserTVC.h"

// segues
static NSString *editUser3Segue = @"editUser3";

@interface ABTeacherTVC () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *subjectsNamesArray;
@property (strong, nonatomic) NSMutableArray *teachersBySubjectArray;
@property (strong, nonatomic) NSArray *coursesArray;
@property (strong, nonatomic) ABUser *user;

@end

@implementation ABTeacherTVC

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.coursesArray = nil;
    self.subjectsNamesArray     = [NSMutableArray array];
    self.teachersBySubjectArray = [NSMutableArray array];
    
    [self createGroupOfTeachersBySubject];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.teachersBySubjectArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSMutableArray *teachersInSubjectArray = [self.teachersBySubjectArray objectAtIndex:section];
    
    return teachersInSubjectArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TeacherCell"
                                                            forIndexPath:indexPath];
    
    NSMutableArray *subjectTeachers = [self.teachersBySubjectArray objectAtIndex:indexPath.section];
    ABUser *user = [subjectTeachers objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", user.lastName, user.firstName];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"teacher on %li course(s)", user.teachCourse.count];
    cell.detailTextLabel.textColor = [UIColor blueColor];
    cell.imageView.image = [UIImage imageWithData:user.photo];
    
    return cell;
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [self.subjectsNamesArray objectAtIndex:section];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return NO;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 1.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *subjectTeachers = [self.teachersBySubjectArray objectAtIndex:indexPath.section];
    self.user = [subjectTeachers objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:editUser3Segue sender:nil];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.textAlignment = NSTextAlignmentRight;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor darkGrayColor];
        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:@"Arial" size:17.f];
        
        tableViewHeaderFooterView.textLabel.layer.shadowColor = [UIColor brownColor].CGColor;
        tableViewHeaderFooterView.textLabel.layer.shadowOpacity = 0.5f;
    }
}


#pragma mark - NSFetchRequest

- (void) createGroupOfTeachersBySubject {
    
    NSFetchRequest *fetchRequest = ABCourse.fetchRequest;
    
    [fetchRequest setFetchBatchSize:20];
    
    NSSortDescriptor *sortDescriptorSubject   = [[NSSortDescriptor alloc]
                                                 initWithKey:@"subject" ascending:YES];
    NSSortDescriptor *sortDescriptorLastName  = [[NSSortDescriptor alloc]
                                                 initWithKey:@"teacher.lastName" ascending:YES];
    NSSortDescriptor *sortDescriptorFirstName = [[NSSortDescriptor alloc]
                                                 initWithKey:@"teacher.firstName" ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptorSubject, sortDescriptorLastName, sortDescriptorFirstName]];
    
    NSError *errorInArray = nil;
    
    self.coursesArray = [[ABDataManager sharedManager].persistentContainer.viewContext executeFetchRequest:fetchRequest error:&errorInArray];
    
    if (errorInArray) {
        NSLog(@"%@", [errorInArray localizedDescription]);
    }
    
    for (ABCourse *course in self.coursesArray) {
        
        if (![self.subjectsNamesArray containsObject:course.subject]) {
            [self.subjectsNamesArray addObject:course.subject];
        }
    }
    
    for (NSString *subjectName in self.subjectsNamesArray) {
        
        NSMutableArray *arr = [NSMutableArray array];
        
        for (ABCourse *course in self.coursesArray) {
            
            if ([course.subject isEqualToString:subjectName] &&
                ![arr containsObject:course.teacher]) {
                
                [arr addObject:course.teacher];
            }
        }
        [self.teachersBySubjectArray addObject:arr];
    }
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:editUser3Segue]){
        
        ABSaveUserTVC *saveUserVC = (ABSaveUserTVC *)segue.destinationViewController;
        saveUserVC.user = self.user;
    }
}

@end
