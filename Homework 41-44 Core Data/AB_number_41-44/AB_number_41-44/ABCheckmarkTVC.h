//
//  ABCheckmarkTVC.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 22.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABCoreDataTVC.h"

typedef enum {
    
    ABCheckmarkSelectStudents,
    ABCheckmarkSelectTeacherForCurrentCourse,
    ABCheckmarkSelectTeacherForNewCourse
    
}ABCheckmark;

@class ABCourse;

@class ABUser; // for block

@interface ABCheckmarkTVC : ABCoreDataTVC

@property (nonatomic, strong) ABCourse *course;
@property (nonatomic, assign) ABCheckmark checkmarkType;
@property (nonatomic, copy) void (^callBackUser)(ABUser *);


@end
