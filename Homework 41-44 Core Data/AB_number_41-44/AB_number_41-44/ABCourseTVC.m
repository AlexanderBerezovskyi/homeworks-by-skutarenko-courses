//
//  ABCourseTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 20.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABCourseTVC.h"
#import "ABCourse+CoreDataProperties.h"
#import "ABSaveCourseTVC.h"

// segues
static NSString *editCourseSegue   = @"editCourse";
static NSString *createCourseSegue = @"createCourse";


@interface ABCourseTVC () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) ABCourse *course;

@end

@implementation ABCourseTVC

@synthesize fetchedResultsController = _fetchedResultsController;

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = ABCourse.fetchRequest;
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.

    NSSortDescriptor *sortDescriptorSubject = [[NSSortDescriptor alloc] initWithKey:@"subject"
                                                                    ascending:YES];
    NSSortDescriptor *sortDescriptorName    = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                                          ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptorSubject, sortDescriptorName]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    NSFetchedResultsController<ABCourse *> *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:@"subject"
                                                   cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseCell"
                                                            forIndexPath:indexPath];
    
    ABCourse *course = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [self configureCell:cell withEvent:course];
    return cell;
}

- (void) configureCell:(UITableViewCell *)cell withEvent:(ABCourse *)course {
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@", course.name];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"%li student(s)", course.users.count];
    cell.detailTextLabel.textColor = [UIColor blueColor];
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    /*
     id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
     return sectionInfo.name; // equal
     */
    
    return [self.fetchedResultsController.sections objectAtIndex:section].name;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.course = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:editCourseSegue sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return 5.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 30.f;    
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.textAlignment = NSTextAlignmentRight;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor darkGrayColor];
        tableViewHeaderFooterView.textLabel.font = [UIFont fontWithName:@"Arial" size:17.f];
        
        tableViewHeaderFooterView.textLabel.layer.shadowColor = [UIColor brownColor].CGColor;
        tableViewHeaderFooterView.textLabel.layer.shadowOpacity = 0.5f;
    }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ABSaveCourseTVC *saveCourseVC = (ABSaveCourseTVC *)segue.destinationViewController;
    
    if([segue.identifier isEqualToString:editCourseSegue]){
        
        saveCourseVC.course = self.course;
        saveCourseVC.courseAction = ABCourseActionEdit;
    
    } else if ([segue.identifier isEqualToString:createCourseSegue]){
        
        saveCourseVC.courseAction = ABCourseActionCreate;
    }
}


@end
