//
//  ABSaveUserTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 16.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABSaveUserTVC.h"
#import "ABUserTVCell.h"
#import "ABUser+CoreDataProperties.h"
#import "ABDataManager.h"
#import "ABCourse+CoreDataProperties.h"

static NSString *firstNameIdentifier = @"FirstNameCell";
static NSString *lastNameIdentifier  = @"LastNameCell";
static NSString *emailIdentifier     = @"EmailCell";
static NSString *courseIdentifier    = @"CourseIdentifier";

typedef enum {
    
    ABCellUserFirstName,
    ABCellUserLastName,
    ABCellUserEmail
    
} ABCellUser;

@interface ABSaveUserTVC () <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) ABUserTVCell *infoUserCell;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *saveButton;
@property (assign, nonatomic) Boolean userTeacherInCourses;
@property (assign, nonatomic) Boolean userStudentInCourses;

- (IBAction)changeUserInfoAction:(UITextField *)sender;

@end

@implementation ABSaveUserTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userTeacherInCourses = self.user.teachCourse.count;
    self.userStudentInCourses = self.user.courses.count;
    
    self.infoUserCell = [[ABUserTVCell alloc] init];
    
    [self saveButtonEnabled];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    if ((self.userTeacherInCourses > 0) && (self.userStudentInCourses > 0))  {
        return 3;
    
    } else if (((self.userTeacherInCourses) && (!self.userStudentInCourses)) ||
               ((!self.userTeacherInCourses) && (self.userStudentInCourses))) {
        return 2;
    
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (section == 0) {
        return 3;
        
    } else if ((section == 1) && (self.userStudentInCourses) && (!self.userTeacherInCourses)) {
        
        return self.user.courses.count;
        
    } else if ((section == 1) && (!self.userStudentInCourses) && (self.userTeacherInCourses)) {
        
        return self.user.teachCourse.count;
    
    } else if ((section == 1) && (self.userStudentInCourses) && (self.userTeacherInCourses)) {
        
        return self.user.courses.count;
        
    } else {
        return self.user.teachCourse.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSMutableArray *coursesStudy   = [NSMutableArray arrayWithArray:[self.user.courses allObjects]];
    NSMutableArray *coursesTeacher = [NSMutableArray arrayWithArray:[self.user.teachCourse allObjects]];
    
    ABUserTVCell *userCell = nil;
    
    if (indexPath.section == 0) {
     
        switch (indexPath.row) {
                
            case ABCellUserFirstName:
                
                userCell = [tableView dequeueReusableCellWithIdentifier:firstNameIdentifier
                                                           forIndexPath:indexPath];
                self.infoUserCell.firstNameTextField = userCell.firstNameTextField;
                
                userCell.firstNameTextField.text = self.user.firstName;
                
                break;
                
            case ABCellUserLastName:
                
                userCell = [tableView dequeueReusableCellWithIdentifier:lastNameIdentifier
                                                           forIndexPath:indexPath];
                self.infoUserCell.lastNameTextField = userCell.lastNameTextField;
                
                userCell.lastNameTextField.text = self.user.lastName;
                
                break;
                
            case ABCellUserEmail:
                
                userCell = [tableView dequeueReusableCellWithIdentifier:emailIdentifier
                                                           forIndexPath:indexPath];
                self.infoUserCell.emailTextField = userCell.emailTextField;
                
                userCell.emailTextField.text = self.user.email;
                
                break;
                
            default:
                break;
        }
        
            return userCell;
        
    } else if ((indexPath.section == 1) && (self.userStudentInCourses)) {
        
        ABCourse *course = [coursesStudy objectAtIndex:indexPath.row];
        
        UITableViewCell *courseCell = [tableView dequeueReusableCellWithIdentifier:courseIdentifier];
        if (!courseCell) {
            courseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                reuseIdentifier:courseIdentifier];
        }

        courseCell.textLabel.text = course.name;
        courseCell.imageView.image = [UIImage imageNamed:@"subject_2.png"];
        
        return courseCell;
    
    } else if ((indexPath.section == 1) &&
               (!self.userStudentInCourses) && (self.userTeacherInCourses)) {
        
        ABCourse *course = [coursesTeacher objectAtIndex:indexPath.row];
        
        UITableViewCell *courseCell = [tableView dequeueReusableCellWithIdentifier:courseIdentifier];
        if (!courseCell) {
            courseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                reuseIdentifier:courseIdentifier];
        }
        
        courseCell.textLabel.text = course.name;
        courseCell.detailTextLabel.text = [NSString stringWithFormat:@"%li student(s) in course", course.users.count];
        courseCell.detailTextLabel.textColor = [UIColor blueColor];
        courseCell.imageView.image = [UIImage imageNamed:@"subject_1.png"];
        
        return courseCell;
        
    } else {
        
        ABCourse *course = [coursesTeacher objectAtIndex:indexPath.row];
        
        UITableViewCell *courseCell = [tableView dequeueReusableCellWithIdentifier:courseIdentifier];
        if (!courseCell) {
            courseCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                                reuseIdentifier:courseIdentifier];
        }
        
        courseCell.textLabel.text = course.name;
        courseCell.detailTextLabel.text = [NSString stringWithFormat:@"%li student(s) in course", course.users.count];
        courseCell.detailTextLabel.textColor = [UIColor blueColor];
        courseCell.imageView.image = [UIImage imageNamed:@"subject_1.png"];
        
        return courseCell;
    }
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (self.user) {
        
        if (section == 0) {
            return @"Info(edit) user";
        
        } else if (section == 1) {
            
            if (self.userStudentInCourses) {
                return @"Student on course(s):";
            } else {
                return @"Teacher on course(s):";
            }
        } else {
            return @"Teacher on course(s):";
        }
        
    } else {
        return @"Create user";
    }
}


#pragma mark - UITableViewDelegate

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


#pragma mark - Actions

- (IBAction)saveAction:(UIBarButtonItem *)sender {
    
    if (self.user) {
        
        self.user.firstName = self.infoUserCell.firstNameTextField.text;
        self.user.lastName = self.infoUserCell.lastNameTextField.text;
        self.user.email = self.infoUserCell.emailTextField.text;
        
    } else {
    
    ABUser *user = [NSEntityDescription insertNewObjectForEntityForName:@"ABUser"
            inManagedObjectContext:[ABDataManager sharedManager].persistentContainer.viewContext];

    user.firstName = self.infoUserCell.firstNameTextField.text;
    user.lastName  = self.infoUserCell.lastNameTextField.text;
    user.email     = self.infoUserCell.emailTextField.text;
    }
    
    [[ABDataManager sharedManager] saveContext];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)changeUserInfoAction:(UITextField *)sender {
    [self saveButtonEnabled];
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    if ([textField isEqual:self.infoUserCell.firstNameTextField]) {
        [self.infoUserCell.lastNameTextField becomeFirstResponder];
        
    } else if ([textField isEqual:self.infoUserCell.lastNameTextField]) {
        [self.infoUserCell.emailTextField becomeFirstResponder];
        
    } else {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.textAlignment = NSTextAlignmentCenter;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor darkGrayColor];
    }
}


#pragma mark - Reduce Methods

- (void) saveButtonEnabled {
    
    if (self.infoUserCell.firstNameTextField.text &&
        self.infoUserCell.lastNameTextField.text&&
        self.infoUserCell.emailTextField.text &&
        ![self.infoUserCell.firstNameTextField.text isEqualToString:@""] &&
        ![self.infoUserCell.lastNameTextField.text isEqualToString:@""] &&
        ![self.infoUserCell.emailTextField.text isEqualToString:@""]) {
        
        [self.saveButton setEnabled:YES];
        
    } else {
        [self.saveButton setEnabled:NO];
    }
}




@end
