//
//  ABUserTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 16.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABUserTVC.h"
#import "ABUser+CoreDataProperties.h"
#import "ABSaveUserTVC.h"


static NSString *editUser1Segue = @"editUser1";

@interface ABUserTVC () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ABUser *user;

@end


@implementation ABUserTVC

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = ABUser.fetchRequest;
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptorLastName = [[NSSortDescriptor alloc] initWithKey:@"lastName"
                                                                    ascending:YES];
    NSSortDescriptor *sortDescriptorFirstName = [[NSSortDescriptor alloc] initWithKey:@"firstName"
                                                                    ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptorLastName, sortDescriptorFirstName]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    NSFetchedResultsController<ABUser *> *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserCell"
                                                            forIndexPath:indexPath];
    
    ABUser *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self configureCell:cell withEvent:user];
    return cell;
}

- (void) configureCell:(UITableViewCell *)cell withEvent:(ABUser *)user {
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", user.lastName, user.firstName];
    cell.detailTextLabel.text = user.email;
    cell.detailTextLabel.textColor = [UIColor blueColor];
    cell.imageView.image = [UIImage imageWithData:user.photo];
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    self.user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    [self performSegueWithIdentifier:editUser1Segue sender:nil];
} 

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:editUser1Segue]){
        
        ABSaveUserTVC *saveUserVC = (ABSaveUserTVC *)segue.destinationViewController;
        saveUserVC.user = self.user;
    }
}




@end
