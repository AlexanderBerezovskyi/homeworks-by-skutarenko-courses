//
//  ABCheckmarkTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 22.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABCheckmarkTVC.h"
#import "ABUser+CoreDataProperties.h"
#import "ABCourse+CoreDataProperties.h"
#import "ABDataManager.h"


@interface ABCheckmarkTVC () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) ABUser *user;

@end

@implementation ABCheckmarkTVC

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.checkmarkType == ABCheckmarkSelectTeacherForNewCourse ||
        self.checkmarkType == ABCheckmarkSelectTeacherForCurrentCourse) {
       
        UIEdgeInsets inset = UIEdgeInsetsMake(30, 0, 0, 0);
        self.tableView.contentInset = inset;
        self.tableView.scrollIndicatorInsets = inset;
    }

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = ABUser.fetchRequest;
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"lastName"
                                                                    ascending:YES];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"firstName"
                                                                    ascending:YES];
    
    [fetchRequest setSortDescriptors:@[sortDescriptor1, sortDescriptor2]];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    
    NSFetchedResultsController<ABUser *> *aFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:self.managedObjectContext
                                          sectionNameKeyPath:nil
                                                   cacheName:nil];
    
    aFetchedResultsController.delegate = self;
    
    NSError *error = nil;
    if (![aFetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
    
    _fetchedResultsController = aFetchedResultsController;
    return _fetchedResultsController;
}

#pragma mark - UITableViewDataSource

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    if (self.checkmarkType == ABCheckmarkSelectTeacherForCurrentCourse) {
        return @"Select a teacher for the current course";
    } else if (self.checkmarkType == ABCheckmarkSelectTeacherForNewCourse) {
        return @"Select a teacher for a new course";
    } else {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CheckmarkCell"
                                                            forIndexPath:indexPath];
    
    ABUser *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (self.checkmarkType == ABCheckmarkSelectStudents) {
        
        if ([self.course.users containsObject:user]) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        
    } else if (self.checkmarkType == ABCheckmarkSelectTeacherForCurrentCourse) {
        
        if (self.course.teacher == user) {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
    }
    
    [self configureCell:cell withEvent:user];
    
    return cell;
}

- (void) configureCell:(UITableViewCell *)cell withEvent:(ABUser *)user {
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", user.lastName, user.firstName];
    cell.detailTextLabel.text = user.email;
    cell.detailTextLabel.textColor = [UIColor blueColor];
    cell.imageView.image = [UIImage imageWithData:user.photo];
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    ABUser *user = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    if (self.checkmarkType == ABCheckmarkSelectStudents) {
    
        if (self.course.teacher != user) {
        
            if (cell.accessoryType == UITableViewCellAccessoryNone) {
                
                [self.course addUsersObject:user];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;

            } else {
                [self.course removeUsersObject:user];
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
        
        } else {
            [self showAlertWithMessage:@"The teacher and student can't be the same person!"];
        }
        
    } else if (self.checkmarkType == ABCheckmarkSelectTeacherForCurrentCourse) {
        
        if (![self.course.users containsObject:user]) {
            self.course.teacher = user;
            [self dismissViewControllerAnimated:YES completion:nil];
        } else {
            [self showAlertWithMessage:@"The teacher and student can't be the same person!"];
        }
    
    } else if (self.checkmarkType == ABCheckmarkSelectTeacherForNewCourse) {
        
        if (self.callBackUser) {
            self.callBackUser (user);
        }
        
        cell.accessoryType = UITableViewCellAccessoryCheckmark;

        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [[ABDataManager sharedManager] saveContext];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    if([view isKindOfClass:[UITableViewHeaderFooterView class]]){
        
        UITableViewHeaderFooterView *tableViewHeaderFooterView = (UITableViewHeaderFooterView *) view;
        tableViewHeaderFooterView.textLabel.textAlignment = NSTextAlignmentCenter;
        tableViewHeaderFooterView.textLabel.textColor = [UIColor brownColor];
    }
}


#pragma mark - UIAlertController

- (void) showAlertWithMessage:(NSString *)message {
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:@"Information"
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action) {
                                    
                                }];
    
    [alert addAction:okButton];

    [self presentViewController:alert animated:YES completion:nil];
}



@end
