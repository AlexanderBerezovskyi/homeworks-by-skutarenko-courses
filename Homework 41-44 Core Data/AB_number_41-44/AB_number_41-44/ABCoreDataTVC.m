//
//  ABCoreDataTVC.m
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 16.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABCoreDataTVC.h"
#import "ABDataManager.h"

//#import "ABCourse+CoreDataProperties.h"
//#import "ABUser+CoreDataProperties.h"

static NSString *usersImages[] = {@"user_1.png", @"user_2.png", @"user_3.png", @"user_4.png"};

static NSString *firstNames[] = {@"Georgan", @"Oigenhein", @"Denya", @"Sergio", @"Arslanito", @"Igorio", @"Lina", @"Kira", @"Maria", @"Olga", @"Karyna", @"Irina", @"Albina", @"Georgina"};

static NSString *lastNames[] = {@"Petrov", @"Ivanov", @"Sydorov", @"Mamontov", @"Vetrov", @"Vorotov", @"Gringo", @"Logman", @"Sisilio"};


@interface ABCoreDataTVC () 

@end

@implementation ABCoreDataTVC

- (instancetype)init
{
    self = [super initWithStyle:UITableViewStylePlain];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIEdgeInsets inset = UIEdgeInsetsMake(10, 0, 0, 0);
    self.tableView.contentInset = inset;
    self.tableView.scrollIndicatorInsets = inset;
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    ABCourse *course = [NSEntityDescription insertNewObjectForEntityForName:@"ABCourse"
//                                                     inManagedObjectContext:[ABDataManager sharedManager].persistentContainer.viewContext];
//    
//    course.name    = @"Geometry";
//    course.subject = @"Mathematic";
//    
//    for (int i = 0; i < 4; i++) {
//        
//        ABUser *user = [NSEntityDescription insertNewObjectForEntityForName:@"ABUser"
//                                                         inManagedObjectContext:[ABDataManager sharedManager].persistentContainer.viewContext];
//        
//        user.firstName  = firstNames[arc4random_uniform(14)];
//        user.lastName   = lastNames[arc4random_uniform(9)];
//        user.email      = [NSString stringWithFormat:@"%@%u@gmail.com", firstNames[arc4random_uniform(14)], arc4random_uniform(99)];
//        
//        NSData *photo   = UIImagePNGRepresentation([UIImage imageNamed:usersImages[arc4random_uniform(4)]]);
//        
//        user.photo      = photo;
//        
//        [course addUsersObject:user];
//    }
//    
//    [[ABDataManager sharedManager] saveContext];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - NSManagedObjectContext

- (NSManagedObjectContext *) managedObjectContext {
    
    if (!_managedObjectContext) {
        _managedObjectContext = [[ABDataManager sharedManager] persistentContainer].viewContext;
    }
    return _managedObjectContext;
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error = nil;
        if (![context save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, error.userInfo);
            abort();
        }
    }
}


- (void)configureCell:(UITableViewCell *)cell withEvent:(NSObject *)event {

}


#pragma mark - NSFetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    return nil;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        default:
            return;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] withEvent:anObject];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

@end
