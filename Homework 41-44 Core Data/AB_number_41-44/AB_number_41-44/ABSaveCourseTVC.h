//
//  ABSaveCourseTVC.h
//  AB_number_41-44
//
//  Created by Alexander Berezovskyy on 20.06.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    
    ABCourseActionCreate,
    ABCourseActionEdit
    
} ABCourseAction;

@class ABCourse;
@class ABUser;

@interface ABSaveCourseTVC : UITableViewController

@property (strong, nonatomic) ABCourse *course;
@property (strong, nonatomic) ABUser *user;
@property (assign, nonatomic) ABCourseAction courseAction;

@end
