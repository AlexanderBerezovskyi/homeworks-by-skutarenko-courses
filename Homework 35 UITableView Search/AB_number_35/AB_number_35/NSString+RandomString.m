//
//  NSString+RandomString.m
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "NSString+RandomString.h"

@implementation NSString (RandomString)

+ (NSString *)randomLetterStringWithLength:(NSUInteger)stringLength {
    
    NSString *letters = @"abcdefghijklmnopqrstuvwxyz";
    NSMutableString *newString = [NSMutableString stringWithCapacity:stringLength];
    
    for (int i = 0; i < stringLength; i++) {
        [newString appendString:[NSString stringWithFormat:@"%C", [letters characterAtIndex:arc4random_uniform((int)[letters length])]]];
    }
    return [newString capitalizedString];
}

+ (NSString *)randomLetterString {
    
    NSUInteger stringLength = arc4random_uniform(3) + 4;
    
    return [[self randomLetterStringWithLength:stringLength] capitalizedString];
}

@end
