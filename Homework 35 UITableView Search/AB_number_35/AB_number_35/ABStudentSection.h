//
//  ABStudentSection.h
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABStudentSection : NSObject

@property (strong, nonatomic) NSString *nameFull;
@property (strong, nonatomic) NSString *nameShort;
@property (strong, nonatomic) NSMutableArray *studentsArray;

@end
