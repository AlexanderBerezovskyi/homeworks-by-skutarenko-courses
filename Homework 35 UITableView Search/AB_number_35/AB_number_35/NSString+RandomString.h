//
//  NSString+RandomString.h
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RandomString)

+ (NSString *)randomLetterStringWithLength:(NSUInteger)stringLength;
+ (NSString *)randomLetterString;

@end
