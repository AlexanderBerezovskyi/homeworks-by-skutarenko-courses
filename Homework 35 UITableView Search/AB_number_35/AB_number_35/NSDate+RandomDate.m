//
//  NSDate+RandomDate.m
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "NSDate+RandomDate.h"

@implementation NSDate (RandomDate)

+ (NSDate *) randomDateOfBirthBetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld {
    
    NSDate      *currentDate = [NSDate date];
    NSCalendar  *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsFromCurrentDate = [currentCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:currentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] - secondYearOld];
    NSDate *dateSecondYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] + secondYearOld - firstYearOld];
    NSDate *dateFirstYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    NSDateComponents *componentsBetweenFirstAndSecontYearsOld = [currentCalendar components:NSCalendarUnitSecond fromDate:dateSecondYearsAgo toDate:dateFirstYearsAgo options:NO];
    
    NSInteger betweenFirstAndSecond = [componentsBetweenFirstAndSecontYearsOld second];
    
    
    NSDate *dateOfBirth = [NSDate dateWithTimeInterval:arc4random() % betweenFirstAndSecond
                                             sinceDate:dateSecondYearsAgo];
                             
    return dateOfBirth;
}

+ (NSString *) randomDateInStringAmericanFormatForDate:(NSDate *)currentDate {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM/dd/yyyy"];
    
    NSString *randomDateString = [dateFormat stringFromDate:currentDate];
    
    return randomDateString;
}

@end
