//
//  ViewController.m
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import "NSString+RandomString.h"
#import "NSDate+RandomDate.h"
#import "ABStudent.h"
#import "ABStudentSection.h"


@interface ViewController () <UITableViewDataSource, UITabBarDelegate, UISearchBarDelegate>

typedef enum {
    
    ABSortStyleMonthFirstNameLastName,
    ABSortStyleFirstNameLastNameDate,
    ABSortStyleLastNameDateFirstName
    
} ABSortStyle;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortStyleSegmentedControl;

@property (strong, nonatomic) NSMutableArray *allStudentsArray;
@property (strong, nonatomic) NSArray *sectionsArray;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.sectionsArray = [NSArray array];
    
    // create students
    NSMutableArray *allStudentsArray = [NSMutableArray array];
    
    int studentsCount = 100000;
    
    for (int i = 0; i < studentsCount; i++) {
        
        ABStudent *student = [ABStudent randomStudent];
        
        [allStudentsArray addObject:student];
    }
    
    self.allStudentsArray = allStudentsArray;
    
    // Settings by default:
    
    // sort students by "Month - First name - Last name"
    [self sortStudentsByMonthFirstNameLastName:self.allStudentsArray];
    
    // create sections by months & add students in corresponded section
    self.sectionsArray = [self generateSectionsByMonthFromArray:self.allStudentsArray withFilter:self.searchBar.text];

    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableViewDataSource

- (nullable NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView {
    
    tableView.sectionIndexColor = [UIColor redColor];
    
    NSMutableArray *shortNamesSectionsArray = [NSMutableArray new];
    
    if (self.sortStyleSegmentedControl.selectedSegmentIndex == ABSortStyleMonthFirstNameLastName) {
        
        for (ABStudentSection *section in self.sectionsArray) {
            [shortNamesSectionsArray addObject:section.nameShort];
        }
        
        return shortNamesSectionsArray;
    
    } else {
        
        for (ABStudentSection *section in self.sectionsArray) {
            [shortNamesSectionsArray addObject:section.nameFull];
        }
        
        return shortNamesSectionsArray;
    }
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    ABStudentSection *tmpSection = [self.sectionsArray objectAtIndex:section];
    
    return tmpSection.nameFull;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.sectionsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    ABStudentSection *tmpSection = [self.sectionsArray objectAtIndex:section];
    
    return [tmpSection.studentsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"identifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:identifier];
    }
    
    ABStudentSection *tmpSection = [self.sectionsArray objectAtIndex:indexPath.section];

    ABStudent *student = [tmpSection.studentsArray objectAtIndex:indexPath.row];
    
    NSString *studentName = [NSString stringWithFormat:@"%@ %@", student.firstName, student.lastName];
    
    NSRange currentNameRange = [studentName rangeOfString:self.searchBar.text
                                                  options:NSCaseInsensitiveSearch];
    
    cell.textLabel.text = studentName;
    
    NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:cell.textLabel.text];
    
    if ([studentName rangeOfString:self.searchBar.text
                            options:NSCaseInsensitiveSearch].location != NSNotFound) {
        
        [attrString addAttribute:NSBackgroundColorAttributeName
                           value:[UIColor yellowColor]
                           range:currentNameRange];
        
        cell.textLabel.attributedText = attrString;
        
    } else {
        
        [attrString addAttribute:NSBackgroundColorAttributeName
                           value:[UIColor clearColor]
                           range:currentNameRange];
        
        cell.textLabel.attributedText = attrString;
    }

        cell.detailTextLabel.text = [NSString stringWithFormat:@"%@", [NSDate randomDateInStringAmericanFormatForDate:student.birthDate]];
        cell.detailTextLabel.textColor = [UIColor blueColor];
    
        return cell;
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *) view;
    header.contentView.backgroundColor = [UIColor brownColor];
    header.textLabel.textColor = [UIColor whiteColor];
    header.textLabel.textAlignment = NSTextAlignmentCenter;
}


#pragma mark - UISearchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:YES animated:YES];
    
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    
    [searchBar becomeFirstResponder];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [self hidenCancelButtonForSearchBar:searchBar];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {

    [self hidenCancelButtonForSearchBar:searchBar];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    if (self.sortStyleSegmentedControl.selectedSegmentIndex == ABSortStyleMonthFirstNameLastName) {
        
        self.sectionsArray = [self generateSectionsByMonthFromArray:self.allStudentsArray
                                                         withFilter:self.searchBar.text];
    
    } else if (self.sortStyleSegmentedControl.selectedSegmentIndex == ABSortStyleFirstNameLastNameDate) {
        
        self.sectionsArray = [self generateSectionsByLastNameOrFirstNameFromArray:self.allStudentsArray withFilter:self.searchBar.text];
    
    } else {
        
        self.sectionsArray = [self generateSectionsByLastNameOrFirstNameFromArray:self.allStudentsArray withFilter:self.searchBar.text];
    }
    
    if ([searchText isEqualToString:@""]) {
        self.searchBar.text = @"";
    }
    
    [self.tableView reloadData];
}


#pragma mark - Sort arrays (NSComparator)

- (void) sortStudentsByMonthFirstNameLastName:(NSMutableArray *)currentArray {
    
    [currentArray sortUsingComparator:^NSComparisonResult(ABStudent* obj1, ABStudent* _Nonnull obj2) {
    
        if (obj1.monthNumberBirth != obj2.monthNumberBirth) {
            return  [[NSNumber numberWithUnsignedInteger: obj1.monthNumberBirth] compare:[NSNumber numberWithUnsignedInteger: obj2.monthNumberBirth]];
        } else {
            
            if (obj1.firstName != obj2.firstName) {
                return [obj1.firstName compare:obj2.firstName];
            } else {
                return [obj1.lastName compare:obj2.lastName];
            }
        }
    }];
}

- (void) sortStudentsByFirstNameLastNameDate:(NSMutableArray *)currentArray {
    
    [currentArray sortUsingComparator:^NSComparisonResult(ABStudent* obj1, ABStudent* _Nonnull obj2) {
        
        if (obj1.firstName!= obj2.firstName) {
            return  [obj1.firstName compare:obj2.firstName];
        } else {
            
            if (obj1.lastName != obj2.lastName) {
                return [obj1.lastName compare:obj2.lastName];
            } else {
                return [obj1.birthDate compare:obj2.birthDate];
            }
        }
    }];
}

- (void) sortStudentsByLastNameDateFirstName:(NSMutableArray *)currentArray {
    
    [currentArray sortUsingComparator:^NSComparisonResult(ABStudent* obj1, ABStudent* _Nonnull obj2) {
        
        if (obj1.lastName!= obj2.lastName) {
            return  [obj1.lastName compare:obj2.lastName];
        } else {
            
            if (obj1.birthDate != obj2.birthDate) {
                return [obj1.birthDate compare:obj2.birthDate];
            } else {
                return [obj1.firstName compare:obj2.firstName];
            }
        }
    }];
}


#pragma mark - Reduce methods 

- (void) hidenCancelButtonForSearchBar:(UISearchBar *)searchBar {
    
    [searchBar setShowsCancelButton:NO animated:YES];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [searchBar resignFirstResponder];
    });
}

- (NSString *) dateForStudent:(ABStudent *) student withFormat:(NSString *) format {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:format];
    
    NSString *month = [dateFormatter stringFromDate:student.birthDate];
    
    return month;
}

- (NSArray *) generateSectionsByMonthFromArray:(NSMutableArray *)array withFilter:(NSString *) filterString {
    
    NSMutableArray *sectionsArray = [NSMutableArray new];
    NSInteger currentMonth = 0;
    
    for (ABStudent *student in array) {
        
        if (filterString.length > 0 &&
            [student.firstName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound &&
            [student.lastName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound)
        {
            continue;
        }
        
        NSInteger monthBirth = student.monthNumberBirth;
        
        ABStudentSection *section = nil;
        
        if (currentMonth != monthBirth) {
            
            section = [[ABStudentSection alloc] init];
            section.studentsArray = [NSMutableArray array];
            
            section.nameFull  = [self dateForStudent:student withFormat:@"MMMM"];
            section.nameShort = [self dateForStudent:student withFormat:@"MMM"];
            
            [sectionsArray addObject:section];
            
            currentMonth = monthBirth;
            
        } else {
            section = [sectionsArray lastObject];
        }
        
        [section.studentsArray addObject:student];
    }
    
    return sectionsArray;
}

- (NSArray *) generateSectionsByLastNameOrFirstNameFromArray:(NSMutableArray *)array withFilter:(NSString *) filterString {
    
    NSMutableArray *sectionsArray = [NSMutableArray new];
    NSString *currentLetter = nil;
    
    for (ABStudent *student in array) {
        
        if (filterString.length > 0 &&
            [student.firstName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound &&
            [student.lastName rangeOfString:filterString options:NSCaseInsensitiveSearch].location == NSNotFound){
            continue;
        }
        
        NSString *firstLetter = nil;
        
        if (self.sortStyleSegmentedControl.selectedSegmentIndex == ABSortStyleFirstNameLastNameDate) {
            firstLetter = [student.firstName substringToIndex:1];
        
        } else if (self.sortStyleSegmentedControl.selectedSegmentIndex == ABSortStyleLastNameDateFirstName) {
        
            firstLetter = [student.lastName substringToIndex:1];
        }
        
        ABStudentSection *section = nil;
        
        if (![currentLetter isEqualToString:firstLetter]) {
            
            section = [[ABStudentSection alloc] init];
            section.studentsArray = [NSMutableArray array];
            
            section.nameFull = firstLetter;
            
            [sectionsArray addObject:section];
            
            currentLetter = firstLetter;
            
        } else {
            section = [sectionsArray lastObject];
        }
        
        [section.studentsArray addObject:student];
    }
    
    return sectionsArray;
}


#pragma mark - Actions

- (IBAction)changeSortStyleSegmentedControl:(UISegmentedControl *)sender {
    
    if (sender.selectedSegmentIndex == ABSortStyleMonthFirstNameLastName) {
        
        // sort students by "Month - First name - Last name"
        [self sortStudentsByMonthFirstNameLastName:self.allStudentsArray];
        
        // create sections by Months & add students in corresponded section
        self.sectionsArray = [self generateSectionsByMonthFromArray:self.allStudentsArray
                                                         withFilter:self.searchBar.text];
    
    } else {
        
        if (sender.selectedSegmentIndex == ABSortStyleFirstNameLastNameDate) {
            
            // sort students by "First name - Last name - Date of Birth"
            [self sortStudentsByFirstNameLastNameDate:self.allStudentsArray];
            
        } else {
        
            // sort students by "Last name - Date of Birth - First name"
            [self sortStudentsByLastNameDateFirstName:self.allStudentsArray];
        }
        
        // create sections by First/Last Name & add students in corresponded section
        self.sectionsArray = [self generateSectionsByLastNameOrFirstNameFromArray:self.allStudentsArray withFilter:self.searchBar.text];
    }
    
    [self.tableView reloadData];
}




















@end
