//
//  ABStudent.m
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

+ (ABStudent *) randomStudent {
    
    ABStudent *student = [[ABStudent alloc] init];
    student.firstName = [NSString randomLetterString];
    student.lastName = [NSString randomLetterString];
    student.birthDate = [NSDate randomDateOfBirthBetween:18 and:50];
    student.monthNumberBirth = [student monthNumberBirthFromDate:student.birthDate];
    
    return student;
}

- (NSUInteger) monthNumberBirthFromDate:(NSDate *)dateBirth {
    
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    
    NSUInteger monthNumber = [currentCalendar component:NSCalendarUnitMonth fromDate:dateBirth];
    
    return monthNumber;
}

@end
