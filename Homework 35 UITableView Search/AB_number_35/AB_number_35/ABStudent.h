//
//  ABStudent.h
//  AB_number_35
//
//  Created by Alexander Berezovskyy on 12.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+RandomString.h"
#import "NSDate+RandomDate.h"

@interface ABStudent : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSDate *birthDate;
@property (assign, nonatomic) NSUInteger monthNumberBirth;

+ (ABStudent *) randomStudent;
- (NSUInteger) monthNumberBirthFromDate:(NSDate *)dateBirth;

@end
