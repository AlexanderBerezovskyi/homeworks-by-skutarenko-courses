//
//  AppDelegate.m
//  AB_number_16
//
//  Created by Alexander Berezovskyy on 17.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABStudent.h"

@interface AppDelegate ()

@property (nonatomic, assign) NSInteger currentDay;
@property (nonatomic, assign) NSInteger workDaysInYear;

@property (nonatomic, strong) NSArray *sortArray;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSInteger fixYear = 2017;
    
    
    NSDateFormatter *dateFormatterYearMonthDay = [[NSDateFormatter alloc] init];
    [dateFormatterYearMonthDay setDateFormat:@"yyyy MMM dd"];
    
    NSDateFormatter *dateFormatterDayFull = [[NSDateFormatter alloc] init];
    [dateFormatterDayFull setDateFormat:@"EEEE"];
    
    NSDateFormatter *dateFormatterMonth = [[NSDateFormatter alloc] init];
    [dateFormatterMonth setDateFormat:@"MMMM"];
    
    NSDateFormatter *dateFormatterMonthDay = [[NSDateFormatter alloc] init];
    [dateFormatterMonthDay setDateFormat:@"MMMM dd"];
    
    
    ABStudent *student = [[ABStudent alloc] init];
    student.studentsArray = [NSMutableArray array];
    
    for (int i = 0; i < 30; i++) {
        
        ABStudent *s = [[ABStudent alloc] init];
        
        [student.studentsArray addObject:s];
    }
    
    
#pragma mark - NSSortDescriptor (sort by Date of Birth)
    
    NSSortDescriptor *sortDescriptorDateOfBirth = [[NSSortDescriptor alloc] initWithKey:@"dateOfBirth" ascending:NO];
    
    NSArray *sortedStudentsArray = [student.studentsArray sortedArrayUsingDescriptors:@[sortDescriptorDateOfBirth]];
    
    for (ABStudent *s in sortedStudentsArray) {
        
        NSLog(@"birthday = %@, %@ %@ (day in Year = %li)",
              [dateFormatterYearMonthDay stringFromDate:s.dateOfBirth],
              s.nameOfStudent, s.surnameOfStudent, s.numberOfDayInYear);
    }
    
    NSLog(@"***************************************************************");
    
    NSArray *sortArray = [[NSArray alloc] initWithArray:sortedStudentsArray];
    self.sortArray = sortArray;
    
    
#pragma mark - NSTimer
    
    NSTimer *timerBirthDay = [NSTimer scheduledTimerWithTimeInterval:0.5f
                                                     target:self
                                                   selector:@selector(celebrateBirthday:)
                                                   userInfo:nil
                                                    repeats:YES];
    
    [timerBirthDay fire];

#pragma mark - Oldest & Youngest students
    
    NSCalendar *currentCalendarAD = [NSCalendar currentCalendar];
    
    ABStudent *youngestStudent = [sortArray objectAtIndex:0];
    ABStudent *oldestStudent = [sortArray objectAtIndex:[sortArray count] - 1];
    
    NSDateComponents *dateComponentsAD = [currentCalendarAD components:NSCalendarUnitYear | NSCalendarUnitMonth |
                                                                       NSCalendarUnitWeekOfMonth | NSCalendarUnitDay
                                                              fromDate:oldestStudent.dateOfBirth
                                                                toDate: youngestStudent.dateOfBirth options:NO];
    
    NSLog(@"Youngest student is %@ %@ was born %@\n"
          "Oldest student is %@ %@ was born %@",
          youngestStudent.nameOfStudent, youngestStudent.surnameOfStudent,
          [dateFormatterYearMonthDay stringFromDate:youngestStudent.dateOfBirth],
          oldestStudent.nameOfStudent, oldestStudent.surnameOfStudent,
          [dateFormatterYearMonthDay stringFromDate:oldestStudent.dateOfBirth]);
    
    NSLog(@"Between oldest and youngest students %li years, %li months, %li weeks, %li days", [dateComponentsAD year], [dateComponentsAD month], [dateComponentsAD weekOfMonth], [dateComponentsAD day]);
    
    NSLog(@"***************************************************************");
    
    
#pragma mark - ***Level Superman***
    
    // First day of month
    
    NSDate *dateTmp1 = [NSDate date];
    
    NSDateComponents *dateTmpComponents = [currentCalendarAD components: NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour fromDate:dateTmp1];
    
    NSDate *date31December = [self dateFromComponets:dateTmpComponents withYear:fixYear withMonth:12 withDay:31];

    NSInteger monthCount = [currentCalendarAD component:NSCalendarUnitMonth fromDate:date31December];
    NSLog(@"monthCount = %li", monthCount);
    
    for (int i = 1; i <= monthCount; i++) {
        
        NSDate *firstDayOfMonth = [self dateFromComponets:dateTmpComponents withYear:fixYear withMonth:i withDay:1];
        
        NSLog(@"The first day of %@ is %@", [dateFormatterMonth stringFromDate:firstDayOfMonth], [dateFormatterDayFull stringFromDate:firstDayOfMonth]);
    }
    
    NSLog(@"***************************************************************");
    
    
    // Dates of Sundays in year
    
    NSDate *date1January = [self dateFromComponets:dateTmpComponents withYear:[dateTmpComponents year] withMonth:1 withDay:1];
    
    NSDateComponents *dayComponentsInYear = [currentCalendarAD components: NSCalendarUnitYear | NSCalendarUnitDay | NSCalendarUnitHour fromDate:date1January toDate:date31December options:NO];
    
    NSInteger dayInYear = [dayComponentsInYear day] + 1;
    NSLog(@"count day = %li", dayInYear);
    
    [dayComponentsInYear setYear:fixYear];
    
    for (int i = 1; i <= dayInYear; i++) {

        [dayComponentsInYear setDay:i];
        NSDate *dateDayInYear = [currentCalendarAD dateFromComponents:dayComponentsInYear];

        NSString *nameOfDayInWeek = [dateFormatterDayFull stringFromDate:dateDayInYear];
        
        if ([nameOfDayInWeek isEqualToString:@"Sunday"]) {
            
            NSLog(@"The %d-th Sunday in year - %@", i/7 + 1, [dateFormatterMonthDay stringFromDate:dateDayInYear]);
        }

        if (![nameOfDayInWeek isEqualToString:@"Sunday"] && ![nameOfDayInWeek isEqualToString:@"Saturday"]) {
            
            self.workDaysInYear ++;
        }
    }
    
    NSLog(@"Work days in this year = %li", self.workDaysInYear);
    
    NSLog(@"***************************************************************");
    
    
    // Work days in month
    
    NSDateComponents *dateComponents31December = [currentCalendarAD components: NSCalendarUnitDay | NSCalendarUnitHour fromDate:date31December];
    
    for (int i = 1; i <= monthCount; i++) {
        
        [dateComponents31December setMonth:i];
        
        NSDate *dateTmp2 = [currentCalendarAD dateFromComponents:dateComponents31December];

        NSInteger dayInMonth = [currentCalendarAD component:NSCalendarUnitDay fromDate:dateTmp2];
        
        if (dayInMonth != 31) {
            dayInMonth = 31 - [currentCalendarAD component:NSCalendarUnitDay fromDate:dateTmp2];
        }
        
        NSInteger workDaysInMonth = 0;
        
        NSDate *dayOfMonth = [NSDate date];
        
        for (int j = 1; j <= dayInMonth; j++) {
            
            dayOfMonth = [self dateFromComponets:dateTmpComponents withYear:[dateTmpComponents year] withMonth:i withDay:j];
            
            NSString *nameOfDayInMonth = [dateFormatterDayFull stringFromDate:dayOfMonth];
            
            if (![nameOfDayInMonth isEqualToString:@"Sunday"] && ![nameOfDayInMonth isEqualToString:@"Saturday"]) {
                
                workDaysInMonth ++;
            }
        }
        
        NSLog(@"In %@ - %li days (%li working)", [dateFormatterMonth stringFromDate:dayOfMonth], dayInMonth, workDaysInMonth);
    }
    NSLog(@"***************************************************************");
    
    return YES;
}


#pragma mark - Student celebrate Birthday

- (void) celebrateBirthday:(NSTimer *)timer {
    
    self.currentDay++;
    
    NSLog(@"%li day of year", self.currentDay);
    
    NSDateComponents *componentsCount = [[NSCalendar currentCalendar] components:NSCalendarUnitDay
                                                                        fromDate:[NSDate date]];
    
    [componentsCount setDay:self.currentDay];
    
    for (ABStudent *s in self.sortArray) {
        
        if (s.numberOfDayInYear == self.currentDay) {
            NSLog(@"Happy Bithday %@ %@ !", s.nameOfStudent, s.surnameOfStudent);
            break;
        }
        
        if (self.currentDay > 365) {
            self.currentDay = 0;
        }
    }
}


#pragma mark - Reduce methods

- (NSDate *) dateFromComponets:(NSDateComponents *)components withYear:(NSInteger)year withMonth:(NSInteger)month withDay:(NSInteger)day {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    
    NSDate *date = [calendar dateFromComponents:components];
    
    return date;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
