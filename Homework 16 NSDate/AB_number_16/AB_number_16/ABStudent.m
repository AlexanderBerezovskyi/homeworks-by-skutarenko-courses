//
//  ABStudent.m
//  AB_number_16
//
//  Created by Alexander Berezovskyy on 17.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.studentsNamesArray = [NSArray arrayWithObjects:@"Petr", @"Denis", @"Sergey", @"Alina", @"Maxim", @"Taras",
                                                            @"Galina", @"Karina", @"Mikhail", @"Boris", nil];
        
        self.studentsSurnamesArray = [NSArray arrayWithObjects:@"Petrov", @"Denisov", @"Sergeyev", @"Alinov",
                                                               @"Maximov", @"Tarasov", @"Galinov", @"Karinov",
                                                               @"Mikhailov", @"Borisov", nil];
        self.nameOfStudent = [self.studentsNamesArray
                              objectAtIndex:arc4random_uniform((int) [self.studentsNamesArray count])];
        
        self.surnameOfStudent = [self.studentsSurnamesArray
                                 objectAtIndex:arc4random_uniform((int) [self.studentsSurnamesArray count])];
        
        self.dateOfBirth = [self studentDateOfBirthbetween:16 and:50];
    }
    return self;
}


#pragma mark - Create random Date of Birth

- (NSDate *) studentDateOfBirthbetween:(NSInteger)firstYearOld and:(NSInteger)secondYearOld {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    
    NSDateComponents *componentsFromCurrentDate = [currentCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:currentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] - secondYearOld];
    NSDate *dateSecondYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    [componentsFromCurrentDate setYear:[componentsFromCurrentDate year] + secondYearOld - firstYearOld];
    NSDate *dateFirstYearsAgo = [currentCalendar dateFromComponents:componentsFromCurrentDate];
    
    
    NSDateComponents *componentsBetweenFirstAndSecontYearsOld = [currentCalendar components:NSCalendarUnitSecond fromDate:dateSecondYearsAgo toDate:dateFirstYearsAgo options:NO];
    
    NSInteger betweenFirstAndSecond = [componentsBetweenFirstAndSecontYearsOld second];
    
    
    NSDate *dateOfBirth = [NSDate dateWithTimeInterval:arc4random() % betweenFirstAndSecond
                                             sinceDate:dateSecondYearsAgo];
    
    
    // Day in year (Birthday)
        
    NSDateFormatter *dateFormatterDayInYear = [[NSDateFormatter alloc] init];
    [dateFormatterDayInYear setDateFormat:@"D"];
    
    self.numberOfDayInYear = [[dateFormatterDayInYear stringFromDate:dateOfBirth] integerValue];
    
    return dateOfBirth;
}




@end


