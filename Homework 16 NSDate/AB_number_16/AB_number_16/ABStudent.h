//
//  ABStudent.h
//  AB_number_16
//
//  Created by Alexander Berezovskyy on 17.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABStudent : NSObject

@property (nonatomic, strong) NSString *nameOfStudent;
@property (nonatomic, strong) NSString *surnameOfStudent;
@property (nonatomic, strong) NSDate *dateOfBirth;

@property (nonatomic, strong) NSArray *studentsNamesArray;
@property (nonatomic, strong) NSArray *studentsSurnamesArray;

@property (nonatomic, strong) NSMutableArray *studentsArray;

@property (nonatomic, assign) NSInteger numberOfDayInYear;

@end
