//
//  AppDelegate.m
//  AB_number_6_part_1-3
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABDirection.h"

@interface AppDelegate ()

@property (nonatomic, assign) Boolean hit;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
#pragma mark - typedef enum
    
    ABDirection *direction1 = [[ABDirection alloc] init];
    ABDirection *direction2 = [[ABDirection alloc] init];
    
    direction1.direction = NorthDirection;
    direction2.direction = EastDirection;
    
    NSLog(@"%@", [direction1 convertToString:direction1.direction]);
    NSLog(@"%@", [direction2 convertToString:WestDirection]);
    
    [direction1 whereAreYouGo];
    [direction2 whereAreYouGo];
    
#pragma mark - CGPoint, CGSize, CGRect
    
    NSInteger a = 0;
    NSInteger b = 0;
    
    CGPoint startPoint = CGPointMake(a, b);
    NSLog(@"start point = %@", NSStringFromCGPoint(startPoint));
    
    NSInteger width = 10;
    NSInteger height = 10;
    
    CGSize sizeRect = CGSizeMake(width, height);
    NSLog(@"size of base rect = %@", NSStringFromCGSize(sizeRect));
    
    CGRect rect = CGRectMake(a, b, width, height);
    NSLog(@"base rect = %@", NSStringFromCGRect(rect));
//    rect.origin = startPoint; - analogue
//    rect.size = sizeRect; - analogue
    
    do {
        CGRect hitRect = CGRectMake(a + 4, b + 4, 3, 3);
        CGPoint findPoint = CGPointMake(arc4random_uniform(11), arc4random_uniform(11));
        NSLog(@"find point = %@", NSStringFromCGPoint(findPoint));
        self.hit = CGRectContainsPoint(hitRect, findPoint);
        
        if (!self.hit) {
            NSLog(@"miss");
        }
        
    } while (!self.hit);
    
    if (self.hit) {
        NSLog(@"HIT");
    }
    
#pragma mark - NSNumber, NSValue
    
    NSInteger intVar    = - 15;
    NSUInteger unIntVar = 6;
    CGFloat floatVar    = 2.34f;
    Boolean booleanVar  = YES;
    double doubleVar    = 6.123456f;
    char charVar1       = 't';
    char charVar2       = ':';
    
    NSNumber *intVarObj = [NSNumber numberWithInteger:intVar];
    NSNumber *unIntVarObj = [NSNumber numberWithUnsignedInteger:unIntVar];
    NSNumber *floatVarObj = [NSNumber numberWithFloat:floatVar];
    NSNumber *booleanVarObj = [NSNumber numberWithBool:booleanVar];
    NSNumber *doubleVarObj = [NSNumber numberWithDouble:doubleVar];
    NSNumber *charVar1Obj = [NSNumber numberWithUnsignedChar:charVar1];
    NSNumber *charVar2Obj = [NSNumber numberWithUnsignedChar:charVar2];
    
    NSArray *objArray = [[NSArray alloc] initWithObjects:intVarObj, unIntVarObj, floatVarObj, booleanVarObj, doubleVarObj, charVar1Obj, charVar2Obj, nil];
    
    for (NSNumber *obj in objArray) {
        NSLog(@"%@", obj);
    }
    
    NSLog(@"char №116 is \"%c\"",[[objArray objectAtIndex:5] charValue]);
    NSLog(@"char №58 is \"%c\"",[[objArray objectAtIndex:6] charValue]);
    NSLog(@"boolean value - \"%@\"", [[objArray objectAtIndex:3] boolValue] == YES ? @"YES" : @"NO");
    
    
    CGRect newRect   = CGRectMake(1, 1, 1, 1);
    CGPoint newPoint = CGPointMake(2, 2);
    CGSize newSize   = CGSizeMake(3, 3);
    
    NSArray *arraySG = [NSArray arrayWithObjects:[NSValue valueWithCGRect:newRect],
                                                 [NSValue valueWithCGPoint:newPoint],
                                                 [NSValue valueWithCGSize:newSize], nil];
    
    for (NSValue *sg in arraySG) {
        NSLog(@"%@", sg);
    }
    
    NSLog(@"newRect = %@, newPoint = %@, newSize = %@", NSStringFromCGRect(newRect), NSStringFromCGPoint(newPoint), NSStringFromCGSize(newSize));
    
    
    
    // Override point for customization after application launch.
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
