//
//  ABDirection.m
//  AB_number_6_part_1-3
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDirection.h"

@implementation ABDirection

- (NSString *)convertToString:(Direction) yourDirection {
    
    NSString *resultDirection = nil;
    
    switch (yourDirection) {
        case EastDirection:
            resultDirection = @"EAST";
            break;
            
        case WestDirection:
            resultDirection = @"WEST";
            break;
            
        case NorthDirection:
            resultDirection = @"NORTH";
            break;
            
        case SouthDirection:
            resultDirection = @"SOUTH";
            break;
            
        default:
            resultDirection = @"Direction not found";
            break;
    }
    return resultDirection;
}

- (void) whereAreYouGo {
    
    NSLog(@"I am going to the %@", [self convertToString:self.direction]);
}


















@end
