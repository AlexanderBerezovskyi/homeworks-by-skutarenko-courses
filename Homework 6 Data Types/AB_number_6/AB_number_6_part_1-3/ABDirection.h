//
//  ABDirection.h
//  AB_number_6_part_1-3
//
//  Created by Alexander Berezovskyy on 30.01.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    EastDirection,
    WestDirection,
    SouthDirection,
    NorthDirection
    
} Direction;

@interface ABDirection : NSObject

@property (nonatomic, assign) Direction direction;

- (NSString *)convertToString:(Direction) yourDirection;

- (void) whereAreYouGo;

@end
