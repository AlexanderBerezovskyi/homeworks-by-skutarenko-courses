//
//  ViewController.m
//  AB_number_30
//
//  Created by Alexander Berezovskyy on 26.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"
#import "ABColor.h"
#import "ABStudent.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (assign, nonatomic) CGFloat r;
@property (assign, nonatomic) CGFloat g;
@property (assign, nonatomic) CGFloat b;

// colors

@property (weak, nonatomic) IBOutlet UITableView *colorTableView;
@property (strong, nonatomic) NSMutableArray *colorsArray;

// students

@property (strong, nonatomic) NSMutableArray *allStudentsArray;
@property (strong, nonatomic) NSArray *allGroupsStudentsArray;
@property (strong, nonatomic) NSArray *allGroupsNamesArray;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(40, 0, 0, 0);
    self.colorTableView.contentInset = insets;
    self.colorTableView.scrollIndicatorInsets = insets;
    
    self.allStudentsArray = [NSMutableArray new];
    
    for (int i = 0; i < 30; i++) {
        
        ABStudent *studentObj = [[ABStudent alloc] init];
        
        [self.allStudentsArray addObject:studentObj];
    }
    
    // sort students
    
    self.allStudentsArray = [NSMutableArray arrayWithArray:[self sortStudentsArrayBySurnameAndName]];
    
    // sort students by average mark
    
    NSMutableArray *goodStudentsArray = [NSMutableArray new];
    NSMutableArray *normalStudentsArray = [NSMutableArray new];
    NSMutableArray *badStudentsArray = [NSMutableArray new];
    
    for (ABStudent *student in self.allStudentsArray) {
        
        if (student.averageMark >= 4 && student.averageMark <= 5) {
            
            [goodStudentsArray addObject:student];
        
        } else if (student.averageMark >= 3 && student.averageMark < 4) {
            
            [normalStudentsArray addObject:student];
            
        } else {
            
            [badStudentsArray addObject:student];
        }
    }
    
    // create 1 color group
    
    self.colorsArray = [NSMutableArray new];
    
    for (int i = 0; i < 10; i++) {
        
        ABColor *colorObj = [[ABColor alloc] init];
        colorObj.color  = [self setColor];
        colorObj.name   = [NSString stringWithFormat:@"RGB (%.f, %.f, %.f)", self.r, self.g, self.b];
        
        [self.colorsArray addObject:colorObj];
    }
    
    // create array with all students groups and 1 color group
    
    self.allGroupsStudentsArray = [[NSArray alloc] initWithObjects:goodStudentsArray, normalStudentsArray, badStudentsArray, self.colorsArray, nil];
    
    self.allGroupsNamesArray = [[NSArray alloc] initWithObjects:@"Good students", @"Normal students", @"Bad students", @"Colors", nil];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return self.allGroupsStudentsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
//    return 1000;
    
    NSArray *groupArray = [self.allGroupsStudentsArray objectAtIndex:section];
    
    return groupArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *studentCellIdentifier = @"studentCellIdentifier";
    NSString *colorCellIdentifier   = @"colorCellIdentifier";
    
    if (indexPath.section >= 0 && indexPath.section <= 2) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:studentCellIdentifier];
        
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                          reuseIdentifier:studentCellIdentifier];
        }
        
        NSArray *currentGroupArray = [self.allGroupsStudentsArray objectAtIndex:indexPath.section];
        
        ABStudent *currentStudent = [currentGroupArray objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [NSString stringWithFormat:@"%@ %@", currentStudent.name, currentStudent.surname];
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f", currentStudent.averageMark];
        
        if (currentStudent.averageMark < 3.f) {
            cell.textLabel.textColor = [UIColor redColor];
        } else {
            cell.textLabel.textColor = [UIColor blackColor];
        }
        
        return cell;
        
    } else {

        UITableViewCell *cellColor = [tableView dequeueReusableCellWithIdentifier:colorCellIdentifier];
        
        if (!cellColor) {
            
            cellColor = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:colorCellIdentifier];
        }
        
        ABColor *currentObject = [self.colorsArray objectAtIndex:indexPath.row];
        
        cellColor.textLabel.textColor = [self setColor];
        cellColor.textLabel.text = [NSString stringWithFormat:@"RGB (%.f, %.f, %.f)", self.r, self.g, self.b];
        
        cellColor.textLabel.textColor = currentObject.color;
        cellColor.textLabel.text = currentObject.name;
        
        return cellColor;
    }
}

- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    
    return [self.allGroupsNamesArray objectAtIndex:section];
}


#pragma mark - Colors

- (UIColor *) setColor {
    
    self.r = arc4random_uniform(256);
    self.g = arc4random_uniform(256);
    self.b = arc4random_uniform(256);
    
    UIColor *color = [UIColor colorWithRed:self.r/255.f green:self.g/255.f blue:self.b/255.f alpha:1.f];
    
    return color;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 30.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    return 15.f;
}


#pragma mark - Sort students

- (NSArray *) sortStudentsArrayBySurnameAndName {
    
    NSArray *sortedStudentsArray = [[NSArray alloc] init];
    
    sortedStudentsArray = [self.allStudentsArray sortedArrayUsingComparator:^NSComparisonResult(ABStudent *student1, ABStudent *student2) {
        
        if ([student1.surname isEqualToString:student2.surname]) {
            
            return [student1.name compare:student2.name
                                  options:NSCaseInsensitiveSearch | NSNumericSearch];
        }
        
        return [student1.surname compare:student2.surname
                                 options:NSCaseInsensitiveSearch | NSNumericSearch];
    }];
    
    return sortedStudentsArray;
}



@end
