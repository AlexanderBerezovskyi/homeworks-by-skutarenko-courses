//
//  ABStudent.m
//  AB_number_30
//
//  Created by Alexander Berezovskyy on 26.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

static NSString *names[] = {@"Georgan", @"Oigenhein", @"Denya", @"Sergio", @"Arslanito", @"Igorio"};
static NSString *surnames[] = {@"Petrov", @"Ivanov", @"Sydorov", @"Mamontov", @"Vetrov", @"Vorotov"};

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = names[arc4random_uniform(6)];
        self.surname = surnames[arc4random_uniform(6)];
        self.averageMark = arc4random_uniform(301)/100.f + 2;
    }
    return self;
}

@end
