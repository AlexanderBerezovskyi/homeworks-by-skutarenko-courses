//
//  ABStudent.h
//  AB_number_30
//
//  Created by Alexander Berezovskyy on 26.04.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ABStudent : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;
@property (assign, nonatomic) CGFloat averageMark;

@end
