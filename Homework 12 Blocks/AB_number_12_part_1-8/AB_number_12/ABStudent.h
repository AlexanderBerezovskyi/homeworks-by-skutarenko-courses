//
//  ABStudent.h
//  AB_number_12
//
//  Created by Alexander Berezovskyy on 09.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ABStudent : NSObject

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;

#pragma mark - Class Methods

+ (ABStudent *) withFirstName:(NSString *)firstName andLastName:(NSString *)lastName;

@end
