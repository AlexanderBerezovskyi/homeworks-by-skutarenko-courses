//
//  AppDelegate.m
//  AB_number_12
//
//  Created by Alexander Berezovskyy on 09.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABStudent.h"

typedef void (^BlockWithoutParameters)(void);
typedef NSString* (^BlockWithReturnParameter)(void);
typedef NSInteger (^ReturnSumBlock)(NSInteger a, CGFloat b);
typedef void (^TakeParametersBlock)(NSString* string, NSUInteger uIntValue, CGFloat floatValue);


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

#pragma mark - Different blocks
    
    BlockWithoutParameters block1 = ^{
        NSLog(@"Block without parameters");
    };
    
    block1 ();
    
    
    BlockWithReturnParameter block2 = ^{
        return [NSString stringWithFormat:@"Block with return parameter"];
    };
    
    NSLog(@"return = %@", block2 ());
    
    
    ReturnSumBlock block3 = ^(NSInteger intValue, CGFloat floatValue) {
        return (NSInteger)(intValue + floatValue);
    };
    
    NSLog(@"sum = %li", block3 (5, 6.7f));
    
    TakeParametersBlock block4 = ^(NSString* string, NSUInteger uIntValue, CGFloat floatValue) {
        NSLog(@"string = %@, integer = %lu, float = %.2f", string, uIntValue, floatValue);
    };
    
    block4 (@"Hello", 5, 26.498f);
    
    
#pragma mark - Use Methods With Blocks
    
    [self methodWithNoneReturnBlock:^{
        NSLog(@"How are you?");
    }];
    
    
    NSInteger numberFromMethod = [self methodWithReturnBlock:^NSInteger (NSInteger a, CGFloat b) {
        NSLog(@"sum (a + b) = %li", (NSInteger) (a + b));
        
        NSInteger c = (a + b) * 1000;
        return c;
    }];
    
    NSLog(@"number from method = %li", numberFromMethod);
    

#pragma mark - ABStudent
    
    ABStudent *student1 = [ABStudent withFirstName:@"Billy" andLastName:@"Petrenko"];
    ABStudent *student2 = [ABStudent withFirstName:@"Alex" andLastName:@"Petrenko"];
    ABStudent *student3 = [ABStudent withFirstName:@"Yuriy" andLastName:@"Postenko"];
    ABStudent *student4 = [ABStudent withFirstName:@"Sasha" andLastName:@"Ronenko"];
    ABStudent *student5 = [ABStudent withFirstName:@"Kirill" andLastName:@"Ivanov"];
    ABStudent *student6 = [ABStudent withFirstName:@"Dasha" andLastName:@"Yatsenko"];
    ABStudent *student7 = [ABStudent withFirstName:@"Cortney" andLastName:@"Petrenko"];
    ABStudent *student8 = [ABStudent withFirstName:@"Anna" andLastName:@"Bahmut"];
    ABStudent *student9 = [ABStudent withFirstName:@"Yanina" andLastName:@"Petrenko"];
    ABStudent *student10 = [ABStudent withFirstName:@"Olya" andLastName:@"Petrenko"];
    
    
    NSArray *students = [[NSArray alloc] initWithObjects:student1, student2, student3, student4, student5, student6, student7, student8, student9, student10, nil];
    
    NSComparisonResult (^sortArrayBlock) (ABStudent*, ABStudent*) = ^(ABStudent *studentFirst, ABStudent *studentSecond) {
        
        if ([studentFirst.lastName isEqual:studentSecond.lastName]) {
            return [studentFirst.firstName compare:studentSecond.firstName];
        }
        return [studentFirst.lastName compare:studentSecond.lastName];

    };
    
    NSArray *sortedStudentsArray = [[NSArray alloc] init];
    sortedStudentsArray = [students sortedArrayUsingComparator:sortArrayBlock];
    
    for (ABStudent *s in sortedStudentsArray) {
        NSLog(@"%@ %@", s.lastName, s.firstName);
    }
    
    
    // Override point for customization after application launch.
    return YES;
}

#pragma mark - Help Methods

- (void) methodWithNoneReturnBlock:(BlockWithoutParameters) block {
    
    NSLog(@"methodWithNoneReturnBlock");
    
    block ();
    block ();
    block ();
}

- (NSInteger) methodWithReturnBlock:(ReturnSumBlock) block {
    
    NSLog(@"methodWithBlockReturn");
    
    NSInteger x = arc4random_uniform(10);
    NSInteger y = arc4random_uniform(20);
    
    return block (x, y);
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
