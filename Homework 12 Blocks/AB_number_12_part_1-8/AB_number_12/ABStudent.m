//
//  ABStudent.m
//  AB_number_12
//
//  Created by Alexander Berezovskyy on 09.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABStudent.h"

@implementation ABStudent

#pragma mark - Class Methods

+ (ABStudent *) withFirstName:(NSString *)firstName andLastName:(NSString *)lastName {
    
    ABStudent *student = [[ABStudent alloc] init];
    student.firstName = firstName;
    student.lastName = lastName;
    
    return student;
}

@end
