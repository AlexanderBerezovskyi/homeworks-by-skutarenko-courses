//
//  ABPatient.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABPatient.h"


@implementation ABPatient

#pragma mark - Initialization

- (instancetype)init
{
    self = [super init];
    if (self) {
        
        self.name = @"none";
        self.temperature = 36.6f;
        self.headache = NO;
        self.cough = NO;
        self.doctorMark = 5;
        
    }
    return self;
}


#pragma mark - Class Methods

+ (ABPatient *) temperatureCoughHeadacheForName:(NSString *) firstName {
    
    ABPatient *patient = [[ABPatient alloc] init];
    patient.name = firstName;
    patient.temperature = (CGFloat)(arc4random_uniform(51) + 360) / 10;
    patient.cough = (Boolean) arc4random_uniform(201)/100;
    patient.headache = (Boolean) arc4random_uniform(201)/100;
    patient.bodyPart = arc4random_uniform(3);
    
    return patient;
}


#pragma mark - Feeling Methods

- (Boolean) howAreYou {
    
    NSLog(@"xXx %@ xXx", self.name);
    
    Boolean iFeelGood = (Boolean) arc4random_uniform(201)/100;
    
    if (!iFeelGood) {
        
        if (self.bodyPart == ABPartOfBodyHand) {
            NSLog(@"I have some problem with my: *hand*");
        } else if (self.bodyPart == ABPartOfBodyLeg) {
            NSLog(@"I have some problem with my: *leg*");
        } else if (self.bodyPart == ABPartOfBodyStomach) {
            NSLog(@"I have some problem with my: *stomach*");
        } else {
            NSLog(@"something went wrong");
        }
    
    } else {
        NSLog(@"Patient %@ is OK", self.name);
    }
    
    return iFeelGood;
}


#pragma mark - Treat Methods

- (void) takePill {
    
    NSLog(@"%@ takes a pill", self.name);
}

- (void) makeShot {
    
    NSLog(@"%@ make a shot", self.name);
}

- (void) treatHand {
    
    NSLog(@"doctorBlock Treat the hand");
}

- (void) treatLeg {
    
    NSLog(@"doctorBlock Treat the leg");
}

- (void) treatStomach {
    
    NSLog(@"doctorBlock Treat the stomach");
}


- (void) dealloc {
    
    NSLog(@"%@ deallocated", self.name);
}


@end

