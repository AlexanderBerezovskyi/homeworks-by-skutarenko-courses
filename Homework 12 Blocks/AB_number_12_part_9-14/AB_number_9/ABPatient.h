//
//  ABPatient.h
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


typedef enum {
    
    ABPartOfBodyLeg,
    ABPartOfBodyHand,
    ABPartOfBodyStomach

} ABPartOfBody;

@interface ABPatient : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CGFloat temperature;
@property (nonatomic, assign) Boolean headache;
@property (nonatomic, assign) Boolean cough;
@property (nonatomic, assign) ABPartOfBody bodyPart;
@property (nonatomic, assign) NSUInteger doctorMark;

typedef void (^DoctorBlock)(ABPatient *patient);

#pragma mark - Class Methods

+ (ABPatient *) temperatureCoughHeadacheForName:(NSString *) firstName;


#pragma mark - Feeling Methods

- (Boolean) howAreYou;


#pragma mark - Treat Methods

- (void) takePill;
- (void) makeShot;

- (void) treatHand;
- (void) treatLeg;
- (void) treatStomach;

@end





