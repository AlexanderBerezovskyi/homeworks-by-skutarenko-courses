//
//  AppDelegate.m
//  AB_number_9
//
//  Created by Alexander Berezovskyy on 06.02.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "AppDelegate.h"
#import "ABPatient.h"

typedef void (^DoctorBlock)(ABPatient *patient);

@interface AppDelegate ()

@property (nonatomic, strong) NSArray *patients;
@property (nonatomic, copy) DoctorBlock doctorBlock;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    ABPatient *patient1 = [ABPatient temperatureCoughHeadacheForName:@"Patient_1"];
    ABPatient *patient2 = [ABPatient temperatureCoughHeadacheForName:@"Patient_2"];
    ABPatient *patient3 = [ABPatient temperatureCoughHeadacheForName:@"Patient_3"];
    ABPatient *patient4 = [ABPatient temperatureCoughHeadacheForName:@"Patient_4"];
    ABPatient *patient5 = [ABPatient temperatureCoughHeadacheForName:@"Patient_5"];

    
    NSArray *patients = [[NSArray alloc] initWithObjects:patient1, patient2, patient3, patient4, patient5, nil];
    
    self.patients = patients;

    self.doctorBlock = ^(ABPatient *patient){
      
        NSLog(@"Patient %@ feels bad", patient.name);
        NSLog(@"Doctor: Do you have a headache? - %@", patient.headache == NO ? @"NO" : @"YES");
        NSLog(@"Doctor: Do you have a cough? - %@", patient.headache == NO ? @"NO" : @"YES");
        
        if (patient.temperature > 37.f && patient.temperature <= 39.f) {
            [patient takePill];
            
        } else if (patient.temperature > 39.f) {
            [patient makeShot];
            
        } else {
            NSLog(@"Patient %@ should rest", patient.name);
        }
        
        patient.doctorMark = (NSUInteger) (5 - arc4random_uniform(4));
        
        switch (patient.bodyPart) {
                
            case ABPartOfBodyLeg:
                [patient treatLeg];
                break;
                
            case ABPartOfBodyHand:
                [patient treatHand];
                break;
                
            case ABPartOfBodyStomach:
                [patient treatStomach];
                break;
                
            default:
                NSLog(@"You don't have any promlems");
                break;
        }
    };
    
//    *Level - Master*
    
//    for (ABPatient *patient in self.patients) {
//
//        Boolean iFeelGood = [patient howAreYou];
//       
//        if (!iFeelGood) {
//            [self treatMethodForPatient:patient withBlock:self.doctorBlock];
//            NSLog(@"Doctors rating = %lu", patient.doctorMark);
//        }
//        NSLog(@"---------------------------------------------");
//    }
    
    
//    *Level - Superman*
    
    for (ABPatient *patient in self.patients) {
        
        [self performSelector:@selector(treatMethodForPatient2:) withObject:patient afterDelay:arc4random_uniform(30) + 5];
        
    }

    
    return YES;
}

#pragma mark - Methods With Blocks

- (void) treatMethodForPatient:(ABPatient*) patient withBlock:(DoctorBlock) block {
    block (patient);
}

- (void) treatMethodForPatient2:(ABPatient*) patient {
    if (![patient howAreYou]) {
        self.doctorBlock (patient);
    }
    NSLog(@"---------------------------------------------");
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
