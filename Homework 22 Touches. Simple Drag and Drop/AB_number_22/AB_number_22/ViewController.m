//
//  ViewController.m
//  AB_number_22
//
//  Created by Alexander Berezovskyy on 03.03.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSMutableArray *darkSquaresArray;
@property (nonatomic, strong) NSMutableArray *allCheckersCentersArray;
@property (nonatomic, strong) NSMutableArray *allCheckersArray;

@property (nonatomic, strong) UIView *checkerBoardView;
@property (nonatomic, weak) UIView *draggingView;

@property (nonatomic, assign) CGPoint centerCurrentView;
@property (nonatomic, assign) CGPoint touchOffset;
@property (nonatomic, assign) CGFloat squareSize;

@property (nonatomic, strong) NSMutableDictionary *dictionaryDiagonals;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.checkerBoardView = [[UIView alloc] init];
    
    self.darkSquaresArray =         [NSMutableArray array];
    self.allCheckersCentersArray =  [NSMutableArray array];
    self.allCheckersArray =         [NSMutableArray array];
    self.dictionaryDiagonals =      [NSMutableDictionary dictionary];
    
    self.view.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:1.f];
    
    CGFloat viewWidth = CGRectGetWidth(self.view.bounds);
    CGFloat viewHeight = CGRectGetHeight(self.view.bounds);
    
    // Checker board size
    
    CGFloat checkerBoardSize = MIN(viewWidth, viewHeight);
    
    CGRect checkerBoard = CGRectMake((CGRectGetWidth(self.view.bounds) - checkerBoardSize) / 2,
                                     (CGRectGetHeight(self.view.bounds) - checkerBoardSize) / 2,
                                     checkerBoardSize,
                                     checkerBoardSize);
    
    self.checkerBoardView = [[UIView alloc] initWithFrame:checkerBoard];
    
    self.checkerBoardView.backgroundColor = [[UIColor alloc] initWithRed:(float) 255/255 green: (float) 228/255 blue:(float) 225/255 alpha:1];
    self.checkerBoardView.layer.borderWidth = 3.f;
    self.checkerBoardView.layer.shadowOpacity = 10.f;
    self.checkerBoardView.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.checkerBoardView.layer.shadowColor = [UIColor whiteColor].CGColor;
    
    self.checkerBoardView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    [self.view addSubview:self.checkerBoardView];
    
    CGFloat squareSize = checkerBoardSize / 8;
    CGFloat checkerSize = squareSize * 0.75;
    self.squareSize = squareSize;
    
    for (int i = 0; i < 8; i++) {
        
        for (int j = 0; j < 8; j++) {
            
            if (((i % 2 == 1) && (j % 2 == 1)) || ((i % 2 == 0) && (j % 2 == 0))) {
                
                CGRect rectTmp = CGRectMake(i * squareSize, j * squareSize, squareSize, squareSize);
                
                UIView *blackSquareView = [[UIView alloc] initWithFrame:rectTmp];
                
                blackSquareView.backgroundColor = [[UIColor alloc] initWithRed:(float) 198/256
                                                                         green:(float) 166/255
                                                                          blue:(float) 100/255
                                                                         alpha:1];
                
                [self.checkerBoardView addSubview:blackSquareView];
                [self.darkSquaresArray addObject:blackSquareView];
                
                // Create checkers
                
                UIView *checkerView = [[UIView alloc] initWithFrame:
                                       CGRectMake((squareSize - checkerSize) / 2 + i * squareSize,
                                                  (squareSize - checkerSize) / 2 + j * squareSize,
                                                  checkerSize,
                                                  checkerSize)];
                
                checkerView.layer.cornerRadius = checkerSize/2;
                checkerView.layer.shadowColor = [[UIColor whiteColor] colorWithAlphaComponent:0.9f].CGColor;
                checkerView.layer.shadowOpacity = 7.f;
                
                // Paint checkers in Red
                
                if (j < 3) {
                    
                    checkerView.backgroundColor = [[UIColor redColor] colorWithAlphaComponent:0.9f];
                    [self.checkerBoardView addSubview:checkerView];
                    [self.allCheckersArray addObject:checkerView];
                    
                    NSString *centerOfRedCheckerView = NSStringFromCGPoint(checkerView.center);
                    [self.allCheckersCentersArray addObject:centerOfRedCheckerView];
                    
                // Paint checkers in Green
                    
                } else if (j > 4) {
                    
                    checkerView.backgroundColor = [[UIColor greenColor] colorWithAlphaComponent:0.9f];
                    [self.checkerBoardView addSubview:checkerView];
                    [self.allCheckersArray addObject:checkerView];
                    
                    NSString *centerOfGreenCheckerView = NSStringFromCGPoint(checkerView.center);
                    [self.allCheckersCentersArray addObject:centerOfGreenCheckerView];
                }
            }
        }
    }
}

#pragma mark - UITouch & UIResponder

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint startPointOnCheckerBoard = [touch locationInView:self.checkerBoardView];
    
    UIView *currentView = [self.checkerBoardView hitTest:startPointOnCheckerBoard
                                               withEvent:event];
    
    CGPoint centerCurrentView =  currentView.center;
    self.centerCurrentView = centerCurrentView;
    
    if ([self.allCheckersArray containsObject:currentView]) {
        
        self.draggingView = currentView;
        
        [self.checkerBoardView bringSubviewToFront:self.draggingView];
        
        CGPoint touchPoint = [touch locationInView:self.draggingView];
        
        self.touchOffset = CGPointMake(CGRectGetMidX(self.draggingView.bounds) - touchPoint.x,
                                       CGRectGetMidY(self.draggingView.bounds) - touchPoint.y);

    }
    
    [self stateWithAlpha:0.8f withTransform:CGAffineTransformMakeScale(1.2f, 1.2f) andAnimationDuration:0.3f];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint movedPointOnCheckerBoard = [touch locationInView:self.checkerBoardView];
    
    CGPoint correction =CGPointMake(movedPointOnCheckerBoard.x + self.touchOffset.x,
                                    movedPointOnCheckerBoard.y + self.touchOffset.y);
    
    self.draggingView.center = correction;
    
//    self.draggingView.center = movedPointOnCheckerBoard;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {

    UITouch *touch = [touches anyObject];
    CGPoint finalPointOnCheckerBoard = [touch locationInView:self.checkerBoardView];
    
    for (UIView *darkSquare in self.darkSquaresArray) {
        
        CGPoint centerDarkSquare = CGPointMake(CGRectGetMidX(darkSquare.frame),
                                               CGRectGetMidY(darkSquare.frame));
        
        if (finalPointOnCheckerBoard.x < 0 ||
            finalPointOnCheckerBoard.y < 0 ||
            finalPointOnCheckerBoard.x >= self.squareSize * 7.9 ||
            finalPointOnCheckerBoard.y >= self.squareSize * 7.9) {
            
            self.draggingView.center = self.centerCurrentView;
            [self stateWithAlpha:1.f withTransform:CGAffineTransformIdentity andAnimationDuration:0.3f];
            self.draggingView = nil;
        }
        
        else if (centerDarkSquare.x - finalPointOnCheckerBoard.x < self.squareSize &&
                 centerDarkSquare.y - finalPointOnCheckerBoard.y < self.squareSize &&
                 centerDarkSquare.x - finalPointOnCheckerBoard.x > - self.squareSize &&
                 centerDarkSquare.y - finalPointOnCheckerBoard.y > - self.squareSize) {
            
            CGFloat tmpDiagonal = pow((centerDarkSquare.x - finalPointOnCheckerBoard.x), 2) + pow(centerDarkSquare.y - finalPointOnCheckerBoard.y, 2);
            CGFloat diagonal = sqrt(tmpDiagonal);
            
            [self.dictionaryDiagonals setObject:darkSquare forKey:[NSNumber numberWithFloat:diagonal]];
            
            self.draggingView.center = centerDarkSquare;
        }
    }
    
    // Find final destination for checker
    
    NSArray *sortedArrayDiagonals = [[self.dictionaryDiagonals allKeys] sortedArrayUsingSelector:@selector(compare:)];
    
    UIView *finalBlackSquare = [self.dictionaryDiagonals objectForKey:[sortedArrayDiagonals firstObject]];
    
    // If place are not free
    
    if ([self.allCheckersCentersArray containsObject:NSStringFromCGPoint(finalBlackSquare.center)]) {
        
        self.draggingView.center = self.centerCurrentView;
        
        [self stateWithAlpha:1.f withTransform:CGAffineTransformIdentity andAnimationDuration:0.3f];
        
        self.draggingView = nil;
        
    // If place are free
        
    } else {
        
        [self.allCheckersCentersArray addObject:NSStringFromCGPoint(finalBlackSquare.center)];
        self.draggingView.center = finalBlackSquare.center;
        [self.allCheckersCentersArray removeObject:NSStringFromCGPoint(self.centerCurrentView)];
    }
    
    [self stateWithAlpha:1.f withTransform:CGAffineTransformIdentity andAnimationDuration:0.3f];
    
    self.draggingView = nil;
    [self.dictionaryDiagonals removeAllObjects];
}

- (void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(nullable UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    CGPoint movedPointOnCheckerBoard = [touch locationInView:self.checkerBoardView];
    
    movedPointOnCheckerBoard = self.centerCurrentView;
    
    self.draggingView = nil;
}

#pragma mark - Reduce Methods

- (void) stateWithAlpha:(CGFloat)alpha withTransform:(CGAffineTransform)transform andAnimationDuration:(CGFloat)animationDuration {
    
    [UIView animateWithDuration:animationDuration animations:^{
        
        self.draggingView.transform = transform;
        self.draggingView.alpha = alpha;
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
