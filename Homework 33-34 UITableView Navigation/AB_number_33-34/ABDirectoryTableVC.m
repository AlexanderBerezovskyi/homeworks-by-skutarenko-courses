//
//  ABDirectoryTableVC.m
//  AB_number_33-34
//
//  Created by Alexander Berezovskyy on 08.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABDirectoryTableVC.h"
#import "ABFileCell.h"

@interface ABDirectoryTableVC ()

@property (strong, nonatomic) NSString *path;

@property (strong, nonatomic) NSArray *contentsArray;
@property (strong, nonatomic) NSArray *sortedArray;

@property (strong, nonatomic) NSString *folderName;
@property (assign, nonatomic) BOOL folderCreated;

@end

@implementation ABDirectoryTableVC

- (id)initWithFolderPath:(NSString *) folderPath {
    self = [super initWithStyle:UITableViewStyleGrouped];
    
    if (self) {
        self.path = folderPath;
    }
    return self;
}

- (void) setPath:(NSString *)path {
    
    _path = path;
    
    self.contentsArray = [self getContentsFromPath:path];
    
    [self.tableView reloadData];
    
    self.navigationItem.title = [self.path lastPathComponent];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if (self.navigationController.viewControllers.count > 1) {
        
        UIBarButtonItem *rootItem = [[UIBarButtonItem alloc] initWithTitle:@"Root"
                                                                     style:UIBarButtonItemStylePlain
                                                                    target:self
                                                                    action:@selector(backToRootAction:)];
        self.navigationItem.rightBarButtonItem = rootItem;
    }
    
    self.contentsArray = [self getContentsFromPath:self.path];
    self.sortedArray = [self sortedArrayByFoldersAndFiles:self.contentsArray];
    
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [self.navigationController setHidesBarsOnSwipe:YES];
    [self.navigationController setToolbarHidden:NO animated:YES];
    
    if (!self.path) {
        self.path = @"/Users/alexanderberezovskyy/Downloads";
    }

    self.sortedArray = [self sortedArrayByFoldersAndFiles:self.contentsArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Actions

- (void) backToRootAction:(UIBarButtonItem *) sender {

    [self.navigationController popToRootViewControllerAnimated:YES];


}

- (IBAction)addFolderAction:(UIBarButtonItem *)sender {
    
    UIAlertController *folderNameAlert =
    [UIAlertController alertControllerWithTitle:@"Name"
                                        message:@"Enter name for new folder"
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    [folderNameAlert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        
    }];
    
    UIAlertAction *cancel =
    [UIAlertAction actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                           handler:^(UIAlertAction * _Nonnull action) {
                   
                           }];
    
    UIAlertAction *ok =
    [UIAlertAction actionWithTitle:@"OK"
                             style:UIAlertActionStyleDefault
                           handler:^(UIAlertAction * _Nonnull action) {
                                   
       if (folderNameAlert.textFields.count > 0 &&
           ![[folderNameAlert.textFields lastObject].text isEqualToString:@""]) {
           
           self.folderName = [folderNameAlert.textFields lastObject].text;
           
           [self createFolderWithPath:self.path];
           
           if (self.folderCreated == NO) {
               UIAlertController *badNameAlert =
               [UIAlertController alertControllerWithTitle:@"Error"
                                                   message:@"Folder with this name already exists / improper name"
                                            preferredStyle:UIAlertControllerStyleActionSheet];
               
               UIAlertAction *okAction =
               [UIAlertAction actionWithTitle:@"OK"
                                        style:UIAlertActionStyleDestructive
                                      handler:^(UIAlertAction * _Nonnull action) {
                                          
                                      }];
               
               [badNameAlert addAction:okAction];
               
               [self presentViewController:badNameAlert
                                  animated:YES
                                completion:nil];
               
           }
       }
                           }];
    
    [folderNameAlert addAction:cancel];
    [folderNameAlert addAction:ok];
    
    [self presentViewController:folderNameAlert
                       animated:YES
                     completion:nil];
}




#pragma mark - NSFileManager methods

- (BOOL) isDirectoryAtIndexPathRow:(NSIndexPath *) currentIndexPath {
    
    NSString *fileName = [self.sortedArray objectAtIndex:currentIndexPath.row];
    NSString *filePath = [self.path stringByAppendingPathComponent:fileName];
    
    BOOL isDirectory = [self isDirectoryAtPath:filePath];
    
    return isDirectory;
}

- (BOOL) isDirectoryAtPath:(NSString *) currentPath {
    
    BOOL isDirectory = NO;
    
    [[NSFileManager defaultManager] fileExistsAtPath:currentPath
                                         isDirectory:&isDirectory];
    
    return isDirectory;
}

- (BOOL) createFolderWithPath:(NSString *)newFolderDirectoryPath {
    
    self.folderCreated = YES;
    
    NSString *newFolderName = self.folderName;
    NSString *filePath = [newFolderDirectoryPath stringByAppendingPathComponent:newFolderName];
    
    NSError *error = nil;
    
    if (![[NSFileManager defaultManager] createDirectoryAtPath:filePath
                                   withIntermediateDirectories:NO
                                                    attributes:nil
                                                         error:&error]) {
        NSLog(@"Create directory error: %@", error);
        
        self.folderCreated = NO;
    }
    
    self.contentsArray = [self getContentsFromPath:newFolderDirectoryPath];
    self.sortedArray = [self sortedArrayByFoldersAndFiles:self.contentsArray];
    
    [self.tableView reloadData];
    
    NSLog(@"folder create = %d", self.folderCreated);
    
    return self.folderCreated;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.sortedArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *folderIdentifier = @"folderIdentifier";
    static NSString *fileIdentifier = @"fileIdentifier";
    
    NSString *fileName = [self.sortedArray objectAtIndex:indexPath.row];
    
    if ([self isDirectoryAtIndexPathRow:indexPath]) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:folderIdentifier];
        
        NSString *sizeFolderContentsArray = [self reduceFileSize:[self folderSizeWithDirectory:[self.path stringByAppendingPathComponent:fileName]]];
        
        cell.textLabel.text = fileName;
        cell.detailTextLabel.text = sizeFolderContentsArray;
        cell.detailTextLabel.textColor = [UIColor blueColor];
        
        return cell;
        
    } else {
        
        NSString *filePath = [self.path stringByAppendingPathComponent:fileName];
        
        NSDictionary *atributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
        
        ABFileCell *cell = [tableView dequeueReusableCellWithIdentifier:fileIdentifier];
        
        cell.nameLabel.text = fileName;
        cell.sizeLabel.text = [self reduceFileSize:[atributes fileSize]];
        
        NSDateFormatter *dateFormatter = nil;
        
        if (!dateFormatter) {
            dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"MM/dd/yyyy HH:mm"];
        }
        cell.dateLabel.text = [dateFormatter stringFromDate:[atributes fileCreationDate]];
        
        return cell;
    }
}

// Data manipulation - insert and delete support

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // delete folder from disc
    
    NSString *fileName = [self.sortedArray objectAtIndex:indexPath.row];
    NSString *filePath = [self.path stringByAppendingPathComponent:fileName];
    
    NSError *error = nil;
    
    [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
    
    if (error) {
        NSLog(@"Error = %@", [error localizedDescription]);
    }
    
    // delete correspond object in array
    
    NSMutableArray *tmpArray = [NSMutableArray arrayWithArray:self.sortedArray];
    [tmpArray removeObjectAtIndex:indexPath.row];
    
    self.sortedArray = tmpArray;
    
    // delete correspond row in section (with anamation)
    
    [self.tableView beginUpdates];
    
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    
    [self.tableView endUpdates];
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self isDirectoryAtIndexPathRow:indexPath]) {
        return 60.f;
    } else {
        return 80.f;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([self isDirectoryAtIndexPathRow:indexPath]) {
        
        NSString *fileName = [self.sortedArray objectAtIndex:indexPath.row];
        NSString *filePath = [self.path stringByAppendingPathComponent:fileName];
        
        ABDirectoryTableVC *directoryTVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ABDirectoryTableVC"];
        
        directoryTVC.path = filePath;
        
        [self.navigationController pushViewController:directoryTVC animated:YES];
    }
}




#pragma mark - Reduce methods

- (NSString *) reduceFileSize:(unsigned long long) fileSize {
    
    static NSString *units[] = {@"B", @"KB", @"MB", @"GB", @"TB"};
    static int unitsCount = 5;
    
    int index = 0;
    
    double size = (double) fileSize;
    
    while (size > 1000 && index < unitsCount) {
        
        size = size / 1000;
        index ++;
    }
    
    return [NSString stringWithFormat:@"%.2f %@", size, units[index]];
}

- (unsigned long long) folderSizeWithDirectory:(NSString *) folderDirectory {
                                    
    unsigned long long folderSize = 0;
    
    NSArray *folderContentsArray = [self getContentsFromPath:folderDirectory];
    
    for (int i = 0; i < [folderContentsArray count]; i++) {
        
        NSString *fileName = [folderContentsArray objectAtIndex:i];
        NSString *filePath = [folderDirectory stringByAppendingPathComponent:fileName];

        BOOL isDirectory = [self isDirectoryAtPath:filePath];
        
        if (!isDirectory) {
            
            NSDictionary *atributes = [[NSFileManager defaultManager] attributesOfItemAtPath:filePath error:nil];
            unsigned long long fileSize = [atributes fileSize];

            folderSize += fileSize;
        
        } else {
            folderSize += [self folderSizeWithDirectory:filePath];
        }
    }
    
    return folderSize;
}

- (NSArray *) getContentsFromPath:(NSString *)contentPath {
    
    NSError *error = nil;
    
    NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:contentPath
                                                                             error:&error];
    if (error) {
        NSLog(@"Error = %@", [error localizedDescription]);
    }
    
    return contents;
}

- (NSArray *) sortedArrayByFoldersAndFiles:(NSArray *)arrayForSort {
    
    NSMutableArray *foldersArray = [NSMutableArray array];
    NSMutableArray *filesArray   = [NSMutableArray array];
    
    for (NSString *object in arrayForSort) {
        
        if ([object characterAtIndex:0] != '.') {
            
            NSString *filePath = [self.path stringByAppendingPathComponent:object];
            
            BOOL isDirectory = [self isDirectoryAtPath:filePath];
            
            if (isDirectory) {
                [foldersArray addObject:object];
            } else {
                [filesArray addObject:object];
            }
        }
    }
    
    [foldersArray addObjectsFromArray:filesArray];
    
    NSArray *sortedArray = [NSMutableArray arrayWithArray:foldersArray];
    
    return sortedArray;
}



@end
