//
//  ABFileCell.m
//  AB_number_33-34
//
//  Created by Alexander Berezovskyy on 10.05.17.
//  Copyright © 2017 Alexander Berezovskyy. All rights reserved.
//

#import "ABFileCell.h"

@implementation ABFileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
